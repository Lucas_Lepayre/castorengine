<?php
class tga
{
	private function rle_decode($data, $datalen)
	{
		$len = strlen($data);
		$out = '';
		$i = 0;
		$k = 0;
		while ($i<$len) {
			$this->dec_bits(ord($data[$i]), $type, $value);
			if ($k >= $datalen) break;
			$i++;
			if ($type == 0) {
				for ($j=0; $j<3*$value; $j++) {
					$out .= $data[$j+$i];
					$k++;
				}
				$i += $value*3;
			} else {
				for ($j=0; $j<$value; $j++) {
					$out .= $data[$i] . $data[$i+1] . $data[$i+2];
					$k++;
				}
				$i += 3;
			}
		}
		return $out;
	}

	private function dec_bits($byte, &$type, &$value)
	{
		$type = ($byte & 0x80) >> 7;
		$value = 1 + ($byte & 0x7F);
	}

	private function dwordize($str)
	{
		$a = ord($str[0]);
		$b = ord($str[1]);
		$c = ord($str[2]);
		return $c*256*256 + $b*256 + $a;
	}

	private function bit5($x)
	{
		return ($x & 32) >> 5;
	}

	public function getimagesizetga($filename)
	{
		$f = fopen($filename, 'rb');
		$header = fread($f, 18);
		$header = @unpack("cimage_id_len/ccolor_map_type/cimage_type/vcolor_map_origin/vcolor_map_len/ccolor_map_entry_size/vx_origin/vy_origin/vwidth/vheight/cpixel_size/cdescriptor", $header);
		fclose($f);
		$types = array(0,1,2,3,9,10,11,32,33);
		if (!in_array($header['image_type'], $types)) {
			return array(0, 0, 0, 0, 0);
		}
		if ($header['pixel_size'] > 32) {
			return array(0, 0, 0, 0, 0);
		}
		return array($header['width'], $header['height'], 'tga', $header['pixel_size'], $header['image_type']);
	}

	public function imagecreatefromtga($filename)
	{
		$f = fopen($filename, 'rb');
		if (!$f) {
			return false;
		}
		$header = fread($f, 18);
		$header = unpack("cimage_id_len/ccolor_map_type/cimage_type/vcolor_map_origin/vcolor_map_len/ccolor_map_entry_size/vx_origin/vy_origin/vwidth/vheight/cpixel_size/cdescriptor", $header);
		switch ($header['image_type']) {
			case 2:		//no palette, uncompressed
			case 10:	//no palette, rle
			break;
			default:	die('Unsupported TGA format');
		}
		if ($header['pixel_size'] != 24) {
			//die('Unsupported TGA color depth');
		}
		$bytes = $header['pixel_size'] / 8;
		if ($header['image_id_len'] > 0) {
			$header['image_id'] = fread($f, $header['image_id_len']);
		} else {
			$header['image_id'] = '';
		}
		$im = imagecreatetruecolor($header['width'], $header['height']);
		$size = $header['width'] * $header['height'] * 3;
		//-- check whether this is NEW TGA or not
		$pos = ftell($f);
		fseek($f, -26, SEEK_END);
		$newtga = fread($f, 26);
		if (substr($newtga, 8, 16) != 'TRUEVISION-XFILE') {
			$newtga = false;
		}
		fseek($f, 0, SEEK_END);
		$datasize = ftell($f) - $pos;
		if ($newtga) {
			$datasize -= 26;
		}
		fseek($f, $pos, SEEK_SET);
		$data = fread($f, $datasize);
		if ($header['image_type'] == 10) {
			$data = $this->rle_decode($data, $size);
		}
		if ($this->bit5($header['descriptor']) == 1)
		{
			$reverse = true;
		} else {
			$reverse = false;
		}
		$pixels = str_split($data, 3);
		$i = 0;
		if ($reverse){
			for ($y=0; $y<$header['height']; $y++) {
				for ($x=0; $x<$header['width']; $x++) {
					imagesetpixel($im, $x, $y, $this->dwordize($pixels[$i]));
					$i++;
				}
			}
		} else {
			for ($y=$header['height']-1; $y>=0; $y--) {
				for ($x=0; $x<$header['width']; $x++) {
					imagesetpixel($im, $x, $y, $this->dwordize($pixels[$i]));
					$i++;
				}
			}
		}
		fclose($f);
		return $im;
	}

	public function resize($img, $w, $h, $newfilename)
	{
		$imgInfo = getimagesize($img);
		switch ($imgInfo[2]) {
			case 1: $im = imagecreatefromgif($img); break; // GIF
			case 2: $im = imagecreatefromjpeg($img);break; // JPEG
			case 3: $im = imagecreatefrompng($img); break; // PNG
		}
		if ($imgInfo[0] <= $w && $imgInfo[1] <= $h) {
			$nHeight = $imgInfo[1];
			$nWidth = $imgInfo[0];
		} else {
			if ($w/$imgInfo[0] > $h/$imgInfo[1]) {
				$nWidth = $w;
				$nHeight = $imgInfo[1]*($w/$imgInfo[0]);
			} else {
				$nWidth = $imgInfo[0]*($h/$imgInfo[1]);
				$nHeight = $h;
			}
		}
		$nWidth = round($nWidth);
		$nHeight = round($nHeight);
		$newImg = imagecreatetruecolor($nWidth, $nHeight);
		if(($imgInfo[2] == 1) OR ($imgInfo[2]==3)){
			imagealphablending($newImg, false);
			imagesavealpha($newImg,true);
			$transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 127);
			imagefilledrectangle($newImg, 0, 0, $nWidth, $nHeight, $transparent);
		}
		imagecopyresampled($newImg, $im, 0, 0, 0, 0, $nWidth, $nHeight, $imgInfo[0], $imgInfo[1]);
		switch ($imgInfo[2]) {
			case 1: imagegif($newImg,$newfilename); break;  // GIF
			case 2: imagejpeg($newImg,$newfilename);break; // JPEG
			case 3: imagepng($newImg,$newfilename); break; // PNG
		}
		return $newfilename;
	}
}
?>