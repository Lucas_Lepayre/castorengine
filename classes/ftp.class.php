<?php
Class FTPClient
{
    private $connectionId;
	private $loginOk = false;
	private $messageArray = array();

    public function __construct() { }

	private function logMessage($message)
	{
		$this->messageArray[] = $message;
	}

	public function getMessages()
	{
		return $this->messageArray;
	}

	public function connect ($server, $ftpUser, $ftpPassword, $isPassive = false)
	{
		// *** Set up basic connection
		$this->connectionId = ftp_connect($server);
		// *** Login with username and password
		$loginResult = ftp_login($this->connectionId, $ftpUser, $ftpPassword);
		// *** Sets passive mode on/off (default off)
		ftp_pasv($this->connectionId, $isPassive);
		// *** Check connection
		if ((!$this->connectionId) || (!$loginResult)) {
			$this->logMessage('FTP connection has failed!');
			$this->logMessage('Attempted to connect to ' . $server . ' for user ' . $ftpUser, true);
			return false;
		} else {
			$this->logMessage('Connected to ' . $server . ', for user ' . $ftpUser);
			$this->loginOk = true;
			return true;
		}
	}

	public function makeDir($directory)
	{
		// *** If creating a directory is successful...
		if (ftp_mkdir($this->connectionId, $directory)) {
			$this->logMessage('Directory "' . $directory . '" created successfully');
			return true;
		} else {
			// *** ...Else, FAIL.
			$this->logMessage('Failed creating directory "' . $directory . '"');
			return false;
		}
	}

	public function uploadFile ($fileFrom, $fileTo)
	{
		// *** Set the transfer mode
		$asciiArray = array('txt', 'csv');
		$extension = end(explode('.', $fileFrom));
		if (in_array($extension, $asciiArray)) {
			$mode = FTP_ASCII;
		} else {
			$mode = FTP_BINARY;
		}
		// *** Upload the file
		$upload = ftp_put($this->connectionId, $fileTo, $fileFrom, $mode);
		// *** Check upload status
		if (!$upload) {
			$this->logMessage('FTP upload has failed!');
			return false;
		} else {
			$this->logMessage('Uploaded "' . $fileFrom . '" as "' . $fileTo);
			return true;
		}
	}

	public function changeDir($directory)
	{
		if (ftp_chdir($this->connectionId, $directory)) {
			$this->logMessage('Current directory is now: ' . ftp_pwd($this->connectionId));
			return true;
		} else {
			$this->logMessage('Couldn\'t change directory');
			return false;
		}
	}

	public function getDirListing($directory = '.', $parameters = '-la')
	{
		// get contents of the current directory
		$contentsArray = ftp_nlist($this->connectionId, $parameters . '  ' . $directory);
		return $contentsArray;
	}

}

/* USE
// *** Define your host, username, and password
define('FTP_HOST', '192.168.1.88');
define('FTP_USER', 'Blimpf');
define('FTP_PASS', 'catfish');


// *** Include the class
include('ftp_class.php');

// *** Create the FTP object
$ftpObj = new FTPClient();

// *** Connect
// *** Connect
if ($ftpObj -> connect(FTP_HOST, FTP_USER, FTP_PASS)) {
    // *** Then add FTP code here
    echo 'connected';
} else {
    echo 'Failed to connect';
}


$dir = 'photos';
// *** Make directory
$ftpObj->makeDir($dir);


$fileFrom = 'zoe.jpg';
$fileTo = $dir . '/' . $fileFrom;
// *** Upload local file to new directory on server
$ftpObj -> uploadFile($fileFrom, $fileTo);


// *** Change to folder
$ftpObj->changeDir($dir);

// *** Get folder contents
$contentsArray = $ftpObj->getDirListing();
// *** Output our array of folder contents
print_r($contentsArray);

//debug
print_r($ftpObj->getMessages());
*/
?>
