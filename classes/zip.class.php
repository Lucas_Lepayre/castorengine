<?php
class HZip
{
	/**
	* Ajouter des fichiers et sous-r�pertoires dans un dossier de fichier zip .
	* @param string $folder
	* @param ZipArchive $zipFile
	* @param int $exclusiveLength Number of text to be exclusived from the file path.
	*/
    private static function folderToZip($folder, &$zipFile, $exclusiveLength) {
		$handle = opendir($folder);
		while ($f = readdir($handle)) {
			if ($f != '.' && $f != '..') {
				$filePath = "$folder/$f";
				// Supprimer pr�fixe de chemin d'acc�s avant d'ajouter au zip.
				$localPath = substr($filePath, $exclusiveLength);
				if (is_file($filePath)) {
					chmod($filePath, 0777);
					$zipFile->addFile($filePath, $localPath);
				} elseif (is_dir($filePath)) {
					// Add sub-directory.
					$zipFile->addEmptyDir($localPath);
					chmod($filePath, 0777);
					self::folderToZip($filePath, $zipFile, $exclusiveLength);
				}
			}
		}
		closedir($handle);
    }

	/**
	* Zip a folder (include itself).
	* Usage:
	*   HZip::zipDir('/path/to/sourceDir', '/path/to/out.zip');
	*
	* @param string $sourcePath Path of directory to be zip.
	* @param string $outZipPath Path of output zip file.
	*/
	public static function zipDir($sourcePath, $outZipPath)
	{
		$pathInfo = pathInfo($sourcePath);
		chmod($outZipPath, 0777);
		$parentPath = $pathInfo['dirname'];
		$dirName = $pathInfo['basename'];
		$z = new ZipArchive();
		$z->open($outZipPath, ZIPARCHIVE::CREATE);
		$z->addEmptyDir($dirName);
		self::folderToZip($sourcePath, $z, strlen("$parentPath/"));
		$z->close();
	}

}
?>