<?php error_reporting(0);
class Lang
{
	public $lang;
	public $load = array();
	public $listLangs = array();
	public $ext = ".lng";
	public $path = "lang/";
	public $cookieName = "langCastor";
	public $defautLang = "English";

	public function __construct() {
		if(isset($_GET['lang'])) {
			$this->CheckLang($_GET['lang']);
			$delais = time() + 365*24*3600;
			@setcookie($this->cookieName, $_GET['lang'], $delais, "/Editor", null, false, false);
		}
		elseif(@$_COOKIE[$this->cookieName]) {
			$this->CheckLang(@$_COOKIE[$this->cookieName]);
		}
		else {
			$this->CheckLang($this->defautLang);
			@setcookie($this->cookieName, $this->defautLang, $delais, "/Editor", null, false, false);
		}
		$this->GetLangs();
		$this->LoadLang();
	}

	private function CheckLang($lang) {
		$this->lang = $lang;
	}

	private function LoadLang() {
		$file = file_get_contents($this->path.$this->lang.$this->ext);
		$json = json_decode($file, true);
		foreach($json as $key => $value) {
			$this->load[$key] = $value;
		}	
	}

	private function GetLangs() {
		if( $handle = opendir($this->path)) {
		    while(false !== ($file = readdir($handle))) {
		        if($file != "." && $file != ".." && preg_match("/.lng$/",$file)) $this->listLangs[] = $file;
		    }
		    closedir($handle);
		}
	}
}
?>