<?php header('Content-type: text/html; charset=utf-8');
error_reporting(0);
//include_once('classes/jsmin.class.php');
$dirname = '../public_scripts/'.$_POST['user'].'/';
if($_POST['getScript'])
{
	echo file_get_contents('../'.$_POST['getScript']);
}
elseif($_POST['setScript'])
{
	$json = array();
	$dir = opendir($dirname);
	while($file = readdir($dir)) {
 		if($file != '.' && $file != '..') {
			$scriptName = $file;
			$code = file_get_contents($dirname.$scriptName);
			if($code) {
				file_put_contents($dirname.$scriptName, $code);
				chmod($dirname.$scriptName, 0777);
				$fileName = str_replace(".js", "", $file);
				$json[$fileName] = $code;
			}
		}
	}
	closedir($dir);
	echo json_encode($json, JSON_UNESCAPED_SLASHES, JSON_UNESCAPED_UNICODE);
}
elseif($_POST['saveScript'])
{
	$scriptName = $_POST['name'];
	chmod('../'.$_POST['saveScript'], 0777);
	$code = $_POST['code'];
	file_put_contents('../'.$_POST['saveScript'], $code);
	//file_put_contents('../'.$_POST['saveScript'], trim(JSMin::minify($code)));
	chmod('../'.$_POST['saveScript'], 0644);
}
elseif($_POST['creatScript'])
{
	file_put_contents($dirname.$_POST['name'].".js", $_POST['code']);
	chmod($dirname.$_POST['name']."js", 0777);
	echo "ok";
}
elseif($_POST['newScript'])
{
	$scriptNew = null;
	$scriptNew = "var SCRIPT = SCRIPT || {};

(function ()
{
	// Cette fonction est appelée que une seule fois au chargement du jeu.
	// Declarer ici toutes vos variables à assigner.
	SCRIPT.Start_".$_POST['name']." = function(gameobject){
		this.gameobject = gameobject || null;
	};

	// Cette fonction est appelée avant de rendre une image. C'est la boucle de jeu qui régler a 60 frames par seconde.
	// Ajouter dans cette fonction tout ce qui doit être animmer (transformation d'objets par exemple)
	SCRIPT.Start.prototype.Update_".$_POST['name']." = function(){

	};

	// Cette fonction est appelée que une seule fois au chargement du jeu pour afficher tout ce qui est interface graphique.
	// Ajouter ici tout ce qui est fenetres avec la class CASTOR.GUI()
	SCRIPT.Start.prototype.OnGUI_".$_POST['name']." = function(){

	};

})();";
	if(!file_get_contents($dirname.$_POST['name'].".js"))
	{
		file_put_contents($dirname.$_POST['name'].".js", $scriptNew);
		chmod($dirname.$_POST['name'].".js", 0777);
		echo $scriptNew;
	} else {
		echo "failled";
	}
}
elseif($_POST['deleteScript'])
{
	chmod('../'.$_POST['deleteScript'], 0777);
	unlink('../'.$_POST['deleteScript']);
}
elseif($_POST['newNameScript'])
{
	chmod($dirname.$_POST['oldName'].".js", 0777);
	rename($dirname.$_POST['oldName'].".js", $dirname.$_POST['name'].".js");
	chmod($dirname.$_POST['name'].".js", 0644);
	echo file_get_contents($dirname.$_POST['name'].".js");
}
?>