<?php
header('Content-Type: text/html; charset=UTF-8');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="sk" lang="sk">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta http-equiv="Content-Language" content="sk" />
		<title></title>
	</head>
	<body>
		<form method="POST">
			<table width="100%">
				<tr>
					<th style="text-align:left"> Editer une lange:</th>
					<th style="text-align:left"><select name="editerLang">
							<?php $i = 0;
							$flag_exist = array();
							$dir = opendir("lang/");
							while($file = readdir($dir)) {
								if($file != '.' && $file != '..' && $file != "images") {									
									$file = str_replace(".lng", "", $file);
									echo '<option value="'.$file.'">'.$file.'</option>';	
									$flag_exist[$i] = $file;
									$i++;
								}
							}
							closedir($dir);							
							?>							
						</select>
					</th>
					<th style="text-align:left"> Ou créer suivant cette liste :</th> 
					<th style="text-align:left">
						<select name="newLang">
							<?php
							$dir = opendir("lang/images/");
							while($file = readdir($dir)) {
								if($file != '.' && $file != '..' && $file != "add_lang.png") {									
									$file = str_replace(".png", "", $file);
									$key = array_search($file, $flag_exist);
									if(array_key_exists($key, $flag_exist) == false)
									{
										echo '<option value="'.$file.'">'.$file.'</option>';
									}
								}
							}
							closedir($dir);							
							?>
						</select>
				</tr>
				<tr>
					<th style="text-align:left"> Comparer avec:</th>
					<th style="text-align:left"><select name="comparerLang">
							<?php
							foreach($flag_exist as $lang) {
								echo '<option value="'.$lang.'">'.$lang.'</option>';
							}
							?>
						</select> <input type="submit" name="valider" value="Valider vos choix" />		
					</th>
					<th>&nbsp;</th>
				</tr>
			</table>
		</form>
		<hr />		
		<form method="POST">	
			<table width="100%">
				<tr>
					<th>Langue de comparaisons</th>
					<th>Langue à éditer/créer</th>
				</tr>
				<?php	
				if(@$_POST['newLang'] != "") {
					$newLangue = $_POST['newLang'].".lng";
					@file_put_contents("lang/".$_POST['newLang'].".lng", "{}");
					chmod("lang/".$_POST['newLang'].".lng", 0777);
					$fileEdite = @file_get_contents("lang/".$_POST['newLang'].".lng");
					$lang = $_POST['newLang'];
				} else {
					$fileEdite = @file_get_contents("lang/".$_POST['editerLang'].".lng");
					$lang = @$_POST['newLang'];
				}		
				$fileComparaisons = @file_get_contents("lang/".$_POST['comparerLang'].".lng");	
				$json_edite = @json_decode($fileEdite, true);
				$json_comparaisons = @json_decode($fileComparaisons, true);	
				
				echo '<input type="hidden" name="lang" value="'.$lang.'" />';
				
				if($json_comparaisons) {
					foreach($json_comparaisons as $key => $value) {			
						?>
						<tr>
							<td><input type="text" id="<?php echo $key;?>_comparaisons" value="<?php echo $value;?>" size="60" disabled /></td>
							<td><input type="text" name="key[<?php echo $key;?>]" value="<?php echo @$json_edite[$key];?>" size="60" /></td>
						</tr>
						<?php
					}
				}
				?>
			</table>
			<input type="submit" name="valider" value="Enregistrer" style="float:right;margin-right:10px;width:200px;height:35px;background-color:#0972a5;color:white;" /><br /><br />
		</form>
	</body>
</html>