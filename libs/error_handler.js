﻿var appIsHandlingError = false;
var lang_handle = new LANG.load();
function handleError(msg, url, line, column)
{		
	$.ajax({ type:"POST", cache:false, url:"Data/PHP/jserrorhandler.php",
		data: 'message='+ msg +'&url='+url+'&userAgent='+navigator.userAgent+'&line='+line+'&collone='+column+'&website='+document.domain,
		success: function(test){			
			if(window.console) console.log(lang_handle.search("handle", "consoleError") +"\n "+lang_handle.search("handle", "message")+msg+"\n URL="+url+"\n "+lang_handle.search("handle", "line")+line+" - "+lang_handle.search("handle", "column")+column);			
		}
	});
	IsHandlingError = false;
	return true;
}
(function($){
	$.fn.jsErrorHandler = function(options) {
		/*
		var settings = {website: document.domain}
		if (options) $.extend(settings, options);
		*/
		window.onerror = function (msg, url, line, column) {		
			if (!appIsHandlingError) {
				appIsHandlingError = true;
				handleError(msg, url, line, column);
			}
		};
  };
})(jQuery);		