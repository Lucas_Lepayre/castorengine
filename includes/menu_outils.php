<div id="toolbar" class="ui-widget-header ui-corner-all">
  <button id="new-projet"><?php echo $lang->load["menu-outil-new"];?></button>
  <button id="open-projet"><?php echo $lang->load["menu-outil-open"];?></button>
  <button id="save-projet"><?php echo $lang->load["menu-outil-save"];?></button>
  <button id="screeshot"><?php echo $lang->load["menu-outil-screenshot"];?></button>
  <span id="repeat">
		<input type="radio" id="move" name="repeat" checked="checked"><label for="move" style="height:27px"></label>
		<input type="radio" id="rotate" name="repeat"><label for="rotate" style="height:27px"></label>
		<input type="radio" id="scale" name="repeat"><label for="scale" style="height:27px"></label>
  </span>
  <button id="cloner"><?php echo $lang->load["menu-outil-clone"];?></button>
  <button id="instancier"><?php echo $lang->load["menu-outil-instance"];?></button>
  <button id="supprimer"><?php echo $lang->load["menu-outil-delete"];?></button>
  <button id="play"><?php echo $lang->load["menu-outil-play"];?></button>
</div>
