<div id="menu-barre">
	<ul id="nav" class="nav">
		<li><a class="clk" href="javascript:void(0);"><?php echo $lang->load["menu-file"];?></a>
			<ul class="sub-menu">
				<li><img src="images/file.gif"><a href="javascript:void(0);" onClick="menu.menuFile('new');"><?php echo $lang->load["sub-menu-new"];?></a></li>
				<li><img src="images/open.png"><a href="javascript:void(0);" onClick="menu.menuFile('open');"><?php echo $lang->load["sub-menu-openpc"];?></a></li>
				<li><img src="images/load_ftp.png"><a href="javascript:void(0);" onClick="menu.menuFile('openServeur');"><?php echo $lang->load["sub-menu-openftp"];?></a></li>
				<li><img src="images/save.png"><a href="javascript:void(0);" onClick="menu.menuFile('save');"><?php echo $lang->load["sub-menu-savetopc"];?></a></li>
				<li><img src="images/add_ftp.png"><a href="javascript:void(0);" onClick="menu.menuFile('saveServeur');"><?php echo $lang->load["sub-menu-savetoftp"];?></a></li>
				<li><img src="images/zip.gif"><a href="javascript:void(0);" onClick="menu.menuFile('exportZip');"><?php echo $lang->load["sub-menu-export"];?></a></li>
				<li><img src="images/games.png"><a href="javascript:void(0);" onClick="menu.menuFile('publish');"><?php echo $lang->load["sub-menu-publish"];?></a></li>
				<li><img src="images/about.png"><a href="javascript:void(0);" onClick="menu.menuFile('about');"><?php echo $lang->load["sub-menu-credit"];?></a></li>
			</ul>
		</li>
		<li><a class="clk" href="javascript:void(0);"><?php echo $lang->load["menu-edite"];?></a>
			<ul class="sub-menu">
				<li><img src="images/undo.png"><a href="javascript:void(0);" onClick="menu.menuEdite('undo');"><?php echo $lang->load["sub-menu-undo"];?></a></li>
				<li><img src="images/redo.png"><a href="javascript:void(0);" onClick="menu.menuEdite('redo');"><?php echo $lang->load["sub-menu-redo"];?></a></li>
				<li><img src="images/menu.png"><a href="javascript:void(0);"><?php echo $lang->load["menu-lang"];?></a>
					<ul class="sub-menu">
						<?php
						$dir = opendir("lang/");
						while($file = readdir($dir)) {
							if($file != '.' && $file != '..' && $file != "images") {									
								$file = str_replace(".lng", "", $file);
								echo '<li><img src="lang/images/'.$file.'.png"><a href="javascript:void(0);" onClick="menu.menuEdite(\''.$file.'\');">'.$file.'</a></li>';									
							}
						}
						closedir($dir);
						?>						
						<li><img src="lang/images/add_lang.png"><a href="javascript:void(0);" onClick="menu.menuEdite('add_lang');"><?php echo $lang->load["sub-menu-addlang"];?></a></li>
					</ul>
				</li>
			</ul>
		</li>
		<li><a class="clk" href="javascript:void(0);"><?php echo $lang->load["menu-compte"];?></a>
			<ul class="sub-menu">
				<li id="li_connexion"><img src="images/connexion.png"><a href="javascript:void(0);" onClick="menu.menuCompte('login');"><?php echo $lang->load["sub-menu-connect"];?></a></li>
				<li id="li_profil"><img src="images/profil.png"><a href="javascript:void(0);" onClick="menu.menuCompte('profil');"><?php echo $lang->load["sub-menu-editeprofil"];?></a></li>
				<li><img src="images/news.png"><a href="javascript:void(0);" target="_blank" onClick="menu.menuCompte('actualiter');"><?php echo $lang->load["sub-menu-cretactu"];?></a></li>
				<li><img src="images/com.png"><a href="javascript:void(0);" target="_blank" onClick="menu.menuCompte('commentaire');"><?php echo $lang->load["sub-menu-seecom"];?></a></li>
				<li><img src="images/game.png"><a href="javascript:void(0);" target="_blank" onClick="menu.menuCompte('games');"><?php echo $lang->load["sub-menu-allgame"];?></a></li>
			</ul>
		</li>
		<li><a class="clk" href="javascript:void(0);"><?php echo $lang->load["menu-communauter"];?></a>
			<ul class="sub-menu">
				<li><img src="images/bug.png"><a href="https://bitbucket.org/Dad72/castorengine/issues?status=new&status=open" target="_blank"><?php echo $lang->load["sub-menu-bug"];?></a></li>
				<li><img src="images/contribute.png"><a href="https://bitbucket.org/Dad72/castorengine" target="_blank"><?php echo $lang->load["sub-menu-contribut"];?></a></li>
				<li><img src="images/tuto.png"><a href="http://www.castorengine.com/forum/index.php?/tutorials/" target="_blank"><?php echo $lang->load["sub-menu-tuto"];?></a></li>
				<li><img src="images/blog.png"><a href="http://www.castorengine.com/forum/index.php?/blogs/" target="_blank"><?php echo $lang->load["sub-menu-blog"];?></a></li>
				<li><img src="images/forum.png"><a href="http://www.castorengine.com/forum/" target="_blank"><?php echo $lang->load["sub-menu-forum"];?></a></li>
			</ul>
		</li>
		<li><a class="clk" href="javascript:void(0);"><?php echo $lang->load["menu-display"];?></a>
			<ul class="sub-menu">
				<li><img src="images/stats.png"><a href="javascript:void(0);" onClick="menu.menuDisplay('stats');"><?php echo $lang->load["sub-menu-stat"];?></a></li>
				<li><img src="images/debug.png"><a href="javascript:void(0);" onClick="menu.menuDisplay('debug');"><?php echo $lang->load["sub-menu-debuglayer"];?></a></li>
				<li><img src="images/property.png"><a href="javascript:void(0);" onClick="menu.menuDisplay('properties');"><?php echo $lang->load["sub-menu-properties"];?></a></li>
				<li><img src="images/store.png"><a href="javascript:void(0);" onClick="menu.menuDisplay('store');"><?php echo $lang->load["sub-menu-store"];?></a></li>
			</ul>
		</li>
	</ul>
</div>
<div id="version">
<?php
$version = json_decode(file_get_contents('version.json'), true);
echo "<a href='http://www.castorengine.com/' target='_blank'>Castor3D Studio</a> ".$version['version']." - Update: ".$version['update']." |
	  <a href='http://www.babylonjs.com/' target='_blank'>Engine</a>: <span id='versionEngine'></span>";
?>
</div>
<script>
$(document).ready(function(){
	$("#nav").menu({
		position: {my: "left top", at: "left bottom"},
		items: "> :not(.ui-widget-header)",
		blur: function() {
			$(this).menu("option", "position", {my: "left top", at: "left bottom"});
		},
		focus: function(e, ui) {
			if ($("#nav").get(0) !== $(ui).get(0).item.parent().get(0)) {
				$(this).menu("option", "position", {my: "left top", at: "right top"});
			}
		}
	});
	//$('.ui-menu-icon .ui-icon .ui-icon-carat-1-e').remove();
});
</script>