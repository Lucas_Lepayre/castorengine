var LANG = LANG || {};

(function ()
{
	LANG.loading = function()
	{
		this.lang = this.getCookie("langCastor");		
		if(this.lang == null) { 
			this.lang = "English";			
		}
		this.root = "./lang/"+this.lang+".lng";
		this.load = [];
		this.LoadLang();
	};

	LANG.loading.prototype.LoadLang = function()
	{
		var that = this;
		$.getJSON(this.root, function(json) {			
			$.each(json, function (key, data) {
				that.load[key] = data;
			});			
		});
	};

	LANG.loading.prototype.getCookie = function(name) {
		if(document.cookie.length == 0) return null;

		var regSepCookie = new RegExp('(; )', 'g');
		var cookies = document.cookie.split(regSepCookie);

		for(var i = 0; i < cookies.length; i++){
		    var regInfo = new RegExp('=', 'g');
		    var infos = cookies[i].split(regInfo);
		    if(infos[0] == name){
				return unescape(infos[1]);
		    }
		}
		return null;
    };

    LANG.loading.prototype.setCookie = function(name, value, days) {
		if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			var expires = "; expires="+date.toGMTString();
		}
		else var expires = "";
		document.cookie = name+"="+value+expires+"; path=/Editor";
	};

})();