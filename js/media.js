/// <reference path="libs/babylon.d.ts" />

var CASTORENGINE = CASTORENGINE || {};

(function () {

	/*!
	* \class scene
	* Contructeur de la classe scene
	*/
	CASTORENGINE.media = function()
	{
		BABYLON.Engine.ShadersRepository = "data/";
		this.canvas = document.getElementById("renderMedia");

		BABYLON.SceneLoader.ForceFullSceneLoadingForIncremental = true;
		this.engine = new BABYLON.Engine(this.canvas, true);
		this.engine.enableOfflineSupport = false;

		this.scene = null;
		this.cameraMedia = null;
		this.LightHemispheric = null;
		this.objets = [];
		this.myCheminAndFile;
		this.SONS = null;
	};

	/*!
	* Point d'entrer du chargement de la scene 3d
	*/
	CASTORENGINE.media.prototype.init = function(CheminDuModele, Fichier, isScene)
	{
		var that = this;
		if(this.scene) {
			//this.engine.stopRenderLoop();
			this.scene.dispose();
			this.scene = null;
			//this.engine.dispose();
			//this.engine = null;
		}

		this.scene = new BABYLON.Scene(this.engine);
		this.scene.ShowLoadingScreen = false;
		this.scene.ForceFullSceneLoadingForIncremental = true;
		this.scene.clearColor = new BABYLON.Color3(0.20, 0.20, 0.20);
		this.scene.ambientColor = new BABYLON.Color3(0.3, 0.3, 0.3);

		this.Camera();
		this._Light();

		if(isScene == false) {
			this.engine.displayLoadingUI();
			BABYLON.SceneLoader.ImportMesh("", CheminDuModele, Fichier, this.scene, function (newMeshes)
			{
				that.myCheminAndFile = CheminDuModele+Fichier;
				newMeshes[0].position = new BABYLON.Vector3(0, 0, 0);
				mesh = newMeshes[0];
				that.engine.hideLoadingUI();
			}, function(e) {
				if (e.lengthComputable) {
					that.engine.loadingUIText = lang.load["loading"]+ (e.loaded * 100 / e.total).toFixed() + "%";
				} else {
					var dlCount = e.loaded / (1024 * 1024);
					that.engine.loadingUIText = lang.load["loading"]+ Math.floor(dlCount * 100.0) / 100.0 + " MB already loaded.";
				}
			});
		} else {
			if(CheminDuModele && Fichier) {
				BABYLON.SceneLoader.Load(CheminDuModele, Fichier, this.engine, function (newScene) {
					media.scene.dispose();
					media.scene = newScene;
					if(newScene.activeCamera) {
						newScene.activeCamera.dispose();
					}
				}, function (event) {
					if (event.lengthComputable) {
						that.engine.loadingUIText = lang.load["loading"]+ (event.loaded * 100 / event.total).toFixed() + "%";
					} else {
						var dlCount = event.loaded / (1024 * 1024);
						that.engine.loadingUIText = lang.load["loading"]+ Math.floor(dlCount * 100.0) / 100.0 + " MB already loaded.";
					}
				});
			}
		}

		// Loop of game
		this.engine.runRenderLoop(function () {
			that.scene.render();
		});

		// bloque le clique droit
		this.canvas.oncontextmenu = function(e){
			e.preventDefault();
			return false;
		};

		// Resize engine
		window.addEventListener("resize", function () {
			that.engine.resize();
		});

	};

	/*!
	* Charge la camera de la scene
	*/
	CASTORENGINE.media.prototype.Camera = function()
	{
		this.cameraMedia = new BABYLON.ArcRotateCamera("cameraMedia", 3.14, 3.14/2, 17, BABYLON.Vector3.Zero(), this.scene);
		this.cameraMedia.lowerRadiusLimit = 0.0001;
		this.cameraMedia.wheelPrecision = 15;
		this.cameraMedia.__fxaa_cookie = new BABYLON.FxaaPostProcess("fxaa", 1.0, this.cameraMedia, BABYLON.Texture.BILINEAR_SAMPLINGMODE, this.engine, false);
		this.cameraMedia.attachControl(this.canvas, true);
	};

	/*!
	* Charge la lumiere directionnelle de la scene
	*/
	CASTORENGINE.media.prototype._Light = function()
	{
		var colorSpecular = BABYLON.Color3.Black();
		var colorDiffuse =  new BABYLON.Color3(0.80, 0.80, 0.80);
		this.LightHemispheric = new BABYLON.HemisphericLight("Ambiante", new BABYLON.Vector3(0, 1, 0), this.scene);
		this.LightHemispheric.diffuse = colorDiffuse;
		this.LightHemispheric.specular = colorSpecular;
		this.LightHemispheric.groundColor = BABYLON.Color3.Black();
		this.LightHemispheric.intensity = 1.6;
	};

	CASTORENGINE.media.prototype.selectEntity = function(meshe, dossier, root)
	{
		var that = this;
		$('#imageAppercue').html('');
		$('#imageAppercue, #ControleSons').hide();
		switch(dossier) {
			case 'meshes': // On li les fichiers modeles babylon				
				var CheminDuModele = decodeURIComponent(root);
				CheminDuModele = $.parseJSON(CheminDuModele);				
				CheminDuModele = CheminDuModele.chemin;
				if(this.SONS) this.SONS.stop();
				var MyFichier = basename(CheminDuModele);				
				CheminDuModele = CheminDuModele.replace(MyFichier, "");				
				that.init(CheminDuModele, MyFichier, false);
			break;
			case 'scenes': // On li les fichiers scenes babylon
				var CheminDuModele = decodeURIComponent(root);
				CheminDuModele = $.parseJSON(CheminDuModele);				
				CheminDuModele = CheminDuModele.chemin;
				if(this.SONS) this.SONS.stop();
				var MyFichier = basename(CheminDuModele);				
				CheminDuModele = CheminDuModele.replace(MyFichier, "");	
				that.init(CheminDuModele, MyFichier, true);
			break;
			case 'textures': // On li les fichiers images
				if(this.SONS) this.SONS.stop();
				var image = new Image();
				var cheminTGA = root.chemin.replace(".png", ".tga");
				if(file_exists(cheminTGA)) {
					imagePNG = root.chemin.replace(".tga", ".png");
					image.src = imagePNG;
					root.chemin = imagePNG;
				} else {
					image.src = root.chemin;
				}
				if(image.height > SCREEN_HEIGHT && image.width <= SCREEN_WIDTH)
					$('#imageAppercue').html('<img src="'+root.chemin+'" height="'+(SCREEN_HEIGHT - 2)+'" style="position:absolute;" />').show(); // on redimmention si c'est trop grand
				else if(image.height <= SCREEN_HEIGHT &&  image.width > SCREEN_WIDTH)
					$('#imageAppercue').html('<img src="'+root.chemin+'" width="'+(SCREEN_WIDTH - 2)+'" style="position:absolute;" />').show(); // on redimmention si c'est trop grand
				else if(image.height > SCREEN_HEIGHT &&  image.width > SCREEN_WIDTH)
					$('#imageAppercue').html('<img src="'+root.chemin+'" width="'+(SCREEN_WIDTH - 2)+'" style="position:absolute;" height="'+(SCREEN_HEIGHT - 2)+'" />').show(); // on redimmention si c'est trop grand
				else {
					$('#imageAppercue').html('<img src="'+root.chemin+'" style="position:absolute;" />').show();
				}
			break;
			case 'sounds': // On li les fichiers musiques et sons
				$('#ControleSons').show();
				if(this.SONS) this.SONS.stop();
				this.SONS = new BABYLON.Sound("Music", root.chemin, that.scene, null, { loop: true, autoplay: true, streaming: true });
			break;
			case 'scripts':
				if(this.SONS) this.SONS.stop();
				$.ajax({ type: "POST", url: 'fonctions/readfile.php', data: "&root="+root.chemin,
					success: function (readfile) {
						$('#ControleSons').hide();
						$('#imageAppercue').html(readfile).show();
					}
				});
			break;
		}
	};

})();