var CASTORENGINE = CASTORENGINE || {};

(function () {

	CASTORENGINE.addObjet = function()
	{
		this.distanceFront = 10;
	};

	CASTORENGINE.addObjet.prototype.readTextFile = function(file)
	{
		var rawFile = new XMLHttpRequest();
		rawFile.open("GET", file, false);
		rawFile.onreadystatechange = function() {
			if(rawFile.readyState === 4) {
				if(rawFile.status === 200 || rawFile.status == 0) {
					return rawFile.responseText;
				}
			}
		}
		rawFile.send(null);
	};

	CASTORENGINE.addObjet.prototype.console = function(msg, error_type)
	{
		var myDate = new Date();
		var hour = myDate.getHours();
		var minute = myDate.getMinutes();
		var second = myDate.getSeconds();
		if(error_type === 1) $("#alertOnDesing").attr("src", "images/error.png").show();
		if(error_type === 2) $("#alertOnDesing").attr("src", "images/warning.png").show();
		if(error_type === 3) $("#alertOnDesing").attr("src", "images/attention.png").show();
		$("#console_desing").append(hour+":"+minute+":"+second+" - "+msg);
	};

	CASTORENGINE.addObjet.prototype.add = function(key)
	{
		switch (key) {
			// cameras
			case "free": case "rotative": case "gamepad": case "touch": case "follow": this.addCamera(key); break;
			// lumieres
			case "spot": case "point": case "directionnnelle": case "hemispherique": this.addLumieres(key); break;
			// primitifs
			case "empty": this.addEmpty(); break;
			case "sprite": this.addSprites(); break;
			case "box": this.addCube(); break;
			case "sphere": this.addSphere(); break;
			case "plan": this.addPlan(); break;
			case "cylindre": this.addCylindre(); break;
			case "torus": this.addTorus(); break;
			case "line": this.addLine(); break;
			case "ground": this.addGround(); break;
			// Composants sur objets
			case "lensflare": this.addLensFlare(); break;
			case "sceneOptimisation": this.addSceneOptimisation(); break;
			case "material": this.addMaterial(); break;
			case "particules": this.addParticules(); break;
			case "sound": this.addSound(); break;
			case "video": this.addVideo(); break;
			case "script": this.addScript(); break;
			case "networkview": this.addReseau(); break;
			// autres
			case "importMesh": this.addMeshes(); break;
			case "importScene": this.addScene(); break;
			case "fog": this.addFog(); break;
			case "environnement": this.addEnvironnement(); break;
			case "scene": this.addEditionScene(); break;
			case "engine": this.addEditionEngine(); break;
			case "sphereshape":this.addShape("sphere"); break;
			case "boxshape":this.addShape("box"); break;
			case "heightmapshape": this.addShape("heightmap"); break;
			case "rigidBody": this.addRigidBody(); break;
			case "collision": this.addCollision(); break;
			case "FXAA": this.addPostProcess("FXAA"); break;
			case "SSAO": this.addPostProcess("SSAO"); break;
			case "procedural": this.addProcedural(); break;
		}
	};

	CASTORENGINE.addObjet.prototype.addExplorer = function(media, type, root, isMedia)
	{
		var ex_Media = null, ex_Media_media = null;
		switch(type) {
			// Onglet design ou/et media ou/et code
			case "meshes":
				if(isMedia == false) {
					$(".explorer").find("#count_meshes").text(parseInt($(".explorer").find("#count_meshes").text()) + 1); ex_Media = $(".explorer").find("#ex_meshes");
				} else if(isMedia == true) {
					$(".explorer_media").find("#count_meshes").text(parseInt($(".explorer_media").find("#count_meshes").text()) + 1); ex_Media_media = $(".explorer_media").find("#ex_meshes");
				}
			break;
			case "scenes": $(".explorer").find("#count_scenes").text(parseInt($(".explorer").find("#count_scenes").text()) + 1); ex_Media = $(".explorer").find("#ex_scenes"); break;
			case "textures":
				$(".explorer").find("#count_textures").text(parseInt($(".explorer").find("#count_textures").text()) + 1); ex_Media = $(".explorer").find("#ex_textures"); break;
				$(".explorer_media").find("#count_textures").text(parseInt($(".explorer_media").find("#count_textures").text()) + 1); ex_Media_media = $(".explorer_media").find("#ex_textures"); break;
			case "sounds":
				$(".explorer").find("#count_sounds").text(parseInt($(".explorer").find("#count_sounds").text()) + 1); ex_Media = $(".explorer").find("#ex_sounds");
				$(".explorer_media").find("#count_sounds").text(parseInt($(".explorer_media").find("#count_sounds").text()) + 1); ex_Media_media = $(".explorer_media").find("#ex_sounds");				
			break;
			case "scripts":
				$(".explorer_code").find("#count_scripts").text(parseInt($(".explorer_code").find("#count_scripts").text()) + 1); ex_Media = $(".explorer_code").find("#ex_scripts");
				$(".explorer_media").find("#count_scripts").text(parseInt($(".explorer_media").find("#count_scripts").text()) + 1); ex_Media_media = $(".explorer_media").find("#ex_scripts");				
			break;
			case "materials":
				$(".explorer").find("#count_materials").text(parseInt($(".explorer").find("#count_materials").text()) + 1); ex_Media = $(".explorer").find("#ex_materials");
				$(".explorer_media").find("#count_materials").text(parseInt($(".explorer_media").find("#count_materials").text()) + 1); ex_Media_media = $(".explorer_media").find("#ex_materials");
			break;
			// Onglet script
			case "references": $(".explorer_code").find("#count_references").text(parseInt((".explorer_code").find$("#count_references").text()) + 1); ex_Media = $(".explorer_code").find("#ex_references"); break;
			//Onglet design
			case "projets": $(".explorer").find("#count_projet3d").text(parseInt($(".explorer").find("#count_projet3d").text()) + 1); ex_Media = $(".explorer").find("#ex_projet3d"); break;
			case "primitifs": $(".explorer").find("#count_primitifs").text(parseInt($(".explorer").find("#count_primitifs").text()) + 1); ex_Media = $(".explorer").find("#ex_primitifs"); break;
			case "sprites": $(".explorer").find("#count_sprites").text(parseInt($(".explorer").find("#count_sprites").text()) + 1); ex_Media = $(".explorer").find("#ex_sprites"); break;
			case "lights_spot": case "lights_directionnnelle":  case "lights_hemispherique":  case "lights_point": $(".explorer").find("#count_lights").text(parseInt($(".explorer").find("#count_lights").text()) + 1); ex_Media = $(".explorer").find("#ex_lights"); break;
			case "world": $(".explorer").find("#count_world").text(parseInt($(".explorer").find("#count_world").text()) + 1); ex_Media = $(".explorer").find("#ex_world"); break;
			case "shapes": $(".explorer").find("#count_shapes").text(parseInt($(".explorer").find("#count_shapes").text()) + 1); ex_Media = $(".explorer").find("#ex_shapes"); break;
			case "video": $(".explorer").find("#count_video").text(parseInt($(".explorer").find("#count_video").text()) + 1); ex_Media = $(".explorer").find("#ex_video");break;
			case "procedural": $(".explorer").find("#count_procedural").text(parseInt($(".explorer").find("#count_procedural").text()) + 1); ex_Media = $(".explorer").find("#ex_procedural"); break;
			case "particules": $(".explorer").find("#count_particle").text(parseInt($(".explorer").find("#count_particle").text()) + 1); ex_Media = $(".explorer").find("#ex_particle"); break;
			case "reseau": $(".explorer").find("#count_world").text(parseInt($(".explorer").find("#count_world").text()) + 1); ex_Media = $(".explorer").find("#ex_world"); break;
		}
		if(ex_Media) { ex_Media.append('<a href="javascript:void(0)" id="'+media.name+'" onClick="editor.selectEntity(\''+media+'\', \''+type+'\');" style="display:block;">'+media.name+'</a>'); }
		else if(ex_Media_media) { ex_Media_media.append('<a href="javascript:void(0)" id="'+media.name+'" onClick="media.selectEntity(\''+media+'\', \''+type+'\', \''+root+'\');" style="display:block;">'+media.name+'</a>'); }
	};

	CASTORENGINE.addObjet.prototype.importMediaStore = function(mesh, type)
	{
		if(mesh) {
			var rootMedia = mesh.replace(basename(mesh), "");
			var nameMedia = basename(mesh);
			var that = this;			
			$("#ControleSons, #ControleVideo, #imageAppercue").hide();
			if(media.SONS) media.SONS.stop();
			if(type == "modeles") {					
				// Ajout sur l'onglet media
				if(media.scene) {
					media.scene.dispose();
					media.scene = null;
					media.init(rootMedia, nameMedia, false);
				}
				this.loadMesh(rootMedia, nameMedia, media.scene, true);
				media.engine.resize();
				setTimeout(function() {
					// Ajout sur l'onglet design
					that.loadMesh(rootMedia, nameMedia, editor.scene, false);
					editor.engine.resize();
				}, 500);
			}
			if(type == "scene") {
				// Ajout sur l'onglet design
				that.loadScene(rootMedia, nameMedia, editor.engine);
				// Ajout sur l'onglet media
				that.loadScene(rootMedia, nameMedia, media.engine);
			}
			else if(type == "textures") {
				$("#imageAppercue, #ControleVideo").show();
				$('#imageAppercue').html('<img src="'+media+'" style="position:absolute;" />');
				this.addExplorer({name:"textures"}, type, encodeURIComponent(JSON.stringify({"chemin":mesh})));
			}
			else if(type == "sounds") {
				$("#ControleSons").show();
				this.addExplorer({name:"sound"}, type, encodeURIComponent(JSON.stringify({"chemin":mesh})));
			}
			else if(type == "scripts") {
				$("#imageAppercue").show();
				this.txt = this.readTextFile(mesh);
				$('#imageAppercue').html(this.txt);
				this.addExplorer({name:"scripts"}, type, encodeURIComponent(JSON.stringify({"chemin":mesh})));
			}
		}
	};

	CASTORENGINE.addObjet.prototype.addMeshes = function()
	{
		var form =  '<div id="dialog-import-objet" title="'+lang.load["form-title-import-objet"]+'">'+
						'<form>'+
							'<label for="meshe">'+lang.load["form-import"]+' : </label><br /><input type="text" name="meshe" id="meshe" value="" size="50" class="text ui-widget-content ui-corner-all"><br />'+
							'<label for="partage" style="position:absolute;margin-top:6px;">'+lang.load["form-partage-objet"]+' : </label> <input type="checkbox" name="partage" id="partage" value="yes" style="width:20px;height:20px;position:absolute;margin-top:5px;margin-left:160px" class="text ui-widget-content ui-corner-all"><br /><br />'+
							'<label id="label_url_image_scene" for="url_image_objet" style="display:none;">'+lang.load["form-image-oblet"]+' : </label><br /><input type="text" name="url_image_objet" size="50" id="url_image_objet" value="" style="display:none;" class="text ui-widget-content ui-corner-all">'+
							'<input type="submit" style="position:absolute;top:-1000px">'+
						'</form>'+
					'</div><script>$("#partage").click(function() { if($("#partage").is(":checked")){ $("#url_image_objet, #label_url_image_scene").show(); } else {$("#url_image_objet, #label_url_image_scene").hide();} });</script>';
		layout.layoutW2uiDesing.content("main", {
			render: function() {
				$("#containeur").append(form);
				$("#dialog-import-objet").dialog({
					autoOpen: true, height: 220, width: 450, modal: true,
					buttons: {
						"Ajouter": function() {
							var urlObjet = $("#meshe").val();
							var urlImage = $("#url_image_objet").val();
							var rootObjet = urlObjet.replace(basename(urlObjet), "");
							var nameObjet = basename(urlObjet);
							that.loadMesh(rootObjet, nameObjet, editor.scene, false);
							if($("#partage").is(":checked")) {
								var that = this;
								$.ajax({ type: "POST", url: 'http://www.castorengine.com/partage_boutique.php', crossDomain: true, data: "urlObjet="+urlObjet+"&urlImageObjet="+urlImage,
									success: function (msg) {
										$(that).dialog("close");
									}
								});
							}
						}, Cancel: function() { $(this).dialog( "close" ); }
					}, close: function() { $(this).dialog( "close" ); }
				});
			}
		});
	};

	CASTORENGINE.addObjet.prototype.addScene = function()
	{
		var that = this;
		var form =  '<div id="dialog-import-objet" title="'+lang.load["form-title-import-scene"]+'">'+
						'<form>'+
							'<label for="scene">'+lang.load["form-import-scene"]+' : </label><br /><input type="text" name="scene" id="scene" value="" size="50" class="text ui-widget-content ui-corner-all"><br />'+
							'<label for="partage" style="position:absolute;margin-top:6px;">'+lang.load["form-partage-scene"]+' : </label> <input type="checkbox" name="partage" id="partage" value="yes" tyle="width:20px;height:20px;position:absolute;margin-top:5px;margin-left:160px" class="text ui-widget-content ui-corner-all"><br />'+
							'<label id="label_url_image_scene" for="url_image_scene" style="display:none;">'+lang.load["form-url-image-scene"]+' :</label><br /><input type="text" name="url_image_scene" size="50" id="url_image_scene" value="" style="display:none;" class="text ui-widget-content ui-corner-all">'+
							'<input type="submit" style="position:absolute;top:-1000px">'+
						'</form>'+
					'</div><script>$("#partage").click(function() { if($("#partage").is(":checked")){ $("#url_image_scene, #label_url_image_scene").show(); } else {$("#url_image_scene, #label_url_image_scene").hide();} });</script>';
		layout.layoutW2uiDesing.content("main", {
			render: function() {
				$("#containeur").append(form);
				$("#dialog-import-objet").dialog({
					autoOpen: true, height: 220, width: 450, modal: true,
					buttons: {
						"Ajouter": function() {
							var urlScene = $("#scene").val();
							var urlImage = $("#url_image_scene").val();
							var rootScene = urlObjet.replace(basename($("#scene").val()), "");
							var nameScene = basename($("#scene").val());
							that.loadScene(rootScene, nameScene, editor.engine);
							if($("#partage").is(":checked")) {
								var that = this;
								$.ajax({ type: "POST", url: 'http://www.castorengine.com/partage_boutique.php', crossDomain: true, data: "urlScene="+urlScene+"&urlImageScene="+urlImage,
									success: function (msg) {
										$(that).dialog("close");
									}
								});
							}
						}, Cancel: function() { $(this).dialog( "close" ); }
					}, close: function() { $(this).dialog( "close" ); }
				});
			}
		});
	};

	CASTORENGINE.addObjet.prototype.loadMesh = function(rootObjet, nameObjet, scene, isMedia)
	{
		var that = this;
		BABYLON.SceneLoader.ImportMesh("", rootObjet, nameObjet, scene, function (newmeshes, particleSystems, skeletons) {
			var mesh = newmeshes[0], bones = skeletons[0];
			mesh.isPickable = true;
			mesh.position = BABYLON.Vector3.Zero();
			if(isMedia == false) {				
				editor.selectEntity(mesh, "meshes");
			}
			that.addExplorer(mesh, "meshes", encodeURIComponent(JSON.stringify({chemin:rootObjet+nameObjet})), isMedia);
		});
	}

	CASTORENGINE.addObjet.prototype.loadScene = function(rootScene, nameScene, engine)
	{
		var that = this, strSceneData = null;
		if(rootScene != "") { strSceneData = nameScene; }
		else { strSceneData = 'data:' + nameScene; }
		
		BABYLON.SceneLoader.Load(rootScene, strSceneData, engine, function (newScene) {
			editor.scene.dispose();
			editor.scene = newScene;
			if(editor.scene.activeCamera) {
				editor.scene.activeCamera.attachControl(editor.canvas, false);
			}	
			if(editor.scene.meshes.length > 0) {
				editor.scene.meshes.forEach(function(meshes)
				{
					that.addExplorer(meshes, "scenes",  encodeURIComponent(JSON.stringify({chemin:rootScene+nameScene})), isMedia); // TODO: recuperer ici les types d'entités a ranger dans l'explorer.
					meshes.isPickable = true;
				});
			}
			editor.scene.registerBeforeRender(function()
			{
				editor.BeforeRender(editor);
			});			
			editor.scene.onPointerDown = function (evt, pickResult)
			{
				if (pickResult.hit && pickResult.pickedMesh) {
					var nameObjet = pickResult.pickedMesh.name;
					if (nameObjet != "x" && nameObjet != "y" && nameObjet != "z") {
						editor.selectEntity(pickResult.pickedMesh, "meshes");
					}
				}
			};			
		}, function (evt) {
			if (evt.lengthComputable) {
				editor.engine.loadingUIText = lang.load["loading"]+(evt.loaded * 100 / evt.total).toFixed() + "%";
			} else {
				dlCount = evt.loaded / (1024 * 1024);
				editor.engine.loadingUIText = lang.load["loading"] + Math.floor(dlCount * 100.0) / 100.0 + " MB";
			}
		});
	};

	CASTORENGINE.addObjet.prototype.addSkybox = function(size)
	{
		editor.skybox = BABYLON.Mesh.CreateBox("skyBox", size, editor.scene);
		editor.skybox.isPickable = false;
		this.skyboxMaterial = new BABYLON.StandardMaterial("skyBoxMaterial", editor.scene);
		this.skyboxMaterial.backFaceCulling = false;
		this.skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("data/textures/sky/TropicalSunnyDay", editor.scene);
		this.skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
		this.skyboxMaterial.diffuseColor = BABYLON.Color3.Black();
		this.skyboxMaterial.specularColor = BABYLON.Color3.Black();
		editor.skybox.material = this.skyboxMaterial;
		this.addExplorer(editor.skybox, "world");
	};

	CASTORENGINE.addObjet.prototype.addEmpty = function()
	{
		var countMesh = myObjet.length;
		editor.objets[countMesh] = new BABYLON.Mesh('Empty'+countMesh, editor.scene);
		editor.objets[countMesh].isPickable = true;
		editor.objets[countMesh].position = editor.scene.activeCamera.getFrontPosition(this.distanceFront);
		editor.selectEntity(editor.objets[countMesh], "meshes");
		this.addExplorer(editor.objets[countMesh], "primitifs");
	};

	CASTORENGINE.addObjet.prototype.addCamera = function(type)
	{
		switch (type) {
			case "free":
				var countFreeCamera = FreeCamera.length;
				FreeCamera[countFreeCamera] = new BABYLON.FreeCamera("Free"+countFreeCamera, new BABYLON.Vector3(0, 10, -15), editor.scene);
				editor.selectEntity(FreeCamera[countFreeCamera], "FreeCamera");
				this.addExplorer(FreeCamera[countFreeCamera], "world");
			break;
			case "rotative":
				var countArcRotateCamera = ArcRotateCamera.length;
				ArcRotateCamera[countArcRotateCamera] = new BABYLON.ArcRotateCamera("ArcRotate"+countArcRotateCamera, 1, 0.8, 10, new BABYLON.Vector3(0, 10, -15), editor.scene);
				editor.selectEntity(ArcRotateCamera[countArcRotateCamera], "ArcRotateCamera");
				this.addExplorer(ArcRotateCamera[countArcRotateCamera], "world");
			break;
			case "touch":
				var countTouchCamera = TouchCamera.length;
				TouchCamera[countTouchCamera] = new BABYLON.TouchCamera("Touch"+countTouchCamera, new BABYLON.Vector3(0, 10, -15), editor.scene);
				editor.selectEntity(TouchCamera[countTouchCamera], "TouchCamera");
				this.addExplorer(TouchCamera[countTouchCamera], "world");
			break;
			case "follow":
				var countFollowCamera = FollowCamera.length;
				FollowCamera[countFollowCamera] = new BABYLON.FollowCamera("Follow"+countFollowCamera, new BABYLON.Vector3(0, 10, -15), editor.scene);
				editor.selectEntity(FollowCamera[countFollowCamera], "FollowCamera");
				this.addExplorer(FollowCamera[countFollowCamera], "world");
			break;
			case "gamepad":
				var countGamepadCamera = GamepadCamera.length;
				GameadCamera[countGamepadCamera] = new BABYLON.GamepadCamera("Gamepad"+countGamepadCamera, new BABYLON.Vector3(0, 10, -15), editor.scene);
				editor.selectEntity(GameadCamera[countGamepadCamera], "GamePadCamera");
				this.addExplorer(GameadCamera[countGamepadCamera], "world");
			break;
		}
	};

	CASTORENGINE.addObjet.prototype.addLumieres = function(type)
	{
		var haloLight = null, modele = null, PositionLumiere = BABYLON.Vector3.Zero(), colorEmissive = new BABYLON.Color3(242 / 255, 230 / 255, 6 / 255);

		var matObjectLight = new BABYLON.StandardMaterial("MaterialLight", editor.scene);
		matObjectLight.diffuseColor = BABYLON.Color3.Black();
		matObjectLight.specularColor = BABYLON.Color3.Black();
		matObjectLight.ambientColor = BABYLON.Color3.Black();
		matObjectLight.emissiveColor = colorEmissive;
		matObjectLight.alpha = 1.0;

		var haloLightTexture = new BABYLON.StandardMaterial("MaterialLightDirectional", editor.scene);
		haloLightTexture.diffuseColor = BABYLON.Color3.Black();
		haloLightTexture.specularColor = BABYLON.Color3.Black();
		haloLightTexture.ambientColor = BABYLON.Color3.Black();
		haloLightTexture.emissiveColor = colorEmissive;
		haloLightTexture.alpha = 0.2;

		if(nameObjetSelected !== null) {
			modele = editor.objets[editor.objets.length].getBoundingInfo();
			PositionLumiere = new BABYLON.Vector3(modele.boundingBox.center.x, modele.boundingBox.maximum.y + 2, modele.boundingBox.center.z);
		} else {
			PositionLumiere = editor.scene.activeCamera.getFrontPosition(this.distanceFront);
		}

		editor.LightHemispheric.dispose();

		switch (type) {
			case "directionnnelle":
				var countLightDirectional = LightDirectional.length;
				LightDirectional.push("LightDirectional"+countLightDirectional);
				//## Gizmo
				parentLightDir[countLightDirectional] = BABYLON.Mesh.CreateSphere("LightDirectional"+countLightDirectional+"Gizmo", 25, 2.0, editor.scene);
				parentLightDir[countLightDirectional].isPickable = true;
				parentLightDir[countLightDirectional].material = matObjectLight;
				haloLight = BABYLON.Mesh.CreateSphere(countLightDirectional, 25, 5.0, editor.scene);
				haloLight.isPickable = false;
				haloLight.material = haloLightTexture;
				haloLight.parent = parentLightDir[countLightDirectional];
				//#####
				LightDirectional[countLightDirectional] = new BABYLON.DirectionalLight("LightDirectional"+countLightDirectional, new BABYLON.Vector3(0, -1, 0), editor.scene);
				LightDirectional[countLightDirectional].diffuse = new BABYLON.Color3(0.8, 0.8, 0.8);
				LightDirectional[countLightDirectional].specular = new BABYLON.Color3(0.68, 0.68, 0.68);
				LightDirectional[countLightDirectional].range = 1.8;
				LightDirectional[countLightDirectional].parent = parentLightDir[countLightDirectional];
				LightDirectional[countLightDirectional].parent.position = PositionLumiere;
				LightDirectional[countLightDirectional].position = PositionLumiere;
				editor.selectEntity(LightDirectional[countLightDirectional].parent, "Light");
				this.addExplorer(LightDirectional[countLightDirectional].parent, "lights_directionnnelle");
			break;
			case "point":
				var countLightPoint = LightPoint.length;
				LightPoint.push("LightPoint"+countLightPoint);
				//## Gizmo
				parentLightPoint[countLightPoint] = BABYLON.Mesh.CreateSphere("LightPoint"+countLightPoint+"Gizmo", 25, 1.0, editor.scene);
				parentLightPoint[countLightPoint].isPickable = true;
				parentLightPoint[countLightPoint].material = matObjectLight;
				haloLight = BABYLON.Mesh.CreateSphere(countLightPoint, 25, 2.0, editor.scene);
				haloLight.isPickable = false;
				haloLight.material = haloLightTexture;
				haloLight.parent = parentLightPoint[countLightPoint];
				//#####
				LightPoint[countLightPoint] = new BABYLON.PointLight("LightPoint"+countLightPoint, new BABYLON.Vector3(0, -1, 0), editor.scene);
				LightPoint[countLightPoint].diffuse = new BABYLON.Color3(0.8, 0.8, 0.8);
				LightPoint[countLightPoint].specular = new BABYLON.Color3(0.68, 0.68, 0.68);
				LightPoint[countLightPoint].range = 1.8;
				LightPoint[countLightPoint].parent = parentLightPoint[countLightPoint];
				LightPoint[countLightPoint].parent.position = PositionLumiere;
				LightPoint[countLightPoint].position = PositionLumiere;
				editor.selectEntity(LightPoint[countLightPoint].parent, "Light");
				this.addExplorer(LightPoint[countLightPoint].parent, "lights_point");
			break;
			case "hemispherique":
				var countLightHemispherique = LightHemispherique.length;
				LightHemispherique.push("LightHemispherique"+countLightHemispherique);
				//## Gizmo
				parentLightHemispherique[countLightHemispherique] = BABYLON.Mesh.CreateSphere("LightHemispherique"+countLightHemispherique+"Gizmo", 25, 1.0, editor.scene);
				parentLightHemispherique[countLightHemispherique].isPickable = true;
				parentLightHemispherique[countLightHemispherique].material = matObjectLight;
				haloLight = BABYLON.Mesh.CreateSphere(countLightHemispherique, 25, 3.0, editor.scene);
				haloLight.isPickable = false;
				haloLight.material = haloLightTexture;
				haloLight.parent = parentLightHemispherique[countLightHemispherique];
				//#####
				LightHemispherique[countLightHemispherique] = new BABYLON.HemisphericLight("LightHemispherique"+countLightHemispherique, new BABYLON.Vector3(0, -1, 0), editor.scene);
				LightHemispherique[countLightHemispherique].diffuse = new BABYLON.Color3(0.8, 0.8, 0.8);
				LightHemispherique[countLightHemispherique].specular = new BABYLON.Color3(0.68, 0.68, 0.68);
				LightHemispherique[countLightHemispherique].range = 1.8;
				LightHemispherique[countLightHemispherique].parent = parentLightHemispherique[countLightHemispherique];
				LightHemispherique[countLightHemispherique].parent.position = PositionLumiere;
				LightHemispherique[countLightHemispherique].position = PositionLumiere;
				editor.selectEntity(LightHemispherique[countLightHemispherique].parent, "Light");
				this.addExplorer(LightHemispherique[countLightHemispherique].parent, "lights_hemispherique");
			break;
			case "spot":
				var countLightSpot = LightSpot.length;
				LightSpot.push("LightSpot"+countLightSpot);
				//## Gizmo
				parentLightSpot[countLightSpot] = BABYLON.Mesh.CreateCylinder("LightSpot"+countLightSpot+"Gizmo", 2, 0.5, 2, 20, editor.scene);
				parentLightSpot[countLightSpot].isPickable = true;
				parentLightSpot[countLightSpot].material = matObjectLight;
				haloLight = BABYLON.Mesh.CreateCylinder(countLightSpot, 2, 2, 4, 20, editor.scene);
				haloLight.isPickable = false;
				haloLight.position.y = -2;
				haloLight.material = haloLightTexture;
				haloLight.parent = parentLightSpot[countLightSpot];
				//#####
				LightSpot[countLightSpot] = new BABYLON.SpotLight("LightSpot"+countLightSpot, new BABYLON.Vector3(0, 0, 0), new BABYLON.Vector3(0, -1, 0), 1, 1, editor.scene);
				LightSpot[countLightSpot].diffuse = new BABYLON.Color3(0.8, 0.8, 0.8);
				LightSpot[countLightSpot].specular = new BABYLON.Color3(0.68, 0.68, 0.68);
				LightSpot[countLightSpot].range = 1.8;
				LightSpot[countLightSpot].parent = parentLightSpot[countLightSpot];
				parentLightSpot[countLightSpot].position = PositionLumiere;
				LightSpot[countLightSpot].parent.position = PositionLumiere;
				editor.selectEntity(LightSpot[countLightSpot].parent, "Light");
				this.addExplorer(LightSpot[countLightSpot].parent, "lights_spot");
			break;
		}
	};

	CASTORENGINE.addObjet.prototype.addInstancyMesh = function(objet)
	{
		var countMesh = myObjet.length;
		//var objet = editor.scene.getMeshByName(name);
		var id = objet.name;
		editor.objets[countMesh] = objet.createInstance(objet.name+countMesh);
		editor.objets[countMesh].id = id;
		editor.objets[countMesh].position = new BABYLON.Vector3(objet.position.x + 1, objet.position.y, objet.position.z + 1);
		editor.objets[countMesh].isPickable = true;
		editor.selectEntity(editor.objets[countMesh], "meshes");
		this.addExplorer(editor.objets[countMesh], "primitifs");
	};

	CASTORENGINE.addObjet.prototype.addCloneMesh = function(objet)
	{
		var countMesh = myObjet.length;
		//var objet = editor.scene.getMeshByName(name);
		var id = objet.name;
		editor.objets[countMesh] = objet.clone(objet.name+countMesh);
		editor.objets[countMesh].id = id;
		editor.objets[countMesh].position = new BABYLON.Vector3(objet.position.x + 1, objet.position.y, objet.position.z + 1);
		editor.objets[countMesh].isPickable = true;
		editor.selectEntity(editor.objets[countMesh], "meshes");
		this.addExplorer(editor.objets[countMesh], "primitifs");
	};

	CASTORENGINE.addObjet.prototype.addSprites = function()
	{
		var countMesh = myObjet.length;
		var countSpriteManager = spriteManager.length;
		spriteManager[countSpriteManager] = new BABYLON.SpriteManager("spriteManager"+countSpriteManager, "Images/Logo/logo_castor_black.png", 2000, 128, editor.scene);
		editor.objets[countMesh] = new BABYLON.Sprite('Sprite'+countMesh, spriteManager[countSpriteManager]);
		editor.objets[countMesh].isPickable = true;
		editor.objets[countMesh].position = editor.scene.activeCamera.getFrontPosition(this.distanceFront);
		editor.selectEntity(editor.objets[countMesh], "meshes");
		this.addExplorer(editor.objets[countMesh], "sprites");
	};

	CASTORENGINE.addObjet.prototype.addCube = function()
	{
		var countMesh = myObjet.length;
		editor.objets[countMesh] = BABYLON.Mesh.CreateBox("Box"+countMesh, 2.0, editor.scene, true);
		editor.objets[countMesh].isPickable = true;
		editor.objets[countMesh].position = editor.scene.activeCamera.getFrontPosition(this.distanceFront);
		editor.selectEntity(editor.objets[countMesh], "meshes");
		this.addExplorer(editor.objets[countMesh], "primitifs");
	};

	CASTORENGINE.addObjet.prototype.addSphere = function()
	{
		var countMesh = myObjet.length;
		editor.objets[countMesh] = BABYLON.Mesh.CreateSphere("Sphere"+countMesh, 16.0, 2.0, editor.scene, true);
		editor.objets[countMesh].isPickable = true;
		editor.objets[countMesh].position = editor.scene.activeCamera.getFrontPosition(this.distanceFront);
		editor.selectEntity(editor.objets[countMesh], "meshes");
		this.addExplorer(editor.objets[countMesh], "primitifs");
	};

	CASTORENGINE.addObjet.prototype.addPlan = function()
	{
		var countMesh = myObjet.length;
		editor.objets[countMesh] = BABYLON.Mesh.CreatePlane("Plan"+countMesh, 5.0, editor.scene);
		editor.objets[countMesh].isPickable = true;
		editor.objets[countMesh].position = editor.scene.activeCamera.getFrontPosition(this.distanceFront);
		editor.selectEntity(editor.objets[countMesh], "meshes");
		this.addExplorer(editor.objets[countMesh], "primitifs");
	};

	CASTORENGINE.addObjet.prototype.addCylindre = function()
	{
		var countMesh = myObjet.length;
		editor.objets[countMesh] = BABYLON.Mesh.CreateCylinder("Cylindre"+countMesh, 3, 2, 2, 16, 1, editor.scene, true);
		editor.objets[countMesh].isPickable = true;
		editor.objets[countMesh].position = editor.scene.activeCamera.getFrontPosition(this.distanceFront);
		editor.selectEntity(editor.objets[countMesh], "meshes");
		this.addExplorer(editor.objets[countMesh], "primitifs");
	};

	CASTORENGINE.addObjet.prototype.addTorus = function()
	{
		var countMesh = myObjet.length;
		editor.objets[countMesh] = BABYLON.Mesh.CreateTorus("Torus"+countMesh, 2, 1, 16, editor.scene, true);
		editor.objets[countMesh].isPickable = true;
		editor.objets[countMesh].position = editor.scene.activeCamera.getFrontPosition(this.distanceFront);
		editor.selectEntity(editor.objets[countMesh], "meshes");
		this.addExplorer(editor.objets[countMesh], "primitifs");
	};

	CASTORENGINE.addObjet.prototype.addLine = function()
	{
		var countMesh = myObjet.length;
		//editor.objets[countMesh] = BABYLON.Mesh.
		editor.objets[countMesh].isPickable = true;
		editor.objets[countMesh].position = editor.scene.activeCamera.getFrontPosition(this.distanceFront);
		editor.selectEntity(editor.objets[countMesh], "meshes");
		this.addExplorer(editor.objets[countMesh], "primitifs");
	};

	CASTORENGINE.addObjet.prototype.addGround = function()
	{
		var countMesh = myObjet.length;
		//editor.objets[countMesh] = BABYLON.Mesh.
		editor.objets[countMesh].isPickable = true;
		editor.objets[countMesh].position = editor.scene.activeCamera.getFrontPosition(this.distanceFront);
		editor.selectEntity(editor.objets[countMesh], "meshes");
		this.addExplorer(editor.objets[countMesh], "primitifs");
	};

	CASTORENGINE.addObjet.prototype.addSprites = function()
	{
		var countMesh = meshPerso.length;
		var countSpriteManager = spriteManager.length;
		spriteManager[countSpriteManager] = new BABYLON.SpriteManager("spriteManager"+countSpriteManager, "Images/Logo/logo_castor_black.png", 2000, 128, scene);
		editor.objets[countMesh] = new BABYLON.Sprite('Sprite'+countMesh, spriteManager[countSpriteManager]);
		editor.selectEntity(editor.objets[countMesh], "sprite");
		this.addExplorer(editor.objets[countMesh], "sprites");
	};

	//***************** Composants sur scene ou objets

	CASTORENGINE.addObjet.prototype.addLensFlare = function()
	{
		if(nameObjetSelected) {
			var LensFlareCount = LensFlare.length;
			lensFlareSystem[LensFlareCount] = new BABYLON.LensFlareSystem("lensFlareSystem"+LensFlareCount, editor.objets[nameObjetSelected], editor.scene);
			LensFlare[LensFlareCount] = new BABYLON.LensFlare(0.5, 1, BABYLON.Color3.White(), "images/none.png", lensFlareSystem[LensFlareCount]);
			editor.properties.showLensFlare();
			this.addExplorer(lensFlareSystem[LensFlareCount], "materials");
		} else {
			this.console(lang.load["console-add-entiter"], 2);
		}
	};

	CASTORENGINE.addObjet.prototype.optionOptimizer = function()
	{
		var result = new BABYLON.SceneOptimizerOptions(40, 3000);
		result.optimizations.push(new BABYLON.ShadowsOptimization(0));
		result.optimizations.push(new BABYLON.LensFlaresOptimization(1));
		result.optimizations.push(new BABYLON.TextureOptimization(2, 256));
		result.optimizations.push(new BABYLON.PostProcessesOptimization(3));
		result.optimizations.push(new BABYLON.ParticlesOptimization(4));
		result.optimizations.push(new BABYLON.RenderTargetsOptimization(5));
		result.optimizations.push(new BABYLON.HardwareScalingOptimization(6, 4));
		return result;
	};

	CASTORENGINE.addObjet.prototype.addSceneOptimisation = function()
	{
		if($(".explorer").find("a#Optimizer").text() == true) {
			this.console(lang.load["console-add-optimizer"], 2);
		} else {
			this.optionOptimizer();
			editor.properties.showOptimizer();
			this.addExplorer({name:"Optimizer"}, "scenes");
		}
	};

	CASTORENGINE.addObjet.prototype.addFog = function()
	{
		if($(".explorer").find("a#Fog").text() == true) {
			this.console(lang.load["console-add-fog"], 2);
		} else {
			editor.scene.fogColor = new BABYLON.Color3(0.7, 0.7, 0.7);
			editor.scene.fogMode = BABYLON.Scene.FOGMODE_LINEAR;
			editor.scene.fogStart = json.fog.start;
			editor.scene.fogEnd = json.fog.end;
			editor.scene.fogDensity = 0.005;
			editor.properties.showFog();
			this.addExplorer({name:"Fog"}, "scenes");
		}
	};

	CASTORENGINE.addObjet.prototype.addEnvironnement = function()
	{
		if($(".explorer").find("a#Environnement").text() == true) {
			this.console(lang.load["console-add-environnement"], 2);
		} else {
			editor.properties.showEnvironnement();
			this.addExplorer({name:"Environnement"}, "scenes");
		}
	};

	CASTORENGINE.addObjet.prototype.addEditionScene = function()
	{
		if($(".explorer").find("a#Scene").text() == true) {
			this.console(lang.load["console-add-scene"], 2);
		} else {
			editor.properties.showScene();
			this.addExplorer({name:"projets"}, "scenes");
		}
	};

	CASTORENGINE.addObjet.prototype.addEditionEngine = function()
	{
		if($(".explorer").find("a#Engine").text() == true) {
			this.console(lang.load["console-add-engine"], 2);
		} else {
			editor.properties.showEngine();
			this.addExplorer({name:"Engine"}, "scenes");
		}
	};

	CASTORENGINE.addObjet.prototype.addRigidBody = function()
	{
		if(nameObjetSelected) {
			var modele = editor.objets[nameObjetSelected];
			modele.ellipsoid = new BABYLON.Vector3(0.75, 1.0, 0.75);
			modele.ellipsoidOffset = new BABYLON.Vector3(0, 2.01, 0);
			modele.applyGravity = true;
			editor.properties.showRigidBody();
			this.addExplorer(modele, "rigidbody");
		} else {
			this.console(lang.load["console-add-entiter"], 2);
		}
	};

	CASTORENGINE.addObjet.prototype.addCollision = function()
	{
		if(nameObjetSelected) {
			editor.objets[nameObjetSelected].checkCollisions = false;
			var countModeleCollider = modeleCollider.length;
			ColliderMesh[editor.objets[nameObjetSelected].name] = new CASTORENGINE.collider("Collider"+editor.objets[nameObjetSelected].name, false, editor.objets[nameObjetSelected]);
			editor.properties.showCollider();
			this.addExplorer(ColliderMesh[editor.objets[nameObjetSelected]], "world");
		} else {
			this.console(lang.load["console-add-entiter"], 2);
		}
	};

	CASTORENGINE.addObjet.prototype.addPostProcess = function(type)
	{
		//editor.scene.activeCamera.attachPostProcess
		if(type == "FXAA") {
			var PostProcess = new BABYLON.FxaaPostProcess("Fxaa", 0.77, editor.scene.activeCamera, BABYLON.Texture.TRILINEAR_SAMPLINGMODE, editor.engine, false);
			this.addExplorer(PostProcess, "portProcess");
		}
		if(type == "SSAO") {
			var ssao = new BABYLON.SSAORenderingPipeline('SSAOPipeline', editor.scene, 0.77);
			editor.scene.postProcessRenderPipelineManager.attachCamerasToRenderPipeline("SSAOPipelineManager", editor.scene.activeCamera);
			editor.scene.postProcessRenderPipelineManager.enableEffectInPipeline("SSAOPipelineEnableEffect", ssao.SSAOCombineRenderEffect, editor.scene.activeCamera);
			this.addExplorer(ssao, "portProcess");
		}
	};

	CASTORENGINE.addObjet.prototype.addMaterial = function()
	{
		var countMaterial = materials.length;
		materials[countMaterial] = new BABYLON.StandardMaterial("texture"+countMaterial, editor.scene);
		editor.properties.showMaterial();
		this.addExplorer(materials[countMaterial].name, "materials");
	};

	CASTORENGINE.addObjet.prototype.addProcedural = function()
	{
		var countMaterialProcedural = materialsProcedural.length;
		materialsProcedural[countMaterialProcedural] = new BABYLON.StandardMaterial("materialProcedural"+countMaterialProcedural, editor.scene);
		editor.properties.showProcedural();
		this.addExplorer(materialsProcedural[countMaterialProcedural].name, "procedural");
	};

	CASTORENGINE.addObjet.prototype.addParticules = function()
	{
		var countParticleSystem = particleSystem.length;
		particleSystem[countParticleSystem] = new BABYLON.ParticleSystem("particles"+countParticleSystem, 2000, editor.scene);
		editor.properties.showParticulesSystem();
		this.addExplorer(particleSystem[countParticleSystem].name, "particules");
	};

	CASTORENGINE.addObjet.prototype.addShape = function(type)
	{
		var Impostor = null;
		if(type == "box") Impostor = BABYLON.PhysicsEngine.BoxImpostor;
		if(type == "sphere") Impostor = BABYLON.PhysicsEngine.SphereImpostor;
		if(type == "heightmap") Impostor = BABYLON.PhysicsEngine.HeightmapImpostor;
		if(nameObjetSelected) {
			editor.objets[nameObjetSelected].setPhysicsState(Impostor, { mass: 0, friction: 0, restitution: 0 });
			editor.properties.showPhysic();
			this.addExplorer(editor.objets[nameObjetSelected], "shape");
		} else {
			this.console(lang.load["console-add-entiter"], 2);
		}
	};

	CASTORENGINE.addObjet.prototype.addScript = function()
	{
		var ScriptsCount = Scripts.length;
		Scripts.push("Script"+ScriptsCount);
		editor.properties.showScript();
		// TODO ajouter le script dans l'onglet script et rafraichir la liste
		this.addExplorer({name:"Script"+ScriptsCount+".js"}, "scripts");
	};

	CASTORENGINE.addObjet.prototype.addSound = function()
	{
		//TODO:  Ouvrir une boite de dialogue ici pour importer des sons et musiques par url
		var countSound = sounds.length, audioDefaut = null;
		if(mp3 == true) {
			audioDefaut = "data/music.mp3";
		} else {
			audioDefaut = "data/music.ogg";
		}
		sounds[countSound] = new BABYLON.Sound("sound"+countSound, audioDefaut, editor.scene, null, { loop: true, autoplay: true, streaming: true });
		editor.properties.showSound();
		this.addExplorer(sounds[countSound].name, "sounds");
	};

	CASTORENGINE.addObjet.prototype.addVideo = function()
	{
		//TODO:  Ouvrir une boite de dialogue ici pour importer des videos par url
		//this.addExplorer("??", "videos");
	};

	CASTORENGINE.addObjet.prototype.addReseau = function()
	{
		if(nameObjetSelected) {

			//this.addExplorer("??", "reseau");
		} else {
			this.console(lang.load["console-add-entiter"], 2);
		}
	};

})();