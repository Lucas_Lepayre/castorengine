var CASTORENGINE = CASTORENGINE || {};

(function () {

	CASTORENGINE.layout = function()
	{
		this.layoutW2uiDesing = null;
		this.layoutW2uiCode = null;
		this.layoutW2uiStore = null;
		this.consoleIsOpen = false;
		this.storeIsOpen = true;
		this.propertiesIsOpen = true;
		this.layoutStyle = 'background-color:transparent; border: 1px solid #555555; padding:0px; magin:0px;';
		this.iconsAccordion = { header: "ui-icon-circle-arrow-e", activeHeader: "ui-icon-circle-arrow-s" };

		this.explorerDesing = "<h4>"+lang.load["layout-projets"]+" (<span id='count_projet3d'>0</span>)</h4><div id='ex_projet3d'></div>"+
							"<h4>"+lang.load["layout-world"]+" (<span id='count_world'>0</span>)</h4><div id='ex_world'></div>"+
							"<h4>"+lang.load["layout-shape"]+" (<span id='count_shapes'>0</span>)</h4><div id='ex_shapes'></div>"+
							"<h4>"+lang.load["layout-primitifs"]+" (<span id='count_primitifs'>0</span>)</h4><div id='ex_primitifs'></div>"+
							"<h4>"+lang.load["layout-material"]+" (<span id='count_materials'>0</span>)</h4><div id='ex_materials'></div>"+
							"<h4>"+lang.load["layout-sprites"]+" (<span id='count_sprites'>0</span>)</h4><div id='ex_sprites'></div>"+
							"<h4>"+lang.load["layout-light"]+" (<span id='count_lights'>0</span>)</h4><div id='ex_lights'></div>"+
							"<h4>"+lang.load["layout-video"]+" (<span id='count_video'>0</span>)</h4><div id='ex_video'></div>"+
							"<h4>"+lang.load["layout-procedural"]+" (<span id='count_procedural'>0</span>)</h4><div id='ex_procedural'></div>"+
							"<h4>"+lang.load["layout-particle"]+" (<span id='count_particle'>0</span>)</h4><div id='ex_particles'></div>";
		this.explorerMedia = "<h4>"+lang.load["layout-meshes"]+" (<span id='count_meshes'>0</span>)</h4><div id='ex_meshes'></div>"+
							"<h4>"+lang.load["layout-scene"]+" (<span id='count_scenes'>0</span>)</h4><div id='count_scenes'></div>"+
							"<h4>"+lang.load["layout-sounds"]+" (<span id='count_sounds'>0</span>)</h4><div id='ex_sounds'></div>"+
							"<h4>"+lang.load["layout-textures"]+" (<span id='count_textures'>0</span>)</h4><div id='ex_textures'></div>";
		this.explorerCodeBase = "<h4>"+lang.load["layout-scripts"]+" (<span id='count_scripts'>0</span>)</h4><div id='ex_scripts'></div>";
		this.explorerCodeExterne = "<h4>"+lang.load["layout-ref"]+" (<span id='count_references'>0</span>)</h4><div id='ex_references'></div>";

		this.tabs();
		this.layoutContent();
		this.explorer();
		this.contextMenu();

		this.addObjet = new CASTORENGINE.addObjet();
	};

	CASTORENGINE.layout.prototype.explorer = function()
	{
		$(".explorer").accordion({
			icons: this.iconsAccordion,
			collapsible: true,
			heightStyle: "content",
			active: false
		});
		$(".explorer_code").accordion({
			icons: this.iconsAccordion,
			collapsible: true,
			heightStyle: "content",
			active: false
		});
		$(".explorer_media").accordion({
			icons: this.iconsAccordion,
			collapsible: true,
			heightStyle: "content",
			active: false
		});
	};

	CASTORENGINE.layout.prototype.tabs = function()
	{
		$("#tabsEditor").tabs({
			activate: function (event, ui) {
				ui.newTab.blur();
			},
			active:0
		});
		$('#tabsEditor').on('tabsactivate', function(event, ui) {
			var newIndex = ui.newTab.index();
			if(newIndex == 0) {
				editor.engine.resize();
				$("#renderEditor").focus().trigger("click");
			} else if(newIndex == 2) {
				media.engine.resize();
				$("#renderMedia").focus().trigger("click");
			}			
		});		
		$('#tabsEditor a').click(function () {
			$(this).blur();
		});
	};

	CASTORENGINE.layout.prototype.openConsole = function(value)
	{
		if(this.consoleIsOpen == false) {
			this.layoutW2uiDesing.show('bottom', true);
			this.consoleIsOpen = true;
		} else {
			this.layoutW2uiDesing.hide('bottom', true);
			this.consoleIsOpen = false;
		}
	};


	CASTORENGINE.layout.prototype.openProperties = function(value)
	{
		if(this.propertiesIsOpen == false) {
			this.layoutW2uiDesing.show('right', true);
			this.propertiesIsOpen = true;
		} else {
			this.layoutW2uiDesing.hide('right', true);
			this.propertiesIsOpen = false;
		}
	};

	CASTORENGINE.layout.prototype.openStore = function(value)
	{
		if(this.storeIsOpen == false) {
			this.layoutW2uiStore.show('right', true);
			this.storeIsOpen = true;
		} else {
			this.layoutW2uiStore.hide('right', true);
			this.storeIsOpen = false;
		}
	};

	CASTORENGINE.layout.prototype.layoutContent = function()
	{
		// Layout desing
		$('.layout-desing').w2layout({
			name: 'layout-desing',
			panels: [
				{ type: 'left', size: 250, resizable: true, style: this.layoutStyle},
				{ type: 'main', style: this.layoutStyle},
				{ type: 'right', size: 380, resizable: true, style: this.layoutStyle},
				{ type: 'bottom', size: 250, resizable: true, style: this.layoutStyle}
			]
		});
		this.layoutW2uiDesing = w2ui['layout-desing'];
		this.layoutW2uiDesing.content('left', '<div class="explorer">'+this.explorerDesing + this.explorerMedia+'</div>');
		this.layoutW2uiDesing.content('main', '<div id="containeur"><canvas id="renderEditor"></canvas><div id="show_stats_scene"></div><img src="images/attention.png" id="alertOnDesing" onClick="layout.openConsole(100);" /></div></div>');
		this.layoutW2uiDesing.content('right', '<div id="propGrid"></div>');
		this.layoutW2uiDesing.content('bottom', '<div id="console_desing"></div>');
		this.layoutW2uiDesing.hide('bottom', true);

		// Layout code
		$('.layout-code').w2layout({
			name: 'layout-code',
			panels: [
				{ type: 'left', size: 250, resizable: true, style: this.layoutStyle},
				{ type: 'main', style: this.layoutStyle},
				{ type: 'bottom', size: 200, resizable: false, style: this.layoutStyle}
			]
		});
		this.layoutW2uiCode = w2ui['layout-code'];
		this.layoutW2uiCode.content('left', '<div class="explorer_code">'+this.explorerCodeExterne + this.explorerCodeBase+'</div>');
		this.layoutW2uiCode.content('main', '<iframe src="scripts.php" align="center" width="100%" height="100%" frameborder="0" scrolling="no" id="iframe3" name="iframe3"></iframe>');
		this.layoutW2uiCode.content('bottom', '<div id="console_script">'+lang.load["layout-console-error"]+' : 0</div>');

		// Layout store
		$('.layout-store').w2layout({
			name: 'layout-store',
			panels: [
				{ type: 'left', size: 250, resizable: true, style: this.layoutStyle},
				{ type: 'main', style: this.layoutStyle},
				{ type: 'right', size: 600, resizable: true, style: this.layoutStyle}
			]
		});

		var mediaControl = '<div id="ControleSons">'+
			'<button id="PlaySons">'+lang.load["layout-control-audio-play"]+'</button> <button id="PauseSons">'+lang.load["layout-control-audio-pause"]+'</button> <button id="StopSons">'+lang.load["layout-control-audio-stop"]+'</button><br />'+
			'<span style="float:left;margin-top:10px;">Volume : </span><div id="masterVolumeSons" style="width:260px;margin:15px;float:left;"></div>'+
		'</div>'+
		'<div id="ControleVideo">'+
			'<button id="PlayVideo">'+lang.load["layout-control-audio-play"]+'</button> <button id="PauseVideo">'+lang.load["layout-control-audio-pause"]+'</button> <button id="StopVideo">'+lang.load["layout-control-audio-stop"]+'</button><br />'+
			'<span style="float:left;margin-top:10px;">Volume : </span><div id="masterVolumeVideo" style="width:260px;margin:15px;float:left;"></div>'+
		'</div>'+
		'<div id="imageAppercue" style"width:100%; height:100%;"></div>';

		this.layoutW2uiStore = w2ui['layout-store'];
		this.layoutW2uiStore.content('left', '<div class="explorer_media">'+this.explorerMedia + this.explorerCodeBase+'</div>');
		this.layoutW2uiStore.content('main', '<canvas id="renderMedia"></canvas>'+mediaControl);
		this.layoutW2uiStore.content('right', '<iframe src="http://www.castorengine.com/boutique.php" align="center" width="100%" height="100%" frameborder="0" scrolling="yes" id="iframe3" name="iframe3"></iframe>');
	};

	CASTORENGINE.layout.prototype.contextMenu = function()
	{
		var that = this;
		this.menuListeCreat = {
			"options": {"name": ""+lang.load["layout-context-menu-options"]+"", "icon": "edit",
							"items": {
								"sceneOptimisation": {"name": ""+lang.load["layout-context-menu-optimizer"]+"", "icon": "edit"}, "step1":"--------------",
								"fog": {"name": ""+lang.load["layout-context-menu-fog"]+"", "icon": "edit"}, "step2.1":"--------------",
								"environnement": {"name": ""+lang.load["layout-context-menu-environnement"]+"", "icon": "edit"},	"step2":"--------------",
								"scene": {"name": ""+lang.load["layout-context-menu-scene"]+"", "icon": "edit"}, "step3":"--------------",
								"engine": {"name": ""+lang.load["layout-context-menu-engine"]+"", "icon": "edit"}
							}
						}, "step4":"--------------",
			"camera": {"name": ""+lang.load["layout-context-menu-camera"]+"", "icon": "edit",
							"items": {
								"free": {"name": "FreeCamera", "icon": "edit"}, "step5":"--------------",
								"rotative": {"name": "ArcRotateCamera", "icon": "edit"}, "step6":"--------------",
								"gamepad": {"name": "GamepadCamera", "icon": "edit"}, "step7":"--------------",
								"touch": {"name": "TouchCamera", "icon": "edit"}, "step71":"--------------",
								"follow": {"name": "FollowCamera", "icon": "edit"}
							}
						}, "step8":"--------------",
			"lights": {"name": ""+lang.load["layout-context-menu-light"]+"", "icon": "edit",
							"items": {
								"spot": {"name": "SpotLight", "icon": "edit"}, "step9":"--------------",
								"point": {"name": "PointLigth", "icon": "edit"}, "step10":"--------------",
								"directionnnelle": {"name": "DirectionnalLight", "icon": "edit"}, "step11":"--------------",
								"hemispherique": {"name": "HemispheriqueLight", "icon": "edit"}
							}
						}, "step12":"--------------",
			"meshes": {"name": "Meshes", "icon": "edit",
							"items": {
								"primitifs": {"name": ""+lang.load["layout-context-menu-primitif"]+"", "icon": "edit",
												"items": {
													"empty": {"name": "Empty", "icon": "edit"}, "step16":"--------------",
													"box": {"name": "Box", "icon": "edit"}, "step17":"--------------",
													"sphere": {"name": "Sphere", "icon": "edit"}, "step18":"--------------",
													"cylindre": {"name": "Cylindre", "icon": "edit"}, "step19":"--------------",
													"torus": {"name": "Torus", "icon": "edit"}, "step20":"--------------",
													"tube": {"name": "Tube", "icon": "edit"}, "step21":"--------------",
													"plan": {"name": "Plan", "icon": "edit"}, "step22":"--------------",
													"line": {"name": "Ligne", "icon": "edit"}, "step23":"--------------",
													"ground": {"name": "Ground", "icon": "edit"}
												}
								}, "step25":"--------------",
								"sprite": {"name": "Sprite", "icon": "edit"}, "step26":"--------------",
								"importMesh": {"name": "import Meshes", "icon": "edit"}, "step27":"--------------",
								"importScene": {"name": "import Scene", "icon": "edit"}
							}
						}, "step28":"--------------",
			"physics": {"name": ""+lang.load["layout-context-menu-physics"]+"", "icon": "edit",
							"items": {
								"shapes": {"name": ""+lang.load["layout-context-menu-shapes"]+"", "icon": "edit",
											"items": {
												"sphereshape": {"name": "Sphere", "icon": "edit"}, "step31":"--------------",
												"boxshape": {"name": "Box", "icon": "edit"}, "step31.2":"--------------",
												"heightmapshape": {"name": "Heightmap", "icon": "edit"}
											}
								}, "step32":"--------------",
								"rigidBody": {"name": "RigidBody", "icon": "edit"}
							}
						}, "step37":"--------------",
			"post-process": {"name": "Post-Process", "icon": "edit",
							"items": {
								"FXAA": {"name": "FXAA", "icon": "edit"}, "step37.1":"--------------",
								"SSAO": {"name": "SSAO", "icon": "edit"}
							}
						}, "step38":"--------------",
			"collision": {"name": ""+lang.load["layout-context-menu-collision"]+"", "icon": "edit"}, "step33":"--------------",
			"material": {"name": ""+lang.load["layout-context-menu-material-standard"]+"", "icon": "edit"}, "step39":"--------------",
			"procedural": {"name": ""+lang.load["layout-context-menu-procedural"]+"", "icon": "edit"}, "step40":"--------------",
			"particules": {"name": ""+lang.load["layout-context-menu-particle"]+"", "icon": "edit"}, "step41":"--------------",
			"lensflare": {"name": ""+lang.load["layout-context-menu-flare"]+"", "icon": "edit"}, "step42":"--------------",
			"sound": {"name": ""+lang.load["layout-context-menu-sound"]+"", "icon": "edit"}, "step43":"--------------",
			"video": {"name": ""+lang.load["layout-context-menu-video"]+"", "icon": "edit"}, "step44":"--------------",
			"networkview": {"name": ""+lang.load["layout-context-menu-reseau"]+"", "icon": "edit"}, "step45":"--------------",
			"script": {"name": ""+lang.load["layout-context-menu-script"]+" (*.js)", "icon": "edit"}
		};
		$.contextMenu({ selector: '#containeur', callback: function(key, options) { that.addObjet.add(key); }, items: this.menuListeCreat });
	};

})();