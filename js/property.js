var CASTORENGINE = CASTORENGINE || {};

(function () {

	CASTORENGINE.properties = function()
	{
		this.cpOptions = { preferredFormat:'rgb', showInput:true, showInitial:true, showAlpha:true, };
		this.index = 0;
		this.editeObjet = new CASTORENGINE.editeObjet();
	};

	CASTORENGINE.properties.prototype.creat = function(theObj, theMeta)
	{
		$('#propGrid').html('');
		$('#propGrid').PropertyGrid(theObj, theMeta);
	};

	CASTORENGINE.properties.prototype.getIndex = function(array, value)
	{
		this.index = array.indexOf(media);
		return this.index;
	};

	CASTORENGINE.properties.prototype.showMesh = function()
	{
		this.theObj = {
			type:'meshes',
			name:'',
			id:0,
			tags:'',
			renderingGroupId:0,
			positionx:0, positiony:0, positionz:0,
			rotationx:0, rotationy:0, rotationz:0,
			scalex:0, scaley:0, scalez:0,
			visibility:true,
			parent:'',
			physic:'',
			particules:'',
			checkCollisions:false,
			castShadow:false,
			recieveShadows:false,
			lookat:false,
			optimize:true,
			isPickable:false,
			isBlocker:false,
			layerMask:0,
			rotationQuaternion:true,
			sounds:'',
			material:'',
			LODLow:'',
			LODLowDistance:500,
			LODMedium:'',
			LODMediumDistance:250,
			LODHeight:'',
			LODHeightDistance:100,
			scripts:'',
			actions:'',
			networkview:''
		};
		this.theMeta = {
			type:{group:lang.load["properties-group-general"], name:'Type', disabled:true},
			name:{group:lang.load["properties-group-general"], name:'Name'},
			id:{group:lang.load["properties-group-general"], name:'ID'},
			tags:{group:lang.load["properties-group-general"], name:'Tags'},
			renderingGroupId:{group:lang.load["properties-group-general"], name:'Rendering Group Id', type:'number', description:lang.load["properties-help-zindex"]},

			positionx:{group:lang.load["properties-group-position"], name:'X', type:'number'},
			positiony:{group:lang.load["properties-group-position"], name:'Y', type:'number'},
			positionz:{group:lang.load["properties-group-position"], name:'Z', type:'number'},

			rotationx:{group:lang.load["properties-group-rotation"], name:'X', type:'number'},
			rotationy:{group:lang.load["properties-group-rotation"], name:'Y', type:'number'},
			rotationz:{group:lang.load["properties-group-rotation"], name:'Z', type:'number'},

			scalex:{group:lang.load["properties-group-scaling"], name:'X', type:'number'},
			scaley:{group:lang.load["properties-group-scaling"], name:'Y', type:'number'},
			scalez:{group:lang.load["properties-group-scaling"], name:'Z', type:'number'},

			visibility:{group:lang.load["properties-group-objet"], name:'Visibility', type:'boolean', description:lang.load["properties-help-visibility"]},
			parent:{group:lang.load["properties-group-objet"], name:'Parent', type:'liste', id:'meshesListe', description:lang.load["properties-help-parent"]},
			physic:{group:lang.load["properties-group-objet"], name:'Physic', type:'options', options:[{text:'None', value:'0'}]},
			particules:{group:lang.load["properties-group-objet"], name:'Particle System', type:'liste', id:'particulesListe'},
			checkCollisions:{group:lang.load["properties-group-objet"], name:'Collision', type:'boolean', description:lang.load["properties-help-collision"]},
			castShadow:{group:lang.load["properties-group-objet"], name:lang.load["properties-castshadow"], type:'boolean'},
			recieveShadows:{group:lang.load["properties-group-objet"], name:lang.load["properties-recieveshadow"], type:'boolean'},
			lookat:{group:lang.load["properties-group-objet"], name:'Look at', type:'boolean', description:lang.load["properties-help-lookat"]},
			optimize:{group:lang.load["properties-group-objet"], name:'Optimizer', type:'boolean', description:lang.load["properties-help-optimize"]},
			isPickable:{group:lang.load["properties-group-objet"], name:'Is Pickable', type:'boolean'},
			isBlocker:{group:lang.load["properties-group-objet"], name:'Is Blocker', type:'boolean', description:lang.load["properties-help-blocker"]},
			layerMask:{group:lang.load["properties-group-objet"], name:'Layer Mask', type:'number', description:lang.load["properties-help-layermask"]},
			rotationQuaternion:{group:lang.load["properties-group-objet"], name:'No Rotation Quaternion', type:'boolean', description:lang.load["properties-help-noquaternion"]},
			sounds:{group:lang.load["properties-group-objet"], name:'Sounds', type:'options', options:[{text:'None', value:'0'}]},
			material:{group:lang.load["properties-group-objet"], name:'Material', type:'liste', id:'materialsListe'},

			LODLow:{group:'LOD', name:'Low', type:'liste', id:'meshesListe', description:lang.load["properties-help-lodlow"]},
			LODLowDistance:{group:'LOD', name:'Low Distance', type:'number', description:lang.load["properties-help-lodlowdistance"]},
			LODMedium:{group:'LOD', name:'Medium', type:'liste', id:'meshesListe'},
			LODMediumDistance:{group:'LOD', name:'Medium Distance', type:'number'},
			LODHeight:{group:'LOD', name:'Height', type:'liste', id:'meshesListe'},
			LODHeightDistance:{group:'LOD', name:'Height Distance', type:'number'},

			scripts:{group:lang.load["properties-group-advanced"], name:'Scripts', type:'liste', id:'scriptsListe'},
			actions:{group:lang.load["properties-group-advanced"], name:'Action', type:'options', options:[{text:'None', value:'0'}], description:lang.load["properties-help-action"]},
			networkview:{group:lang.load["properties-group-advanced"], name:'Network View', type:'boolean', description:lang.load["properties-help-network"]}
		};
		this.creat(this.theObj, this.theMeta);
	};

	CASTORENGINE.properties.prototype.showSprite = function()
	{
		this.theObj = {
			type:'sprite',
			name:'',
			id:'',
			tags:'',
			renderingGroupId:0,
			layerMask:0,
			positionx:0, positiony:0, positionz:0,
			color:'',
			width:1,
			height :1,
			angle:0,
			cellIndex:0,
			invertU:0,
			invertV:0,
			size:1,
			scripts:''
		};
		this.theMeta = {
			type:{group:lang.load["properties-group-general"], name:'Type', disabled:true},
			name:{group:lang.load["properties-group-general"], name:'Name'},
			id:{group:lang.load["properties-group-general"], name:'ID'},
			tags:{group:lang.load["properties-group-general"], name:'Tags'},
			renderingGroupId:{group:lang.load["properties-group-general"], name:'Rendering Group Id', type:'number', description:lang.load["properties-help-zindex"]},

			positionx:{group:lang.load["properties-group-position"], name:'X', type:'number'},
			positiony:{group:lang.load["properties-group-position"], name:'Y', type:'number'},
			positionz:{group:lang.load["properties-group-position"], name:'Z', type:'number'},

			layerMask:{group:'Sprite', name:'Layer Mask', type:'number'},
			color:{group:'Sprite', name:'Color', type:'color', options:this.cpOptions},
			width:{group:'Sprite', name:'Width', type:'number'},
			height :{group:'Sprite', name:'Height', type:'number'},
			angle:{group:'Sprite', name:'Angle', type:'number'},
			cellIndex:{group:'Sprite', name:'Cell Index', type:'number'},
			invertU:{group:'Sprite', name:'InvertU', type:'number'},
			invertV:{group:'Sprite', name:'InvertV', type:'number'},
			size:{group:'Sprite', name:'Size', type:'number'},
			scripts:{group:'Sprite', name:'Scripts', type:'liste', id:'scriptsListe'}
		};
		this.creat(this.theObj, this.theMeta);
	};

	CASTORENGINE.properties.prototype.showMaterial = function()
	{
		this.theObj = {
			type:'material',
			name:'',
			id:'',
			tags:'',
			backFaceCulling:true,
			wireframe:false,
			diffuseTexture:'',
			diffuseColor:'#aaaaaa',
			videoTextureMP4:'',
			diffuse_uOffset:0,
			diffuse_vOffset:0,
			diffuse_uScale:1,
			diffuse_vScale:1,
			diffuse_hasAlpha:false,
			diffuse_alpha:0,

			ambientTexture:'',
			ambientColor:'#aaaaaa',
			ambient_uOffset:0,
			ambient_vOffset:0,
			ambient_uScale:1,
			ambient_vScale:1,
			ambient_hasAlpha:false,
			ambient_alpha:0,

			reflectionTexture:'',
			reflection_uOffset:0,
			reflection_vOffset:0,
			reflection_uScale:1,
			reflection_vScale:1,
			reflection_hasAlpha:false,
			reflection_alpha:0,

			opacityTexture:'',
			opacity_uOffset:0,
			opacity_vOffset:0,
			opacity_uScale:1,
			opacity_vScale:1,
			opacity_hasAlpha:false,
			opacity_alpha:0,

			emissiveTexture:'',
			emissiveColor:'#ffffff',
			emissive_uOffset:0,
			emissive_vOffset:0,
			emissive_uScale:1,
			emissive_vScale:1,
			emissive_hasAlpha:false,
			emissive_alpha:0,

			specularTexture:'',
			specularColor:'#000000',
			specular_uOffset:0,
			specular_vOffset:0,
			specular_uScale:1,
			specular_vScale:1,
			specular_hasAlpha:false,
			specular_alpha:0,

			bumpTexture:'',
			bump_uOffset:0,
			bump_vOffset:0,
			bump_uScale:1,
			bump_vScale:1,
			bump_hasAlpha:false,
			bump_alpha:0
		};
		this.theMeta = {
			type:{group:'General Material', name:'Type', disabled:true},
			name:{group:'General Material', name:'Name'},
			id:{group:'General Material', name:'Id'},
			tags:{group:'General Material', name:'Tags'},
			backFaceCulling:{group:'General Material', name:'Back Face Culling', type:'boolean', description:lang.load["properties-help-backFaceCulling"]},
			wireframe:{group:'General Material', name:'Wireframe', type:'boolean', description:lang.load["properties-help-wireframe"]},

			diffuseTexture:{group:'Diffuse', name:'Diffuse Texture', type:'liste', id:'textureListe'},
			diffuseColor:{group:'Diffuse', name:'Diffuse Color', type:'color', options:this.cpOptions},
			videoTextureMP4:{group:'Diffuse', name:'Video Texture MP4', type:'options', options:[{text:'None', value:'0'}], description:lang.load["properties-help-mp4"]},
			diffuse_uOffset:{group:'Diffuse', name:'uOffset', type:'number'},
			diffuse_vOffset:{group:'Diffuse', name:'vOffset', type:'number'},
			diffuse_uScale:{group:'Diffuse', name:'uScale', type:'number'},
			diffuse_vScale:{group:'Diffuse', name:'vScale', type:'number'},
			diffuse_hasAlpha:{group:'Diffuse', name:'Has alpha', type:'boolean', description:lang.load["properties-help-hasalpha"]},
			diffuse_alpha:{group:'Diffuse', name:'Level alpha', type:'number', options:{min:0, max:1}, description:lang.load["properties-help-levelalpha"]},

			ambientTexture:{group:'Ambient', name:'Ambient Texture', type:'liste', id:'textureListe'},
			ambientColor:{group:'Ambient', name:'Color Ambient', type:'color', options:this.cpOptions},
			ambient_uOffset:{group:'Ambient', name:'uOffset', type:'number'},
			ambient_vOffset:{group:'Ambient', name:'vOffset', type:'number'},
			ambient_uScale:{group:'Ambient', name:'uScale', type:'number'},
			ambient_vScale:{group:'Ambient', name:'vScale', type:'number'},
			ambient_hasAlpha:{group:'Ambient', name:'Has alpha', type:'boolean'},
			ambient_alpha:{group:'Ambient', name:'Level alpha', type:'number', options:{min:0, max:1}},

			opacityTexture:{group:'Opacity', name:'Opacity Texture', type:'liste', id:'textureListe'},
			opacity_uOffset:{group:'Opacity', name:'uOffset', type:'number'},
			opacity_vOffset:{group:'Opacity', name:'vOffset', type:'number'},
			opacity_uScale:{group:'Opacity', name:'uScale', type:'number'},
			opacity_vScale:{group:'Opacity', name:'vScale', type:'number'},
			opacity_hasAlpha:{group:'Opacity', name:'Has alpha', type:'boolean'},
			opacity_alpha:{group:'Opacity', name:'Level alpha', type:'number', options:{min:0, max:1}},

			reflectionTexture:{group:'Reflection', name:'Reflection Texture', type:'liste', id:'textureListe'},
			reflection_uOffset:{group:'Reflection', name:'uOffset', type:'number'},
			reflection_vOffset:{group:'Reflection', name:'vOffset', type:'number'},
			reflection_uScale:{group:'Reflection', name:'uScale', type:'number'},
			reflection_vScale:{group:'Reflection', name:'vScale', type:'number'},
			reflection_hasAlpha:{group:'Reflection', name:'Has alpha', type:'boolean'},
			reflection_alpha:{group:'Reflection', name:'Level alpha', type:'number', options:{min:0, max:1}},

			emissiveTexture:{group:'Emissive', name:'Emissive Texture', type:'liste', id:'textureListe'},
			emissiveColor:{group:'Emissive', name:'Color Emissive', type:'color', options:this.cpOptions},
			emissive_uOffset:{group:'Emissive', name:'uOffset', type:'number'},
			emissive_vOffset:{group:'Emissive', name:'vOffset', type:'number'},
			emissive_uScale:{group:'Emissive', name:'uScale', type:'number'},
			emissive_vScale:{group:'Emissive', name:'vScale', type:'number'},
			emissive_hasAlpha:{group:'Emissive', name:'Has alpha', type:'boolean'},
			emissive_alpha:{group:'Emissive', name:'Level alpha', type:'number', options:{min:0, max:1}},

			specularTexture:{group:'Specular', name:'Specular Texture', type:'liste', id:'textureListe'},
			specularspecularColor:{group:'Specular', name:'Color Specular', type:'color', options:this.cpOptions},
			specular_uOffset:{group:'Specular', name:'uOffset', type:'number'},
			specular_vOffset:{group:'Specular', name:'vOffset', type:'number'},
			specular_uScale:{group:'Specular', name:'uScale', type:'number'},
			specular_vScale:{group:'Specular', name:'vScale', type:'number'},
			specular_hasAlpha:{group:'Specular', name:'Has alpha', type:'boolean'},
			specular_alpha:{group:'Specular', name:'Level alpha', type:'number', options:{min:0, max:1}},

			bumpTexture:{group:'Bump', name:'Bump Texture', type:'liste', id:'textureListe'},
			bump_uOffset:{group:'Bump', name:'uOffset', type:'number'},
			bump_vOffset:{group:'Bump', name:'vOffset', type:'number'},
			bump_uScale:{group:'Bump', name:'uScale', type:'number'},
			bump_vScale:{group:'Bump', name:'vScale', type:'number'},
			bump_hasAlpha:{group:'Bump', name:'Has alpha', type:'boolean'},
			bump_alpha:{group:'Bump', name:'Level alpha', type:'number', options:{min:0, max:1}}
		};
		this.creat(this.theObj, this.theMeta);
	};

	CASTORENGINE.properties.prototype.showProcedural = function()
	{
		this.theObj = {
			type:'procedural',
			name:'',
			sizeTexture:1024,
			typeTexture:'',
			woodColor:'',
			ampScale:1,
			numberOfTilesHeight:5,
			numberOfTilesWidth:5,
			jointMarbleColor:'',
			marbleColor:'',
			numberOfBricksHeight:5,
			numberOfBricksWidth:5,
			jointBrickColor:'',
			brickColor:'',
			time:'0.01',
			speedX:1,
			speedY:1,
			grassColor:'',
			groundColor:'',
			roadColor:'',
			skyColor:'',
			cloudColor:''
		};
		this.theMeta = {
			type:{group:lang.load["properties-group-general"], name:'Type', disabled:true},
			name:{group:lang.load["properties-group-general"], name:'Name'},
			sizeTexture:{group:lang.load["properties-group-general"], name:'Size Texture', type:'number'},
			typeTexture:{group:'Diffuse', name:'Type Texture', type:'options', options:[
																							{text:'None', value:'0'},
																							{text:'WoodProceduralTexture', value:'Bois'},
																							{text:'MarbleProceduralTexture', value:'Marbre'},
																							{text:'BrickProceduralTexture', value:'Brique'},
																							{text:'FireProceduralTexture', value:'Feux'},
																							{text:'GrassProceduralTexture', value:'Herbe'},
																							{text:'RoadProceduralTexture', value:'Bitume'},
																							{text:'CloudProceduralTexture', value:'Nuage'}
																						 ], description:lang.load["properties-help-typeprocedural"]},

			woodColor:{group:'Wood Procedural Texture', name:'Wood Color', type:'color', options:this.cpOptions},
			ampScale:{group:'Wood Procedural Texture', name:'Amplitude Scale', type:'number'},

			numberOfTilesHeight:{group:'Marble Procedural Texture', name:lang.load["properties-name-numberOfTilesHeight"], type:'number'},
			numberOfTilesWidth:{group:'Marble Procedural Texture', name:lang.load["properties-name-numberOfTilesWidth"], type:'number'},
			jointMarbleColor:{group:'Marble Procedural Texture', name:'Joint Color', type:'color', options:this.cpOptions},
			marbleColor:{group:'Marble Procedural Texture', name:'Marble Color', type:'color', options:this.cpOptions},

			numberOfBricksHeight:{group:'Brick Procedural Texture', name:lang.load["properties-name-numberOfBricksHeight"], type:'number'},
			numberOfBricksWidth:{group:'Brick Procedural Texture', name:lang.load["properties-name-numberOfBricksWidth"], type:'number'},
			jointBrickColor:{group:'Brick Procedural Texture', name:'Joint Color', type:'color', options:this.cpOptions},
			brickColor:{group:'Brick Procedural Texture', name:'Brick Color', type:'color', options:this.cpOptions},

			time:{group:'Fire Procedural Texture', name:'Time', type:'number'},
			speedX:{group:'Fire Procedural Texture', name:'Speed X', type:'number'},
			speedY:{group:'Fire Procedural Texture', name:'Speed Y', type:'number'},

			grassColor:{group:'Grass Procedural Texture', name:'Grass Color', type:'color', options:this.cpOptions},
			groundColor:{group:'Grass Procedural Texture', name:'Ground Color', type:'color', options:this.cpOptions},

			roadColor:{group:'Road Procedural Texture', name:'Road Color', type:'color', options:this.cpOptions},

			skyColor:{group:'Cloud Procedural Texture', name:'Sky Color', type:'color', options:this.cpOptions},
			cloudColor:{group:'Cloud Procedural Texture', name:'Cloud Color', type:'color', options:this.cpOptions}
		};
		this.creat(this.theObj, this.theMeta);
	};

	CASTORENGINE.properties.prototype.showParticlesSystem = function()
	{
		this.theObj = {
			type:'particules',
			name:'',
			maxParticleSysteme:2000,
			particleTexture:'',
			emitter:'',
			blendMode:'BABYLON.ParticleSystem.BLENDMODE_ONEONE',
			textureMask:'',
			linkToEmitter:true,
			gravityX:0,
			gravityY:-9.81,
			gravityZ:0,
			direction1X:-7,
			direction1Y:8,
			direction1Z:3,
			direction2X:7,
			direction2Y:8,
			direction2Z:-3,
			minEmitBoxX:-1,
			minEmitBoxY:1,
			minEmitBoxZ:0,
			maxEmitBoxX:1,
			maxEmitBoxY:1,
			maxEmitBoxZ:0,
			color1:'',
			color2:'',
			colorDead:'',
			emitRate:500,
			updateSpeed:0.005,
			minEmitPower:1,
			maxEmitPower:3,
			minLifeTime:0.5,
			maxLifeTime:1.5,
			minSize:0.1,
			maxSize:0.5,
			minAngularSpeed:0,
			maxAngularSpeed:3.14
		};
		this.theMeta = {
			type:{group:lang.load["properties-group-general"], name:'Type', disabled:true},
			name:{group:lang.load["properties-group-general"], name:'Name'},
			maxParticleSysteme:{group:lang.load["properties-group-general"], name:lang.load["properties-name-maxParticle"], type:'number'},
			particleTexture:{group:lang.load["properties-group-general"], name:'Texture', type:'liste', id:'textureListe'},
			textureMask:{group:lang.load["properties-group-general"], name:'Texture Mask', type:'liste', id:'textureListe'},
			emitter:{group:lang.load["properties-group-general"], name:lang.load["properties-name-emiter"], type:'liste', id:'meshesListe', description:lang.load["properties-help-emiter"]},
			blendMode:{group:lang.load["properties-group-general"], name:'Blend Mode'},
			linkToEmitter:{group:lang.load["properties-group-general"], name:lang.load["properties-name-linkemiter"], type:'boolean'},

			gravityX:{group:'Particles gravity', name:'X', type:'number'},
			gravityY:{group:'Particles gravity', name:'Y', type:'number'},
			gravityZ:{group:'Particles gravity', name:'Z', type:'number'},

			direction1X:{group:'Particles direction 1', name:'X', type:'number'},
			direction1Y:{group:'Particles direction 1', name:'Y', type:'number'},
			direction1Z:{group:'Particles direction 1', name:'Z', type:'number'},

			direction2X:{group:'Particles direction 2', name:'X', type:'number'},
			direction2Y:{group:'Particles direction 2', name:'Y', type:'number'},
			direction2Z:{group:'Particles direction 2', name:'Z', type:'number'},

			minEmitBoxX:{group:'Min Emit Box', name:'X', type:'number'},
			minEmitBoxY:{group:'Min Emit Box', name:'Y', type:'number'},
			minEmitBoxZ:{group:'Min Emit Box', name:'Z', type:'number'},

			maxEmitBoxX:{group:'Max Emit Box', name:'X', type:'number'},
			maxEmitBoxY:{group:'Max Emit Box', name:'Y', type:'number'},
			maxEmitBoxZ:{group:'Max Emit Box', name:'Z', type:'number'},

			color1:{group:'Particles', name:'Color 1', type:'color', options:this.cpOptions},
			color2:{group:'Particles', name:'Color 2', type:'color', options:this.cpOptions},
			colorDead:{group:'Particles', name:'Color mort', type:'color', options:this.cpOptions},
			emitRate:{group:'Particles', name:'Emit Rate', type:'number'},
			updateSpeed:{group:'Particles', name:'Speed', type:'number', options:{step:0.001}},
			minEmitPower:{group:'Particles', name:'Min Emit Power', type:'number'},
			maxEmitPower:{group:'Particles', name:'Max Emit Power', type:'number'},
			minLifeTime:{group:'Particles', name:'Min Life Time', type:'number'},
			maxLifeTime:{group:'Particles', name:'Max Life Time', type:'number'},
			minSize:{group:'Particles', name:'Min Size', type:'number'},
			maxSize:{group:'Particles', name:'Max Size', type:'number'},
			minAngularSpeed:{group:'Particles', name:'Min Angular Speed', type:'number'},
			maxAngularSpeed:{group:'Particles', name:'Max Angular Speed', type:'number'}
		};
		this.creat(this.theObj, this.theMeta);
	};

	CASTORENGINE.properties.prototype.showSound = function()
	{
		this.theObj = {
			type:'sounds',
			name:'',
			mp3:'',
			ogg:'',
			loop:true,
			autoplay:true,
			streaming:true,
			volume:1.0,
			attachToMesh:'',
			script:''
		};
		this.theMeta = {
			type:{group:lang.load["properties-group-general"], name:'Type', disabled:true},
			name:{group:lang.load["properties-group-general"], name:'Name'},
			mp3:{group:'Sounds', name:'MP3', type:'liste', id:'soundsListe'},
			ogg:{group:'Sounds', name:'OGG', type:'liste', id:'soundsListe'},
			loop:{group:'Sounds', name:'Loop', type:'boolean'},
			autoplay:{group:'Sounds', name:'Auto play', type:'boolean', description:lang.load["properties-help-autoplay"]},
			streaming:{group:'Sounds', name:'Streaming', type:'boolean', description:lang.load["properties-help-streaming"]},
			volume:{group:'Sounds', name:'Volume', type:'number', options:{min:0, max:1.0}},
			attachToMesh:{group:'Sounds', name:'Attach to mesh', type:'liste', id:'meshesListe', description:lang.load["properties-help-attachToMesh"]},
			scripts:{group:'Sounds', name:'Scripts', type:'liste', id:'scriptsListe'}
		};
		this.creat(this.theObj, this.theMeta);
	};

	CASTORENGINE.properties.prototype.showScript = function()
	{
		this.theObj = {
			type:'script',
			name:'mon_script'
		};
		this.theMeta = {
			type:{group:lang.load["properties-group-general"], name:'Type', disabled:true},
			name:{group:lang.load["properties-group-general"], name:'Name script'}
		};
		this.creat(this.theObj, this.theMeta);
	};

	CASTORENGINE.properties.prototype.showCollider = function()
	{
		this.theObj = {
			type:'collider',
			positionX:0,
			positionY:0,
			positionZ:0,
			scaleX:0,
			scaleY:0,
			scaleZ:0
		};
		this.theMeta = {
			type:{group:lang.load["properties-group-general"], name:'Type', disabled:true},
			positionX:{group:lang.load["properties-group-adjustement"], name:'Position x', type:'number'},
			positionY:{group:lang.load["properties-group-adjustement"], name:'Position y', type:'number'},
			positionZ:{group:lang.load["properties-group-adjustement"], name:'Position z', type:'number'},
			scaleX:{group:lang.load["properties-group-adjustement"], name:'Scale x', type:'number'},
			scaleY:{group:lang.load["properties-group-adjustement"], name:'Scale y', type:'number'},
			scaleZ:{group:lang.load["properties-group-adjustement"], name:'Scale z', type:'number'}
		};
		this.creat(this.theObj, this.theMeta);
	};

	CASTORENGINE.properties.prototype.showRigidBody = function()
	{
		this.theObj = {
			type:'rigid body',
			ellipsoidX:0.75,
			ellipsoidY:1.0,
			ellipsoidZ:0.75,
			ellipsoidOffsetX:0,
			ellipsoidOffsetY:2.0,
			ellipsoidOffsetZ:0,
			applyGravity:true
		};
		this.theMeta = {
			type:{group:lang.load["properties-group-general"], name:'Type', disabled:true},
			ellipsoidX:{group:'Ellipsoid', name:'X', type:'number'},
			ellipsoidY:{group:'Ellipsoid', name:'Y', type:'number'},
			ellipsoidZ:{group:'Ellipsoid', name:'Z', type:'number'},
			ellipsoidOffsetX:{group:'Ellipsoid '+lang.load["properties-group-adjustement"], name:'X', type:'number'},
			ellipsoidOffsetY:{group:'Ellipsoid '+lang.load["properties-group-adjustement"], name:'Y', type:'number'},
			ellipsoidOffsetZ:{group:'Ellipsoid '+lang.load["properties-group-adjustement"], name:'Z', type:'number'},
			applyGravity:{group:lang.load["properties-group-general"], name:'Use gravity', type:'boolean'}
		};
		this.creat(this.theObj, this.theMeta);
	};


	CASTORENGINE.properties.prototype.showScene = function()
	{
		this.theObj = {
			type:'scene',
			name:'new scene',
			clearColor:'',
			cameraToUseForPointers:'',
			activeCamera:'',
			collisionsEnabled:false,
			workerCollisions:false,
			gravity:'-10',
			useOctree:false
		};
		this.theMeta = {
			type:{group:lang.load["properties-group-general"], name:'Type', disabled:true},
			name:{group:lang.load["properties-group-general"], name:'Name scene'},
			clearColor:{group:lang.load["properties-group-general"], name:'Color scene', type:'color', options:this.cpOptions},
			cameraToUseForPointers:{group:lang.load["properties-group-general"], name:lang.load["properties-name-camusedforpointer"], type:'options', options:[{text:'None', value:'0'}]},
			activeCamera:{group:lang.load["properties-group-general"], name:'Camera active', type:'options', options:[{text:'None', value:'0'}]},
			collisionsEnabled:{group:lang.load["properties-group-general"], name:'Collisions', type:'boolean'},
			workerCollisions:{group:lang.load["properties-group-general"], name:'Worker Collisions', type:'boolean'},
			gravity:{group:lang.load["properties-group-general"], name:'Gravity', type:'number'},
			useOctree:{group:lang.load["properties-group-general"], name:'Use Octree', type:'boolean'}
		};
		this.creat(this.theObj, this.theMeta);
	};

	CASTORENGINE.properties.prototype.showEngine = function()
	{
		this.theObj = {
			type:'engine',
			antialias:true,
			loadingUIText:lang.load["properties-group-loading"],
			loadingUIBackgroundColor:'black',
			enableOfflineSupport:false,
			engineResize:true,
			serveur:true
		};
		this.theMeta = {
			type:{group:lang.load["properties-group-general"], name:'Type', disabled:true},
			antialias:{group:lang.load["properties-group-general"], name:'Use Anti-Aliasing', type:'boolean'},
			loadingUIText:{group:lang.load["properties-group-general"], name:'Text Loader'},
			loadingUIBackgroundColor:{group:lang.load["properties-group-general"], name:'Color Loader', type:'color', options:this.cpOptions},
			enableOfflineSupport:{group:lang.load["properties-group-general"], name:'Use Support Offline', type:'boolean'},
			engineResize:{group:lang.load["properties-group-general"], name:'Is Resisable', type:'boolean'},
			serveur:{group:lang.load["properties-group-general"], name:'Use Serveur NodeJS', type:'boolean'}
		};
		this.creat(this.theObj, this.theMeta);
	};

	CASTORENGINE.properties.prototype.showFog = function()
	{
		this.theObj = {
			type:'fog',
			name:'',
			fogMode:'FOGMODE_NONE',
			fogDensity:0.01,
			fogStart:50,
			fogEnd:200,
			fogColor:'rgb(150, 150, 150)'
		};
		this.theMeta = {
			type:{group:lang.load["properties-group-general"], name:'Type', disabled:true},
			name:{group:lang.load["properties-group-general"], name:'Name'},
			fogMode:{group:'Fog', name:'Mode', type:'options', options:[
																			{text:'FOGMODE_NONE', value:'FOGMODE_NONE'},
																			{text:'FOGMODE_EXP', value:'FOGMODE_EXP'},
																			{text:'FOGMODE_EXP2', value:'FOGMODE_EXP2'},
																			{text:'FOGMODE_LINEAR', value:'FOGMODE_LINEAR'}
																		]},
			fogDensity:{group:'Fog', name:'Density', type:'number'},
			fogStart:{group:'Fog', name:'Start', type:'number'},
			fogEnd:{group:'Fog', name:'End', type:'number'},
			fogColor:{group:'Fog', name:'Color', type:'color', options:{preferredFormat:'rgb', showInput:true, showInitial:true}}
		};
		this.creat(this.theObj, this.theMeta);
	};

	CASTORENGINE.properties.prototype.showEnvironnement = function()
	{
		this.theObj = {
			type:'environnement',
			skyBox:true,
			textureNameSkyBox:'TropicalSunnyDay'
		};
		this.theMeta = {
			type:{group:lang.load["properties-group-general"], name:'Type', disabled:true},
			skyBox:{group:'SkyBox', name:'Use SkyBox', type:'boolean'},
			textureNameSkyBox:{group:'SkyBox', name:'Name texture of skyBox'}
		};
		this.creat(this.theObj, this.theMeta);
	};

	CASTORENGINE.properties.prototype.showCamera = function(typeCamera)
	{
		if(typeCamera == "FreeCamera") {
			this.theObj = {
				type:typeCamera,
				name:'',
				mode:'PERSPECTIVE',
				layerMask:0,
				positionx:0, positiony:0, positionz:0,
				rotationx:0, rotationy:0, rotationz:0,
				attachControl:true,
				attachPostProcess:'',
				fov:0,
				minZ:0,
				maxZ:0,
				inertia:0,
				//pour l'orthographique
				upVector:0,
				orthoLeft:0,
				orthoRight:0,
				orthoBottom:0,
				orthoTop:0,
				ellipsoidx:0.6,
				ellipsoidy:1.0,
				ellipsoidz:0.6,
				keysUp:null,
				keysDown:null,
				keysLeft:null,
				keysRight:null,
				checkCollisions:false,
				applyGravity:false,
				angularSensibility:1
			};
			this.theMeta = {
				type:{group:lang.load["properties-group-general"], name:'Type', disabled:true},
				name:{group:lang.load["properties-group-general"], name:'Name'},
				mode:{group:lang.load["properties-group-general"], name:'Camera', type:'options', options:[
																				{text:'PERSPECTIVE', value:'PERSPECTIVE'},
																				{text:'ORTHOGRAPHIC', value:'ORTHOGRAPHIC'}
																			]},
				layerMask:{group:lang.load["properties-group-general"], name:'Layer Mask', type:'number'},
				positionx:{group:'position', name:'X', type:'number'},
				positiony:{group:'position', name:'Y', type:'number'},
				positionz:{group:'position', name:'Z', type:'number'},
				rotationx:{group:'rotation', name:'X', type:'number'},
				rotationy:{group:'rotation', name:'Y', type:'number'},
				rotationz:{group:'rotation', name:'Z', type:'number'},
				attachControl:{group:lang.load["properties-group-general"], name:'Attach Control', type:'boolean'},
				attachPostProcess:{group:lang.load["properties-group-general"], name:'Attach Post-Process', type:'options', options:[{text:'None', value:'0'}]},
				fov:{group:lang.load["properties-group-general"], name:'Fov', type:'number'},
				minZ:{group:lang.load["properties-group-general"], name:'MinZ', type:'number'},
				maxZ:{group:lang.load["properties-group-general"], name:'MaxZ', type:'number'},
				inertia:{group:lang.load["properties-group-general"], name:'Inertia', type:'number'},
				//pour l'orthographique
				upVector:{group:'Camera orthographique', name:'Density', type:'number'},
				orthoLeft:{group:'Camera orthographique', name:'Left', type:'number'},
				orthoRight:{group:'Camera orthographique', name:'Right', type:'number'},
				orthoBottom:{group:'Camera orthographique', name:'Bottom', type:'number'},
				orthoTop:{group:'Camera orthographique', name:'Top', type:'number'},
				ellipsoidx:{group:'ellipsoid', name:'X', type:'number'},
				ellipsoidy:{group:'ellipsoid', name:'Y', type:'number'},
				ellipsoidz:{group:'ellipsoid', name:'Z', type:'number'},
				keysUp:{group:lang.load["properties-group-general"], name:'KeysUp'},
				keysDown:{group:lang.load["properties-group-general"], name:'KeysDown'},
				keysLeft:{group:lang.load["properties-group-general"], name:'KeysLeft'},
				keysRight:{group:lang.load["properties-group-general"], name:'KeysRight'},
				checkCollisions:{group:lang.load["properties-group-general"], name:'Collisions', type:'boolean'},
				applyGravity:{group:lang.load["properties-group-general"], name:'Appliquer Gravity', type:'boolean'},
				angularSensibility:{group:lang.load["properties-group-general"], name:'Angular Sensibility', type:'number'}
			};
		}
		else if(typeCamera == "ArcRotateCamera")
		{
			this.theObj = {
				type:typeCamera,
				name:'',
				mode:'PERSPECTIVE',
				layerMask:0,
				positionx:0, positiony:0, positionz:0,
				rotationx:0, rotationy:0, rotationz:0,
				attachControl:true,
				attachPostProcess:'',
				fov:0,
				minZ:0,
				maxZ:0,
				inertia:0,
				//pour l'orthographique
				upVector:0,
				orthoLeft:0,
				orthoRight:0,
				orthoBottom:0,
				orthoTop:0,
				alpha:0,
				beta:0,
				radius:0,
				target:'',
				inertialAlphaOffset:0,
				inertialBetaOffset:0,
				inertialRadiusOffset:0,
				lowerAlphaLimit:0,
				upperAlphaLimit:0,
				lowerBetaLimit:0,
				upperBetaLimit:0,
				lowerRadiusLimit:0,
				upperRadiusLimit:0,
				angularSensibilityX:0,
				angularSensibilityY:0,
				wheelPrecision:0,
				pinchPrecision:0,
				panningSensibility:0,
				inertialPanningX:0,
				inertialPanningY:0,
				keysUp:'',
				keysDown:'',
				keysLeft:'',
				keysRight:'',
				zoomOnFactor:0,
				targetScreenOffsetx:0,
				targetScreenOffsety:0,
				checkCollisions:false,
				collisionRadiusx:0,
				collisionRadiusy:0,
				collisionRadiusz:0,
				angularSensibility:0
			};
			this.theMeta = {
				type:{group:lang.load["properties-group-general"], name:'Type', disabled:true},
				name:{group:lang.load["properties-group-general"], name:'Name'},
				mode:{group:lang.load["properties-group-general"], name:'Camera', type:'options', options:[
																				{text:'PERSPECTIVE', value:'PERSPECTIVE'},
																				{text:'ORTHOGRAPHIC', value:'ORTHOGRAPHIC'}
																			]},
				layerMask:{group:lang.load["properties-group-general"], name:'Layer Mask', type:'number'},
				positionx:{group:'position', name:'X', type:'number'},
				positiony:{group:'position', name:'Y', type:'number'},
				positionz:{group:'position', name:'Z', type:'number'},
				rotationx:{group:'rotation', name:'X', type:'number'},
				rotationy:{group:'rotation', name:'Y', type:'number'},
				rotationz:{group:'rotation', name:'Z', type:'number'},
				attachControl:{group:lang.load["properties-group-general"], name:'Attach Control', type:'boolean'},
				attachPostProcess:{group:lang.load["properties-group-general"], name:'Attach Post-Process', type:'options', options:[{text:'None', value:'0'}]},
				fov:{group:lang.load["properties-group-general"], name:'Fov', type:'number'},
				minZ:{group:lang.load["properties-group-general"], name:'MinZ', type:'number'},
				maxZ:{group:lang.load["properties-group-general"], name:'MaxZ', type:'number'},
				inertia:{group:lang.load["properties-group-general"], name:'Inertia', type:'number'},
				//pour l'orthographique
				upVector:{group:'Camera orthographique', name:'Density', type:'number'},
				orthoLeft:{group:'Camera orthographique', name:'Left', type:'number'},
				orthoRight:{group:'Camera orthographique', name:'Right', type:'number'},
				orthoBottom:{group:'Camera orthographique', name:'Bottom', type:'number'},
				orthoTop:{group:'Camera orthographique', name:'Top', type:'number'},
				alpha:{group:lang.load["properties-group-general"], name:'Alpha', type:'number'},
				beta:{group:lang.load["properties-group-general"], name:'Beta', type:'number'},
				radius:{group:lang.load["properties-group-general"], name:'Radius', type:'number'},

				target:{group:lang.load["properties-group-general"], name:'Objet target', type:'options', options:[{text:'None', value:'0'}]},
				inertialAlphaOffset:{group:'Inertial Offset', name:'Alpha', type:'number'},
				inertialBetaOffset:{group:'Inertial Offset', name:'Beta', type:'number'},
				inertialRadiusOffset:{group:'Inertial Offset', name:'Radius', type:'number'},
				lowerAlphaLimit:{group:'Alpha Limit', name:'Lower', type:'number'},
				upperAlphaLimit:{group:'Alpha Limit', name:'Upper', type:'number'},
				lowerBetaLimit:{group:'Beta Limit', name:'Lower', type:'number'},
				upperBetaLimit:{group:'Beta Limit', name:'Upper', type:'number'},
				lowerRadiusLimit:{group:'Radius Limit', name:'Lower', type:'number'},
				upperRadiusLimit:{group:'Radius Limit', name:'Upper', type:'number'},
				angularSensibilityX:{group:'Angular Sensibility', name:'X', type:'number'},
				angularSensibilityY:{group:'Angular Sensibility', name:'Y', type:'number'},
				wheelPrecision:{group:lang.load["properties-group-general"], name:'Wheel Precision', type:'number'},
				pinchPrecision:{group:lang.load["properties-group-general"], name:'Pinch Precision', type:'number'},
				panningSensibility:{group:'Panning', name:'Sensibility', type:'number'},
				inertialPanningX:{group:'Panning', name:'Inertial X', type:'number'},
				inertialPanningY:{group:'Panning', name:'Inertial Y', type:'number'},
				keysUp:{group:lang.load["properties-group-general"], name:'KeysUp'},
				keysDown:{group:lang.load["properties-group-general"], name:'KeysDown'},
				keysLeft:{group:lang.load["properties-group-general"], name:'KeysLeft'},
				keysRight:{group:lang.load["properties-group-general"], name:'KeysRight'},
				zoomOnFactor:{group:lang.load["properties-group-general"], name:'Density', type:'number'},
				targetScreenOffsetx:{group:'Target Screen Offset', name:'X', type:'number'},
				targetScreenOffsety:{group:'Target Screen Offset', name:'Y', type:'number'},
				checkCollisions:{group:lang.load["properties-group-general"], name:'Collisions', type:'boolean'},
				collisionRadiusx:{group:'Collision Radius', name:'X', type:'number'},
				collisionRadiusy:{group:'Collision Radius', name:'Y', type:'number'},
				collisionRadiusz:{group:'Collision Radius', name:'Z', type:'number'},
				angularSensibility:{group:lang.load["properties-group-general"], name:'Angular Sensibility', type:'number'}
			};
		}
		else if(typeCamera == "GamePadCamera" || typeCamera == "TouchCamera")
		{
			this.theObj = {
				type:typeCamera,
				name:'',
				mode:'PERSPECTIVE',
				layerMask:0,
				positionx:0, positiony:0, positionz:0,
				rotationx:0, rotationy:0, rotationz:0,
				attachControl:true,
				attachPostProcess:'',
				fov:0,
				minZ:0,
				maxZ:0,
				inertia:0,
				//pour l'orthographique
				upVector:0,
				orthoLeft:0,
				orthoRight:0,
				orthoBottom:0,
				orthoTop:0,
				angularSensibility:0,
				moveSensibility:0
			};
			this.theMeta = {
				type:{group:lang.load["properties-group-general"], name:'Type', disabled:true},
				name:{group:lang.load["properties-group-general"], name:'Name'},
				mode:{group:lang.load["properties-group-general"], name:'Camera', type:'options', options:[
																				{text:'PERSPECTIVE', value:'PERSPECTIVE'},
																				{text:'ORTHOGRAPHIC', value:'ORTHOGRAPHIC'}
																			]},
				layerMask:{group:lang.load["properties-group-general"], name:'Layer Mask', type:'number'},
				positionx:{group:'position', name:'X', type:'number'},
				positiony:{group:'position', name:'Y', type:'number'},
				positionz:{group:'position', name:'Z', type:'number'},
				rotationx:{group:'rotation', name:'X', type:'number'},
				rotationy:{group:'rotation', name:'Y', type:'number'},
				rotationz:{group:'rotation', name:'Z', type:'number'},
				attachControl:{group:lang.load["properties-group-general"], name:'Attach Control', type:'boolean'},
				attachPostProcess:{group:lang.load["properties-group-general"], name:'Attach Post-Process', type:'options', options:[{text:'None', value:'0'}]},
				fov:{group:lang.load["properties-group-general"], name:'Fov', type:'number'},
				minZ:{group:lang.load["properties-group-general"], name:'MinZ', type:'number'},
				maxZ:{group:lang.load["properties-group-general"], name:'MaxZ', type:'number'},
				inertia:{group:lang.load["properties-group-general"], name:'Inertia', type:'number'},
				//pour l'orthographique
				upVector:{group:'Camera orthographique', name:'Density', type:'number'},
				orthoLeft:{group:'Camera orthographique', name:'Left', type:'number'},
				orthoRight:{group:'Camera orthographique', name:'Right', type:'number'},
				orthoBottom:{group:'Camera orthographique', name:'Bottom', type:'number'},
				orthoTop:{group:'Camera orthographique', name:'Top', type:'number'},
				angularSensibility:{group:lang.load["properties-group-general"], name:'AngularSensibility', type:'number'},
				moveSensibility:{group:lang.load["properties-group-general"], name:'MoveSensibility', type:'number'}
			};
		}
		else if(typeCamera == "FollowCamera")
		{
			this.theObj = {
				type:typeCamera,
				name:'',
				mode:'PERSPECTIVE',
				layerMask:0,
				positionx:0, positiony:0, positionz:0,
				rotationx:0, rotationy:0, rotationz:0,
				attachControl:true,
				attachPostProcess:'',
				fov:0,
				minZ:0,
				maxZ:0,
				inertia:0,
				//pour l'orthographique
				upVector:0,
				orthoLeft:0,
				orthoRight:0,
				orthoBottom:0,
				orthoTop:0,
				radius:0,
				rotationOffset:0,
				heightOffset:0,
				cameraAcceleration:0,
				maxCameraSpeed:1,
				target:''
			};
			this.theMeta = {
				type:{group:lang.load["properties-group-general"], name:'Type', disabled:true},
				name:{group:lang.load["properties-group-general"], name:'Name'},
				mode:{group:lang.load["properties-group-general"], name:'Camera', type:'options', options:[
																				{text:'PERSPECTIVE', value:'PERSPECTIVE'},
																				{text:'ORTHOGRAPHIC', value:'ORTHOGRAPHIC'}
																			]},
				layerMask:{group:lang.load["properties-group-general"], name:'Layer Mask', type:'number'},
				positionx:{group:'position', name:'X', type:'number'},
				positiony:{group:'position', name:'Y', type:'number'},
				positionz:{group:'position', name:'Z', type:'number'},
				rotationx:{group:'rotation', name:'X', type:'number'},
				rotationy:{group:'rotation', name:'Y', type:'number'},
				rotationz:{group:'rotation', name:'Z', type:'number'},
				attachControl:{group:lang.load["properties-group-general"], name:'Attach Control', type:'boolean'},
				attachPostProcess:{group:lang.load["properties-group-general"], name:'Attach Post-Process', type:'options', options:[{text:'None', value:'0'}]},
				fov:{group:lang.load["properties-group-general"], name:'Fov', type:'number'},
				minZ:{group:lang.load["properties-group-general"], name:'MinZ', type:'number'},
				maxZ:{group:lang.load["properties-group-general"], name:'MaxZ', type:'number'},
				inertia:{group:lang.load["properties-group-general"], name:'Inertia', type:'number'},
				//pour l'orthographique
				upVector:{group:'Camera orthographique', name:'Density', type:'number'},
				orthoLeft:{group:'Camera orthographique', name:'Left', type:'number'},
				orthoRight:{group:'Camera orthographique', name:'Right', type:'number'},
				orthoBottom:{group:'Camera orthographique', name:'Bottom', type:'number'},
				orthoTop:{group:'Camera orthographique', name:'Top', type:'number'},
				radius:{group:lang.load["properties-group-general"], name:'Radius', type:'number'},
				rotationOffset:{group:lang.load["properties-group-general"], name:'Rotation Offset', type:'number'},
				heightOffset:{group:lang.load["properties-group-general"], name:'Height Offset', type:'number'},
				cameraAcceleration:{group:lang.load["properties-group-general"], name:'Camera Acceleration', type:'number'},
				maxCameraSpeed:{group:lang.load["properties-group-general"], name:'Max Camera Speed', type:'number'},
				target:{group:lang.load["properties-group-general"], name:'Objet cible', type:'options', options:[{text:'None', value:'0'}]}
			};
		}
		this.creat(this.theObj, this.theMeta);
	};

	CASTORENGINE.properties.prototype.showLight = function()
	{
		this.theObj = {
			type:'light',
			name:'',
			light:'',
			diffuse:'rgb(255, 255, 255)',
			speculare:'rgb(0, 0, 0)',
			intensity:1.0,
			range:0,
			includeOnlyWithLayerMask:0,
			excludeWithLayerMask:null,
			includedOnlyMeshes:'',
			excludedMeshes:'',
			positionx:0,
			positiony:0,
			positionz:0,
			directionx:0,
			directiony:0,
			direction:0,
			groundColor:'rgb(131, 75, 0)',
			angle:0,
			exponent:0
		};
		this.theMeta = {
			type:{group:lang.load["properties-group-general"], name:'Type', disabled:true},
			name:{group:lang.load["properties-group-general"], name:'Name'},
			light:{group:lang.load["properties-group-general"], name:'Inclure seulement objets', type:'options', options:[
																								{text:'Spot light', value:'SpotLight'},
																								{text:'Point light', value:'PointLight'},
																								{text:'Directionnelle light', value:'DirectionalLight'},
																								{text:'Hemispherique light', value:'HemisphericLight'},
																							], description:"Choix du type de lumiere à utiliser"},
			diffuse:{group:lang.load["properties-group-general"], name:'Color diffuse', type:'color', options:this.cpOptions},
			speculare:{group:lang.load["properties-group-general"], name:'Color speculare', type:'color', options:this.cpOptions},
			intensity:{group:lang.load["properties-group-general"], name:'Intensité', type:'number'},
			range:{group:lang.load["properties-group-general"], name:'Range', type:'number'},
			includeOnlyWithLayerMask:{group:lang.load["properties-group-general"], name:'Inclure seulement avec LayerMask', type:'number'},
			excludeWithLayerMask:{group:lang.load["properties-group-general"], name:'Exlure avec LayerMask', type:'number'},
			includedOnlyMeshes:{group:lang.load["properties-group-general"], name:'Inclure seulement objets', type:'options', options:[{text:'None', value:'0', multiple:true}], description:lang.load["properties-help-lightinclure"]},
			excludedMeshes:{group:lang.load["properties-group-general"], name:'Exlure objets', type:'options', options:[{text:'None', value:'0', multiple:true}], description:lang.load["properties-help-lightexclure"]},
			positionx:{group:lang.load["properties-group-general"], name:'Position X', type:'number'},
			positiony:{group:lang.load["properties-group-general"], name:'Position Y', type:'number'},
			positionz:{group:lang.load["properties-group-general"], name:'Position Z', type:'number'},

			directionx:{group:lang.load["properties-group-fordirectionnelleandhemispherique"], name:'Direction X', type:'number'},
			directiony:{group:lang.load["properties-group-fordirectionnelleandhemispherique"], name:'Direction Y', type:'number'},
			direction:{group:lang.load["properties-group-fordirectionnelleandhemispherique"], name:'Direction Z', type:'number'},

			groundColor:{group:lang.load["properties-group-forhemispherique"], name:'Color of ground', type:'color', options:this.cpOptions},

			angle:{group:'For Spot', name:'Angle', type:'number'},
			exponent:{group:'For Spot', name:'Exponent', type:'number'},
		};
		this.creat(this.theObj, this.theMeta);
	};

	CASTORENGINE.properties.prototype.showLensFlare = function()
	{
		this.theObj = {
			type:'lens flare',
			size:0.5,
			position:1,
			color:''
		};
		this.theMeta = {
			type:{group:lang.load["properties-group-general"], name:'Type', disabled:true},
			size:{group:lang.load["properties-group-general"], name:'Taille of lensflare', type:'number', options:{min:0, max:1.0}},
			position:{group:lang.load["properties-group-general"], name:'Position of lensflare', type:'number', options:{min:0, max:1.0}},
			color:{group:lang.load["properties-group-general"], name:'Color', type:'color', options:this.cpOptions},
		};
		this.creat(this.theObj, this.theMeta);
	};

	CASTORENGINE.properties.prototype.showPhysic = function()
	{
		this.theObj = {
			type:'physic',
			name:'',
			shape:'BoxImpostor',
			mass:0,
            friction:0.5,
			restitution:0.7
		};
		this.theMeta = {
			type:{group:lang.load["properties-group-general"], name:'Type', disabled:true},
			name:{group:lang.load["properties-group-general"], name:'Name'},
			shape:{group:lang.load["properties-group-general"], name:'Shapes', type:'options', options:[
																		{text:'Box Impostor', value:'BoxImpostor'},
																		{text:'Sphere Impostor', value:'SphereImpostor'},
																		{text:'Heightmap Impostor', value:'HeightmapImpostor'},
																	]},
			mass:{group:lang.load["properties-group-general"], name:'Mass', type:'number'},
            friction:{group:lang.load["properties-group-general"], name:'Friction', type:'number'},
			restitution:{group:lang.load["properties-group-general"], name:'Restitution', type:'number'}
		};
		this.creat(this.theObj, this.theMeta);
	};

	CASTORENGINE.properties.prototype.showOptimizer = function()
	{
		this.theObj = {
			type:'optimizer',
			FPSMini:30,
			trackerDuration:2000,
			levelTextureOptimization:1,
			levelLensFlaresOptimization:2,
			levelShadowsOptimization:3,
			levelParticlesOptimization:4,
			levelPostProcessesOptimization:5,
			levelRenderTargetsOptimization:6,
			levelHardwareScalingOptimization:7
		};
		this.theMeta = {
			type:{group:lang.load["properties-group-general"], name:'Type', disabled:true},
			FPSMini:{group:lang.load["properties-group-general"], name:'FPS Minimum', type:'number'},
			trackerDuration:{group:lang.load["properties-group-general"], name:'Tracker Duration', type:'number'},
			levelTextureOptimization:{group:'Optimisation', name:'Level Texture', type:'number'},
			levelLensFlaresOptimization:{group:'Optimisation', name:'Level Lens Flares', type:'number'},
			levelShadowsOptimization:{group:'Optimisation', name:'Level Shadows', type:'number'},
			levelParticlesOptimization:{group:'Optimisation', name:'Level Particles', type:'number'},
			levelPostProcessesOptimization:{group:'Optimisation', name:'Level Post-Processes', type:'number'},
			levelRenderTargetsOptimization:{group:'Optimisation', name:'Level Render Targets', type:'number'},
			levelHardwareScalingOptimization:{group:'Optimisation', name:'Level Hardware Scaling', type:'number'}
		};
		this.creat(this.theObj, this.theMeta);
	};

})();