var CASTORENGINE = CASTORENGINE || {};

(function () {

	CASTORENGINE.editeObjet = function() {};

	CASTORENGINE.editeObjet.prototype.setProperties = function()
	{
		var propertyName = $("#proprieterCurrent").val();
		var dataPropertyGrid = $('#propGrid').PropertyGrid('get');
		if(nameObjetSelected) var meshesSelected = editor.objets[nameObjetSelected];
		var valProp = dataPropertyGrid[propertyName];
		//var data = JSON.stringify(dataPropertyGrid, null, '\t');// resultat du Json complet
		if(propertyName !== null)
		{
			if(meshesSelected && dataPropertyGrid.type == "meshes")
			{
				if(propertyName == "positionx") meshesSelected["position"]["x"] = valProp;
				else if(propertyName == "positiony") meshesSelected["position"]["y"] = valProp;
				else if(propertyName == "positionz") meshesSelected["position"]["z"] = valProp;
				else if(propertyName == "rotationx") meshesSelected["rotation"]["x"] = valProp;
				else if(propertyName == "rotationy") meshesSelected["rotation"]["y"] = valProp;
				else if(propertyName == "rotationz") meshesSelected["rotation"]["z"] = valProp;
				else if(propertyName == "scalex") meshesSelected["scaling"]["x"] = valProp;
				else if(propertyName == "scaley") meshesSelected["scaling"]["y"] = valProp;
				else if(propertyName == "scalez") meshesSelected["scaling"]["z"] = valProp;
				else { meshesSelected[propertyName] = valProp; }
			}
			else if(dataPropertyGrid.type == 'sprite')
			{
				if(propertyName == "positionx") meshesSelected["position"]["x"] = valProp;
				else if(propertyName == "positiony") meshesSelected["position"]["y"] = valProp;
				else if(propertyName == "positionz") meshesSelected["position"]["z"] = valProp;
				else { spriteManager[editor.properties.index] = valProp; }
			}
			else if(dataPropertyGrid.type == 'material')
			{
				materials[editor.properties.index] = valProp;
			}
			else if(dataPropertyGrid.type == 'procedural')
			{
				materialsProcedural[editor.properties.index][propertyName] = valProp;
			}
			else if(dataPropertyGrid.type == 'particules')
			{
				particleSystem[editor.properties.index][propertyName] = valProp;
			}
			else if(dataPropertyGrid.type == 'sounds')
			{
				sounds[editor.properties.index][propertyName] = valProp;
			}
			else if(dataPropertyGrid.type == 'script')
			{				
				if(propertyName == "name") {				
					var fichierExtension = basename(window.iframe3.script.ScriptActif);
					var fichier = fichierExtension.replace(".js", "");
					var saisie = prompt(lang.load["Nouveau nom"]+" : ", fichier);
					if (saisie !== null && saisie !== fichier) {
						$.ajax({ type: "POST",url: 'fonctions/script.php', data: "newNameScript=1&name=" + saisie + "&oldName=" + fichier,
							success: function (msg) {
								if(msg !== "failled") {
									window.iframe3.script.editeurcode.setValue(msg);
									window.iframe3.script.ScriptActif = "public_scripts/"+user+"/"+ saisie + "."+MyMode;
									window.iframe3.script.fichierSaisi = saisie;
								} 
							}
						});
					}
				}
			}
			else if(dataPropertyGrid.type == 'shapes')
			{
				if(propertyName == "shape") {
					var Impostor = null;
					if(valProp == "BoxImpostor") Impostor = BABYLON.PhysicsEngine.BoxImpostor;
					if(valProp == "SphereImpostor") Impostor = BABYLON.PhysicsEngine.SphereImpostor;
					if(valProp == "HeightmapImpostor") Impostor = BABYLON.PhysicsEngine.HeightmapImpostor;
					meshesSelected.setPhysicsState(Impostor, {
																mass: meshesSelected.getPhysicsRestitution(),
																friction: meshesSelected.getPhysicsFriction(),
																restitution: meshesSelected.getPhysicsRestitution()
															});
				}
				if(propertyName == "mass") {
					meshesSelected.setPhysicsState(meshesSelected.getPhysicsImpostor(), {
																	mass: valProp,
																	friction: meshesSelected.getPhysicsFriction(),
																	restitution: meshesSelected.getPhysicsRestitution()
																});
				}
				if(propertyName == "friction") {
					meshesSelected.setPhysicsState(meshesSelected.getPhysicsImpostor(), {
																	mass: meshesSelected.getPhysicsRestitution(),
																	friction: valProp,
																	restitution: meshesSelected.getPhysicsRestitution()
																});
				}
				if(propertyName == "restitution") {
					meshesSelected.setPhysicsState(meshesSelected.getPhysicsImpostor(), {
																	mass: meshesSelected.getPhysicsRestitution(),
																	friction: meshesSelected.getPhysicsFriction(),
																	restitution: valProp
																});
				}
			}
			else if(dataPropertyGrid.type == 'collider')
			{
				ColliderMesh[propertyName] = valProp;
			}
			else if(dataPropertyGrid.type == 'rigid body')
			{
				meshesSelected[propertyName] = valProp;
			}
			else if(dataPropertyGrid.type == "scene")
			{
				if(propertyName == "positionx") editor.nameScene = valProp;
				else { editor.scene[propertyName] = valProp; }
			}
			else if(dataPropertyGrid.type == 'engine')
			{
				editor.engine[propertyName] = valProp;
			}
			else if(dataPropertyGrid.type == 'fog')
			{
				editor.scene[propertyName] = valProp;
			}
			else if(dataPropertyGrid.type == 'FreeCamera' ||  dataPropertyGrid.type == 'ArcRotateCamera' ||  dataPropertyGrid.type == 'GamePadCamera' ||  dataPropertyGrid.type == 'TouchCamera' || dataPropertyGrid.type == 'FollowCamera')
			{
				if(dataPropertyGrid.type == 'FreeCamera') FreeCamera[editor.properties.index][propertyName] = valProp;
				if(dataPropertyGrid.type == 'ArcRotateCamera') ArcRotateCamera[editor.properties.index][propertyName] = valProp;
				if(dataPropertyGrid.type == 'TouchCamera') TouchCamera[editor.properties.index][propertyName] = valProp;
				if(dataPropertyGrid.type == 'FollowCamera') FollowCamera[editor.properties.index][propertyName] = valProp;
				if(dataPropertyGrid.type == 'GamePadCamera') GameadCamera[editor.properties.index][propertyName] = valProp;
			}
			else if(dataPropertyGrid.type == 'light')
			{
				if(dataPropertyGrid.light ==  'DirectionalLight') LightDirectional[editor.properties.index][propertyName] = valProp;
				if(dataPropertyGrid.light ==  'PointLight') LightPoint[editor.properties.index][propertyName] = valProp;
				if(dataPropertyGrid.light ==  'HemisphericLight') LightHemispherique[editor.properties.index][propertyName] = valProp;
				if(dataPropertyGrid.light ==  'SpotLight') LightSpot[editor.properties.index][propertyName] = valProp;
			}
			else if(dataPropertyGrid.type == 'lens flare')
			{
				lensFlareSystem[editor.properties.index][propertyName] = valProp;
			}
			else if(dataPropertyGrid.type == 'optimizer')
			{

			}
			else if(dataPropertyGrid.type == 'environnement')
			{

			}
		}
	};

})();