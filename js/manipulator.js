/// <reference path="../libs/babylon.d.ts" />

var CASTORENGINE = CASTORENGINE || {};

(function () {
    CASTORENGINE.Manipulator = function (mesh, connectedCanvas, control, locked, pickedPoint, camera)
	{
		this.mesh = mesh;
		this.camera = camera;
		this.pick = false;
        this.connectedCanvas = connectedCanvas;
        this.isActiveCamera = true;
        this.control = control;
        this.scaleLocked = locked;
        this.time = null;
        this.scalingMeshMulti = 400;
        this.positiopnMeshMulti = 50;
        this.scaleDistanceManipulator = 80;
		if(pickedPoint !== undefined) {
			this.scene = mesh.getScene();
			this.pickedPoint = pickedPoint;
		} else {
			this.pickedPoint = this.mesh.position;
		}
        this.mouseInfo = { downPosX: 0, downPosY: 0, downRay: null, movePosX: 0, movePosY: 0, upPosX: 0, upPosY: 0, onDownObject: null, onMoveObject: null, objectSelector: null };
        this.mesh.isPickable = false;
        var that = this;
        this.connectedCanvas.addEventListener("mousemove", function (event) { that._mousemove(event); });
        this.connectedCanvas.addEventListener("mouseup", function (event) { that._mouseup(event); });
        this.connectedCanvas.addEventListener("mousedown", function (event) { that._mousedown(event);});
        this._createParentVector();
    };

    CASTORENGINE.Manipulator.prototype._mouveObjectFromVector = function (vector, evt) {
        var vFromRay = this._getMouseRay(evt.offsetX, evt.offsetY, this.mouseInfo.onDownObject);
        var vToRay = this._getMouseRay(this.mouseInfo.movePosX, this.mouseInfo.movePosY, this.mouseInfo.onDownObject);
        var iDiff = vToRay.origin.subtract(vFromRay.origin);
        var distance = BABYLON.Vector3.Distance(vFromRay.origin, vToRay.origin);
		this.mesh.position["x"] = parseFloat(this.mesh.position.x);
        this.mesh.position["y"] = parseFloat(this.mesh.position.y);
        this.mesh.position["z"] = parseFloat(this.mesh.position.z);
		if (iDiff[vector] > 0) {
			this.mesh.position[vector] -= distance * this.positiopnMeshMulti;
			this.manipulate.position[vector] -= distance * this.positiopnMeshMulti;
			this.pickedPoint[vector] -= distance * this.positiopnMeshMulti;
        } else {
			this.mesh.position[vector] += distance * this.positiopnMeshMulti;
			this.manipulate.position[vector] += distance * this.positiopnMeshMulti;
			this.pickedPoint[vector] += distance * this.positiopnMeshMulti;
        }
    };

    CASTORENGINE.Manipulator.prototype._scaleObjectFromVector = function (vector, evt) {
        var vFromRay = this._getMouseRay(evt.offsetX, evt.offsetY, this.mouseInfo.onDownObject);
        var vToRay = this._getMouseRay(this.mouseInfo.movePosX, this.mouseInfo.movePosY, this.mouseInfo.onDownObject);
        var iDiff = vToRay.origin.subtract(vFromRay.origin);
        var distance = BABYLON.Vector3.Distance(vFromRay.origin, vToRay.origin);
		this.mesh.scaling["x"] = parseFloat(this.mesh.scaling.x);
        this.mesh.scaling["y"] = parseFloat(this.mesh.scaling.y);
        this.mesh.scaling["z"] = parseFloat(this.mesh.scaling.z);
        if (!this.scaleLocked) {
            if (iDiff[vector] > 0 && this.mesh.scaling[vector] > 0) {
                this.mesh.scaling[vector] -= distance * this.scalingMeshMulti;
            } else {
                this.mesh.scaling[vector] += distance * this.scalingMeshMulti;
            }
        } else {
            if (iDiff[vector] > 0 && this.mesh.scaling[vector] > 0) {
                this.mesh.scaling.x -= distance * this.scalingMeshMulti;
                this.mesh.scaling.y -= distance * this.scalingMeshMulti;
                this.mesh.scaling.z -= distance * this.scalingMeshMulti;
            } else {
                this.mesh.scaling.x += distance * this.scalingMeshMulti;
                this.mesh.scaling.y += distance * this.scalingMeshMulti;
                this.mesh.scaling.z += distance * this.scalingMeshMulti;
            }
        }
    };

	CASTORENGINE.Manipulator.prototype._rotateObjectFromVector = function (vector, evt) {
        var vFromRay = this._getMouseRay(evt.offsetX, evt.offsetY, this.mouseInfo.onDownObject);
        var vToRay = this._getMouseRay(this.mouseInfo.movePosX, this.mouseInfo.movePosY, this.mouseInfo.onDownObject);
        var iDiff = vToRay.origin.subtract(vFromRay.origin);
        var distance = BABYLON.Vector3.Distance(vFromRay.origin, vToRay.origin);
		this.mesh.rotation["x"] = parseFloat(this.mesh.rotation.x);
        this.mesh.rotation["y"] = parseFloat(this.mesh.rotation.y);
        this.mesh.rotation["z"] = parseFloat(this.mesh.rotation.z);
		if (parseFloat(iDiff[vector]) > 0) {
			this.mesh.rotation[vector] -= distance * this.positiopnMeshMulti;
			this.manipulate.rotation[vector] -= distance * this.positiopnMeshMulti;
        } else {
			this.mesh.rotation[vector] += distance * this.positiopnMeshMulti;
			this.manipulate.rotation[vector] += distance * this.positiopnMeshMulti;
        }
    };

    CASTORENGINE.Manipulator.prototype._createParentVector = function () {
        this.manipulate = BABYLON.Mesh.CreateBox(this.control, 0, this.scene);
		this.manipulate.visibility = 0;

        switch (this.control) {
            case CASTORENGINE.Manipulator.CONTROLMODE_POSITION:
                this._createVectorPosition(CASTORENGINE.Manipulator.CONTROLMODE_POSITION);
            break;
            case CASTORENGINE.Manipulator.CONTROLMODE_SCALE:
                this._createVectorPosition(CASTORENGINE.Manipulator.CONTROLMODE_SCALE);
            break;
            case CASTORENGINE.Manipulator.CONTROLMODE_ROTATION:
                this._createVectorRotation();
            break;
        }
    };

	CASTORENGINE.Manipulator.prototype._createAxis = function(scene, size) {
        this.empty = new BABYLON.Mesh("emptyAxis", scene);
		//X axis
        this.x = BABYLON.Mesh.CreateCylinder("x", size, 0.1, 0.1, 10, scene);
        this.x.material = new BABYLON.StandardMaterial("xColor", scene);
        this.x.material.diffuseColor = BABYLON.Color3.Green();
        this.x.position = new BABYLON.Vector3(size/2, 0, 0);
        this.x.rotation.z = Math.PI / 2;
        //Y axis
        this.y = BABYLON.Mesh.CreateCylinder("y", size, 0.1, 0.1, 10, scene);
        this.y.material = new BABYLON.StandardMaterial("yColor", scene);
        this.y.material.diffuseColor = BABYLON.Color3.Red();
        this.y.position = new BABYLON.Vector3(0, size/2, 0);
        //Z axis
        this.z = BABYLON.Mesh.CreateCylinder("z", size, 0.1, 0.1, 10, scene);
        this.z.material = new BABYLON.StandardMaterial("zColor", scene);
        this.z.material.diffuseColor = BABYLON.Color3.Blue();
        this.z.position = new BABYLON.Vector3(0, 0, size/2);
        this.z.rotation.x = Math.PI / 2;
	};

    CASTORENGINE.Manipulator.prototype._createVectorPosition = function (type) {
        switch (type) {
            case CASTORENGINE.Manipulator.CONTROLMODE_POSITION:
				this.arrowX = BABYLON.Mesh.CreateCylinder("x", 8, 2, 0.01, 12, 1, this.scene);
                this.arrowY = BABYLON.Mesh.CreateCylinder("y", 8, 0.01, 2, 12, 1, this.scene);
                this.arrowZ = BABYLON.Mesh.CreateCylinder("z", 8, 0.01, 2, 12, 1, this.scene);
            break;
            case CASTORENGINE.Manipulator.CONTROLMODE_SCALE:
				this.arrowX = BABYLON.Mesh.CreateBox("x", 1.2, this.scene);
                this.arrowY = BABYLON.Mesh.CreateBox("y", 1.2, this.scene);
                this.arrowZ = BABYLON.Mesh.CreateBox("z", 1.2, this.scene);
            break;
        }
		//* - - - Construction of the vector X (green)- - -
        this.vectorX = BABYLON.Mesh.CreateCylinder("x", 8, 0.2, 0.2, 12, 1, this.scene);
        this.vectorX.renderingGroupId = 2;
        var vectorXMaterial = new BABYLON.StandardMaterial("vectorXMaterial", this.scene);
        vectorXMaterial.diffuseColor = BABYLON.Color3.Green();
        vectorXMaterial.specularColor = BABYLON.Color3.Green();
        vectorXMaterial.specularPower = 32;
        vectorXMaterial.ambientColor = BABYLON.Color3.Green();
        vectorXMaterial.emissiveColor = BABYLON.Color3.Green();
        this.vectorX.material = vectorXMaterial;
        this.vectorX.rotation.z = Math.PI / 2;
        this.vectorX.position.x = 4.0;
        this.vectorX.parent = this.manipulate;

        this.arrowX.renderingGroupId = 2;
        this.arrowX.position.x = 7.5;
        this.arrowX.material = vectorXMaterial;
        this.arrowX.rotation.z = Math.PI / 2;
        this.arrowX.parent = this.manipulate;

	    //* - - - Construction of the vector Y (red)- - -
        this.vectorY = BABYLON.Mesh.CreateCylinder("y", 8, 0.2, 0.2, 12, 1, this.scene);
        this.vectorY.renderingGroupId = 2;
        var vectorYMaterial = new BABYLON.StandardMaterial("vectorYMaterial", this.scene);
        vectorYMaterial.diffuseColor = BABYLON.Color3.Red();
        vectorYMaterial.specularColor = BABYLON.Color3.Red();
        vectorYMaterial.specularPower = 32;
        vectorYMaterial.ambientColor = BABYLON.Color3.Red();
        vectorYMaterial.emissiveColor = BABYLON.Color3.Red();
        this.vectorY.material = vectorYMaterial;
        this.vectorY.position.y = 4.0;
        this.vectorY.parent = this.manipulate;

        this.arrowY.renderingGroupId = 2;
        this.arrowY.position.y = 8.0;
        this.arrowY.material = vectorYMaterial;
        this.arrowY.parent = this.manipulate;

        //* - - - Construction of the vector Z (blue) - - -
        this.vectorZ = BABYLON.Mesh.CreateCylinder("z", 8, 0.2, 0.2, 12, 1, this.scene);
        this.vectorZ.renderingGroupId = 2;
        var vectorZMaterial = new BABYLON.StandardMaterial("vectorZMaterial", this.scene);
        vectorZMaterial.diffuseColor = BABYLON.Color3.Blue();
        vectorZMaterial.specularColor = BABYLON.Color3.Blue();
        vectorZMaterial.specularPower = 32;
        vectorZMaterial.ambientColor = BABYLON.Color3.Blue();
        vectorZMaterial.emissiveColor = BABYLON.Color3.Blue();
        this.vectorZ.material = vectorZMaterial;
        this.vectorZ.rotation.x = Math.PI / 2;
        this.vectorZ.position.z = 4.0;
        this.vectorZ.parent = this.manipulate;

        this.arrowZ.renderingGroupId = 2;
        this.arrowZ.position.z = 7.5;
        this.arrowZ.rotation.x = Math.PI / 2;
        this.arrowZ.material = vectorZMaterial;
        this.arrowZ.parent = this.manipulate;
    };

    CASTORENGINE.Manipulator.prototype._createVectorRotation = function () {
        //* - - - Construction of the vector Y (red)- - -
        this.torusY = BABYLON.Mesh.CreateTorus("y", 18, 0.16, 30, this.scene);
        var vectorYMaterial = new BABYLON.StandardMaterial("vectorYMaterial", this.scene);
        this.torusY.renderingGroupId = 2;
        vectorYMaterial.diffuseColor = BABYLON.Color3.Red();
        vectorYMaterial.specularColor = BABYLON.Color3.Red();
        vectorYMaterial.ambientColor = BABYLON.Color3.Red();
        vectorYMaterial.emissiveColor = BABYLON.Color3.Red();
        this.torusY.material = vectorYMaterial;
        this.torusY.parent = this.manipulate;
        var bigTorus = BABYLON.Mesh.CreateTorus("y", 18, 1, 30, this.scene);
        var bigTorusMaterial = new BABYLON.StandardMaterial("vectorYMaterial", this.scene);
        bigTorusMaterial.alpha = 0;
        bigTorus.material = bigTorusMaterial;
        bigTorus.parent = this.manipulate;

        //* - - - Construction of the vector X (green)- - -
        this.torusX = BABYLON.Mesh.CreateTorus("x", 18, 0.16, 30, this.scene);
        this.torusX.renderingGroupId = 2;
        var vectorXMaterial = new BABYLON.StandardMaterial("vectorXMaterial", this.scene);
        vectorXMaterial.diffuseColor = BABYLON.Color3.Green();
        vectorXMaterial.specularColor = BABYLON.Color3.Green();
        vectorXMaterial.ambientColor = BABYLON.Color3.Green();
        vectorXMaterial.emissiveColor = BABYLON.Color3.Green();
        this.torusX.material = vectorXMaterial;
        this.torusX.rotation.z = Math.PI / 2;
        this.torusX.parent = this.manipulate;

        var bigTorusX = BABYLON.Mesh.CreateTorus("x", 18, 1, 30, this.scene);
        bigTorusX.material = bigTorusMaterial;
        bigTorusX.rotation.z = Math.PI / 2;
        bigTorusX.parent = this.manipulate;

        //* - - - Construction of the vector Z (blue) - - -
        this.torusZ = BABYLON.Mesh.CreateTorus("z", 18, 0.16, 30, this.scene);
        this.torusZ.renderingGroupId = 2;
        var vectorZMaterial = new BABYLON.StandardMaterial("vectorZMaterial", this.scene);
        vectorZMaterial.diffuseColor = BABYLON.Color3.Blue();
        vectorZMaterial.specularColor = BABYLON.Color3.Blue();
        vectorZMaterial.ambientColor = BABYLON.Color3.Blue();
        vectorZMaterial.emissiveColor = BABYLON.Color3.Blue();
        this.torusZ.material = vectorZMaterial;
        this.torusZ.rotation.x = Math.PI / 2;
        this.torusZ.parent = this.manipulate;
        var bigTorusZ = BABYLON.Mesh.CreateTorus("z", 18, 1, 30, this.scene);
        bigTorusZ.material = bigTorusMaterial;
        bigTorusZ.rotation.x = Math.PI / 2;
        bigTorusZ.parent = this.manipulate;
    };

    CASTORENGINE.Manipulator.prototype._mousemove = function (evt) {
        var onDownObjectName = null;
        var manipulateName = null;
        if (this.mouseInfo.onDownObject !== null) {
            onDownObjectName = this.mouseInfo.onDownObject.name;
            manipulateName = this.manipulate.name;
            switch (manipulateName) {
                case CASTORENGINE.Manipulator.CONTROLMODE_POSITION:
                    this._mouveObjectFromVector(this.mouseInfo.onDownObject.name, evt);
                break;
                case CASTORENGINE.Manipulator.CONTROLMODE_SCALE:
                    this._scaleObjectFromVector(this.mouseInfo.onDownObject.name, evt, false);
                break;
                case CASTORENGINE.Manipulator.CONTROLMODE_ROTATION:
                    this._rotateObjectFromVector(this.mouseInfo.onDownObject.name, evt);
                break;
            }
        }
        this.mouseInfo.movePosX = evt.offsetX;
        this.mouseInfo.movePosY = evt.offsetY;
    };

    CASTORENGINE.Manipulator.prototype._mousedown = function (evt) {
        this.mouseInfo.downPosX = evt.offsetX;
        this.mouseInfo.downPosY = evt.offsetY;
        this.mouseInfo.movePosX = evt.offsetX;
        this.mouseInfo.movePosY = evt.offsetY;
        if (CASTORENGINE.Manipulator.isSelected) {
            var pickResult = this.scene.pick(evt.offsetX, evt.offsetY, null, null, this.camera);
            if (pickResult.hit && pickResult.pickedMesh.parent == this.manipulate) {
                this.pick = true;
                this.mouseInfo.onDownObject = pickResult.pickedMesh;
                this.mouseInfo.downRay = this._getMouseRay(this.mouseInfo.downPosX, this.mouseInfo.downPosY, this.mouseInfo.onDownObject);
                this.camera.detachControl(this.connectedCanvas);
            }
        }
        else {
            this.mouseInfo.onDownObject = null;
        }
        if (this.mouseInfo.onDownObject === null && this.pick !== true) {
            this.camera.attachControl(this.connectedCanvas, true);
        }
    };

    CASTORENGINE.Manipulator.prototype._mouseup = function (evt) {
        if (this.mouseInfo.onDownObject !== null) {
            this.mouseInfo.onDownObject = null;
            this.pick = false;
            this.camera.attachControl(this.connectedCanvas, true);
        }
    };

    CASTORENGINE.Manipulator.prototype._getMouseRay = function (x, y, mesh) {
        var ray = this.scene.createPickingRay(x, y, BABYLON.Matrix.Identity());
        return ray;
    };

    CASTORENGINE.Manipulator.prototype.reset = function () {
        this.mouseInfo.onDownObject = null;
        this.manipulate.dispose(false);
        this.mesh.isPickable = true;
        return ("reset");
    };

    // Statics
    CASTORENGINE.Manipulator.CONTROLMODE_POSITION = 1;
    CASTORENGINE.Manipulator.CONTROLMODE_ROTATION = 2;
    CASTORENGINE.Manipulator.CONTROLMODE_SCALE = 3;
    CASTORENGINE.Manipulator.isSelected = false;
})();