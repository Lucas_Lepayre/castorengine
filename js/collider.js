var CASTORENGINE = CASTORENGINE || {};

(function () {

	CASTORENGINE.collider = function(visible, parentMesh)
	{
		this.collider = BABYLON.Mesh.CreateBox("collider_box", 0, scene, false);
		var modele = parentMesh.getBoundingInfo();
		this.collider.scaling = new BABYLON.Vector3(modele.boundingBox.maximum.x*2, modele.boundingBox.maximum.y*2, modele.boundingBox.maximum.z*2);
		this.collider.visibility = visible;
		this.collider.checkCollisions = true;
		this.collider.parent = parentMesh;
		this.colliderMaterial = new BABYLON.StandardMaterial("materialCollider", scene);
		this.colliderMaterial.wireframe = true;
		this.colliderMaterial.diffuseColor = new BABYLON.Color3(1.0, 1.0, 0);
		this.colliderMaterial.specularColor = BABYLON.Color3.Black();
		this.collider.material = this.colliderMaterial;
	};

	CASTORENGINE.collider.prototype.ajustement = function(posx, posy, posz, scalx, scaly, scalz)
	{
		this.collider.position = new BABYLON.Vector3(parseFloat(posx), parseFloat(posy), parseFloat(posz));
		this.collider.scaling = new BABYLON.Vector3(parseFloat(scalx), parseFloat(scaly), parseFloat(scalz));
	};

})();