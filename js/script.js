var CASTORENGINE = CASTORENGINE || {};

(function () {

	CASTORENGINE.script = function()
	{
		var that = this;
		this.load = 0;

		$("#count_references", window.parent.document.body).text(parseInt($("#count_references", window.parent.document.body).text()) + 3);
		$("#ex_references", window.parent.document.body).append('<a href="javascript:void(0)" class="reference" onClick="window.iframe3.script.selectReference("http://www.castorengine.com/babylon/babylon.js");" style="display:block;">http://www.castorengine.com/babylon/babylon.js</a>');
		$("#ex_references", window.parent.document.body).append('<a href="javascript:void(0)" class="reference" onClick="window.iframe3.script.selectReference("http://www.castorengine.com/babylon/hand.js");" style="display:block;">http://www.castorengine.com/babylon/hand.js</a>');
		$("#ex_references", window.parent.document.body).append('<a href="javascript:void(0)" class="reference" onClick="window.iframe3.script.selectReference("http://www.castorengine.com/babylon/cannon.js");" style="display:block;">http://www.castorengine.com/babylon/cannon.js</a>');

		CodeMirror.commands.autocomplete = function(cm) {
			var mode = CodeMirror.innerMode(cm.getMode(), cm.getTokenAt(cm.getDoc().getCursor()).state).mode.name;
			if (mode == 'javascript')
			{
				var orig = CodeMirror.hint.javascript;
				CodeMirror.hint.javascript = function(cm) {
					var inner = orig(cm) || {from: cm.getCursor(), to: cm.getCursor(), list: []};
					inner.list.splice(0, inner.list.length);
					//inner.list.push("WWW('Fichier.js', 'donnee1=x&donnee2=x', callback);");
					// autre inner.list.push ici
					return inner;
				};
				CodeMirror.showHint(cm, CodeMirror.hint.javascript);
			}
		};

		this.editeurcode = CodeMirror.fromTextArea(document.getElementById("code"), { lineNumbers: true, styleActiveLine: true, mode: "javascript", htmlMode: true, indentUnit : 4, extraKeys: {"Ctrl-Space": "autocomplete"}});

		setTimeout(function() {
			that.editeurcode.getWrapperElement().style.height = document.documentElement.clientHeight + 'px';
			that.editeurcode.refresh();
		}, 1500);
		this.editeurcode.setOption("theme", "monokai");
		this.editeurcode.on("change", function(instance, changeObj) {
			if(monMode == "javascript") { // on active pas la recherche d'erreur si on est pas sur la langue javascript
				if (changeObj.text[0] !== "." && changeObj.text[0] !== "="  && changeObj.text[0] !== "+"  && changeObj.text[0] !== "-"  && changeObj.text[0] !== "(" && changeObj.text[0] !== ")" && changeObj.text[0] !== "{" && changeObj.text[0] !== "}" && changeObj.text[0] !== "[" && changeObj.text[0] !== "]") {
					clearTimeout(waiting);
					waiting = setTimeout(that.updateHints(that), 2000);
				}
				else { clearTimeout(waiting); }
			}
		});
		this.change();
	};

	CASTORENGINE.script.prototype.change = function()
	{
		var that = this;
		this.editeurcode.on("changes", function(editeurcode, obj) {
			if(obj[0].origin != "setValue") {
				if(obj[0]) {
					if(obj[0].text[0] == ".") { CodeMirror.showHint(editeur); }
				}
			}
		});
	};

	CASTORENGINE.script.prototype.addFile = function(key, name, code)
	{
		var that = this;
		if(key == "references") {
			var url = prompt(lang.load["script-add-file-externe"]+" :", "");
			if(url != null) {
				$("#count_references", window.parent.document.body).text(parseInt($("#count_references", window.parent.document.body).text()) + 1);
				$("#ex_references", window.parent.document.body).append('<a href="javascript:void(0)" class="reference" onClick="window.iframe3.script.selectReference("'+url+'");" style="display:block;">'+url+'</a>');
			}
		}
		else if(key == "scripjs")
		{
			var saisie = prompt(lang.load["script-name"]+" :", "");
			if (saisie != null) {
				$.ajax({ type: "POST",url: 'fonctions/script.php', data: "newScript=1&name=" + saisie + "&user=" + user,
					success: function (msg) {
						if(msg !== "failled") {
							that.editeurcode.setValue(msg);
							ScriptActif = "public_scripts/"+user+"/"+ saisie + ".js";
							fichierSaisi = saisie;
							$("#ex_scripts", window.parent.document.body).append('<a href="javascript:void(0)" class="scripjs_'+saisie+'" onClick="window.iframe3.script.openScript(\''+fichierSaisi+'\')" style="display:block;">'+fichierSaisi+'</a>');
							$("#count_scripts", window.parent.document.body).text(parseInt($("#count_scripts", window.parent.document.body).text()) + 1);
							$(".selector", window.parent.document.body).accordion( "refresh" );
						} else {
							alert(lang.load["script-thisfile"] +" "+ saisie +" "+ lang.load["script-exist"]);
						}
					}
				});
			}
		}
		else if(key == "creatreference")
		{
			$("#count_references", window.parent.document.body).text(parseInt($("#count_references", window.parent.document.body).text()) + 1);
			$("#ex_references", window.parent.document.body).append('<a href="javascript:void(0)" class="reference" onClick="window.iframe3.script.selectReference("'+code+'");" style="display:block;">'+code+'</a>');
		}
		else if(key == "creatscript") {
			$.ajax({ type: "POST",url: 'fonctions/script.php', data: "creatScript=1&name=" + name + "&user=" + user + "&code="+code,
				success: function (msg) {
					if(msg == "ok") {
						that.editeurcode.setValue(code);
						ScriptActif = "public_scripts/"+user+"/"+ name + ".js";
						fichierSaisi = name;
						$("#ex_scripts", window.parent.document.body).append('<a href="javascript:void(0)" class="scripjs_'+name+'" onClick="window.iframe3.script.openScript(\''+fichierSaisi+'\')" style="display:block;">'+fichierSaisi+'</a>');
						$("#count_scripts", window.parent.document.body).text(parseInt($("#count_scripts", window.parent.document.body).text()) + 1);
						$(".selector", window.parent.document.body).accordion( "refresh" );
					}
				}
			});
		}
	};

	CASTORENGINE.script.prototype.contextMenu = function()
	{
		var that = this;		
		this.menuListeCreatForScript = {
			"references": {"name": window.parent.lang.load["script-context-menu-addref"], "icon": "edit"}, "step1":"--------------",
			"scripjs": {"name": window.parent.lang.load["script-context-menu-addscript"], "icon": "edit"}
		};
		$.contextMenu({ selector: '#body', callback: function(key, options) { that.addFile(key); }, items: this.menuListeCreatForScript });		
	};

	CASTORENGINE.script.prototype.selectReference = function(url)
	{
		window.open(url, lang.load["script-title-select-ref"]);
	};

	CASTORENGINE.script.prototype.save = function(cheminscript)
	{
		var cheminscript = cheminscript.replace(".js", ""), that = this;
		fichierName = basename(ScriptActif);
		fichierName = fichierName.replace(".js", "");
		$.ajax({ type: "POST",url: 'fonctions/script.php', data: "saveScript="+ ScriptActif + "&code="+ encodeURIComponent(this.editeurcode.getValue()) + "&name=" + fichierName,
			success: function (msg) {
				that.open(cheminscript);
			}
		});
	};

	CASTORENGINE.script.prototype.open = function(cheminscript)
	{
		if(fichierSaisi != null) {
			cheminscript = cheminscript.replace(".js", "");
			cheminscript = "public_scripts/"+user+"/"+basename(cheminscript)+".js";
			var that = this;
			$.ajax({ type: "POST", url: 'fonctions/script.php', data: "getScript="+ cheminscript,
				success: function (msg) {
					ScriptActif = cheminscript;
					that.editeurcode.setValue(msg);
					var fichierExtension = basename(cheminscript);
					cheminscript = cheminscript.replace("public_scripts/"+user+"/", "");
					var fichierName = cheminscript.replace(".js", "");
					fichierSaisi = fichierName;
				}
			});
		}
	};

	CASTORENGINE.script.prototype.openScript = function(cheminscript)
	{
		this.save(cheminscript); // on sauvegarde et on ouvre le nouveau script.
		//si aucun script n'est a enregistrer et on ouvre directement le scripts.
		if(fichierSaisi == null) {
			this.open(cheminscript);
		}
	};

	CASTORENGINE.script.prototype.updateHints = function(that)
	{
		that.editeurcode.operation(function()
		{
			JSHINT(that.editeurcode.getValue());
			parentDocument.getElementById("console_script").innerHTML = lang.load["layout-console-error"]+" : "+JSHINT.errors.length;
			for (var i = 0; i < JSHINT.errors.length; ++i) {
				var err = JSHINT.errors[i];
				if (!err) continue;
				var msg = document.createElement("div");
				var icon = msg.appendChild(document.createElement("span"));
				icon.innerHTML = "Error line: "+(err.line) + " !!";
				icon.className = "lint-error-icon";
				msg.appendChild(document.createTextNode(err.reason));
				msg.className = "lint-error";
				parentDocument.getElementById("console_script").appendChild(msg);
			}
		});
	};

})();