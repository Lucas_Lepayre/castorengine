/// <reference path="libs/babylon.d.ts" />

var CASTORENGINE = CASTORENGINE || {};

(function () {

	/*!
	* \class scene
	* Contructeur de la classe scene
	*/
	CASTORENGINE.scene = function()
	{
		BABYLON.Engine.ShadersRepository = "data/";

		this.canvas = document.getElementById("renderEditor");
		this.rootDebugLayer = document.getElementById("containeur");

		this.engine = new BABYLON.Engine(this.canvas, true);
		$("#versionEngine").html(BABYLON.Engine.Version);

		this.properties = new CASTORENGINE.properties();

		this.physique = new BABYLON.CannonJSPlugin();

		BABYLON.Engine.audioEngine.setGlobalVolume(0.6);

		this.scene = null;
		this.nameScene = "new projet";
		this.octree = null;
		this.cameraEditor = null;
		this.cameraPlayer = null;
		this.isCameraEditor = false;
		this.LightHemispheric = null;
		this.objets = [];
		this.show_stats_scene = false;
		this.sceneCharger = false;
		this.isLoguer = false;
		this.isFTPConnected = false;
		this.skybox = false;
	};
	/*!
	* Point d'entrer du chargement de la scene 3d
	*/
	CASTORENGINE.scene.prototype.init = function()
	{
		var that = this;

		this.engine.enableOfflineSupport = false;

		this.scene = new BABYLON.Scene(this.engine);
		this.scene.ShowLoadingScreen = false;
		this.scene.ForceFullSceneLoadingForIncremental = true;
		this.scene.clearColor = new BABYLON.Color3(0.20, 0.20, 0.20);
		this.scene.enablePhysics(new BABYLON.Vector3(0, -1.75, 0), this.physique);
		this.scene.gravity = new BABYLON.Vector3(0, -0.175, 0);
		this.scene.collisionsEnabled = true;
		this.scene.ambientColor = new BABYLON.Color3(0.3, 0.3, 0.3);
		//this.scene.workerCollisions = true;

		this.Camera(false);
		this._Light();

		// Loop of game
		this.engine.runRenderLoop(function () {
			that.scene.render();
            if(that.show_stats_scene === true){
                that.statistiques(that.engine, that.scene);
            }
		});

		// Resize engine
		layout.layoutW2uiDesing.on('resize', function(target, data) {
			data.onComplete = function () {
				that.engine.resize();
			};
		});
		layout.layoutW2uiStore.on('resize', function(target, data) {
			data.onComplete = function () {
				that.engine.resize();
			};
		});

		this.canvas.addEventListener('object:modified', function(e) {
			menu.saveState("modified");
		});

		this.scene.registerBeforeRender(function() {
			that.BeforeRender(that);
		});

		this.scene.executeWhenReady(function() {
			// Optimisation
			//that.octree = that.scene.createOrUpdateSelectionOctree();
			//BABYLON.SceneOptimizer.OptimizeAsync(that.scene, layout.addObjet.optionOptimizer());
			that.sceneCharger = true;
		});

		this.scene.onPointerDown = function (evt, pickResult) {
			if (pickResult.hit && pickResult.pickedMesh) {
				var nameObjet = pickResult.pickedMesh.name;
				if (nameObjet != "x" && nameObjet != "y" && nameObjet != "z") {
					that.selectEntity(pickResult.pickedMesh, "meshes");
				}
			}
		};

		this.canvas.addEventListener("mousedown", that.clickRight.bind(null, that), false);

		window.addEventListener('message', function(event) {
			if(~event.origin.indexOf('http:/')) {
				var data = event.data;
				layout.addObjet.importMediaStore(data.url, data.type);
			}
		});

		//window.addEventListener("keydown", that.avatar.controlKeyDown, false);
		//window.addEventListener("keyup", that.avatar.controlKeyUp, false);
	};

	CASTORENGINE.scene.prototype.BeforeRender = function(that)
	{
		if(that.scene.isReady()) {
			if(that.sceneCharger === true) {
				// Gizmo de transformation
				if(nameObjetSelected) {
					if(switchObjects[nameObjetSelected] !== null && switchObjects[nameObjetSelected].pickedPoint) {
						switchObjects[nameObjetSelected].vecDistanceCameraMesh = switchObjects[nameObjetSelected].pickedPoint.subtract(switchObjects[nameObjetSelected].camera.position);
						switchObjects[nameObjetSelected].vecDistanceCameraMesh.normalize();
						switchObjects[nameObjetSelected].manipulate.position = switchObjects[nameObjetSelected].vecDistanceCameraMesh.scale(switchObjects[nameObjetSelected].scaleDistanceManipulator).add(switchObjects[nameObjetSelected].camera.position);
						if(that.objets[nameObjetSelected]) {
							$('#castor0positionx').val(that.objets[nameObjetSelected].position.x);
							$('#castor0positiony').val(that.objets[nameObjetSelected].position.y);
							$('#castor0positionz').val(that.objets[nameObjetSelected].position.z);
							$('#castor0rotationx').val(that.objets[nameObjetSelected].rotation.x);
							$('#castor0rotationy').val(that.objets[nameObjetSelected].rotation.y);
							$('#castor0rotationz').val(that.objets[nameObjetSelected].rotation.z);
							$('#castor0scalex').val(that.objets[nameObjetSelected].scaling.x);
							$('#castor0scaley').val(that.objets[nameObjetSelected].scaling.y);
							$('#castor0scalez').val(that.objets[nameObjetSelected].scaling.z);
						}
					}
				}
			}
		}
	};

	/*!
	* Charge la camera de la scene
	*/
	CASTORENGINE.scene.prototype.Camera = function(player)
	{
		this.cameraEditor = new BABYLON.FreeCamera("cameraEditor", new BABYLON.Vector3(0, 0, -17), this.scene);
		this.cameraEditor.__fxaa_cookie = new BABYLON.FxaaPostProcess("fxaa", 1.0, this.cameraEditor, BABYLON.Texture.BILINEAR_SAMPLINGMODE, this.engine, false);
		this.cameraEditor.attachControl(this.canvas, true);
		this.scene.cameraToUseForPointers = this.cameraEditor;
		this.cameraEditor.keysUp.push(90); // Z
		this.cameraEditor.keysLeft.push(81); // Q
		this.cameraEditor.keysDown.push(83); // S
		this.cameraEditor.keysRight.push(68); // D
	};

	/*!
	* Charge la lumiere directionnelle de la scene par defaut et est détruite quand l'utilisateur ajoute ses lumières.
	*/
	CASTORENGINE.scene.prototype._Light = function()
	{
		var colorSpecular = BABYLON.Color3.Black();
		var colorDiffuse =  new BABYLON.Color3(0.80, 0.80, 0.80);
		this.LightHemispheric = new BABYLON.HemisphericLight("Ambiante", new BABYLON.Vector3(0, 1, 0), this.scene);
		this.LightHemispheric.diffuse = colorDiffuse;
		this.LightHemispheric.specular = colorSpecular;
		this.LightHemispheric.groundColor = BABYLON.Color3.Black();
		this.LightHemispheric.intensity = 1.6;
	};

	// Ajoute un gizmo de transformation suivant le mode de transformation.
	CASTORENGINE.scene.prototype._TransformGizmo = function(myMesh, name, pickPoint)
	{
		switchObjects[name] = new CASTORENGINE.Manipulator(myMesh, this.canvas, MODETRANSFORM, false, pickPoint, this.scene.activeCamera);
		this.canvas.addEventListener("mousemove", function (event) {
			if (nameObjetSelected) {
				CASTORENGINE.Manipulator.isSelected = true;
			} else {
				CASTORENGINE.Manipulator.isSelected = false;
			}
		});
		if(MODETRANSFORM == 3){ // Mode scaling
			if(switchObjects[name]) {
				window.addEventListener("keydown", function (evt) {
					if(evt.keyCode == 17) switchObjects[name].scaleLocked = true; // pour le scale en x + y + z
				});
				window.addEventListener("keyup", function (evt) {
					if(evt.keyCode == 17) switchObjects[name].scaleLocked = false; // pour le scale en x ou y ou z
				});
			}
		}
	};

	CASTORENGINE.scene.prototype.selectEntity = function(media, type)
	{
		if(media.name) {
			if(switchObjects[nameObjetSelected]) {
				this.resetGizmo();
			}
			if(type === null || type === undefined) type = "meshes";
			var pickPoint = pickPoint;
			nameObjetSelected = media.name;
			_mesh = media;
			_name = media.name;
			_pickPoint = media.position;
			switch(type) {
				case "meshes": case "primitifs": case "shapes":
					this.objets[nameObjetSelected] = media;
				break;
				case "sprites":
					spriteManager[this.properties.getIndex(spriteManager, media)] = media;
				break;
				case "sounds":
					sounds[this.properties.getIndex(sounds, media)] = media;
				break;
				case "scripts":
					Scripts[this.properties.getIndex(sounds, media)] = media;
				break;
				case "materials":
					materials[this.properties.getIndex(materials, media)] = media;
				break;
				case "lights_spot":
					LightSpot[this.properties.getIndex(LightSpot, media)] = media;
				break;
				case "lights_directionnnelle":
					LightDirectional[this.properties.getIndex(LightDirectional, media)] = media;
				break;
				case "lights_hemispherique":
					LightHemispherique[this.properties.getIndex(LightHemispherique, media)] = media;
				break;
				case "lights_point":
					LightPoint[this.properties.getIndex(LightPoint, media)] = media;
				break;
				case "FreeCamera":
					FreeCamera[this.properties.getIndex(FreeCamera, media)] = media;
				break;
				case "ArcRotateCamera":
					ArcRotateCamera[this.properties.getIndex(ArcRotateCamera, media)] = media;
				break;
				case "GamePadCamera":
					GameadCamera[this.properties.getIndex(GameadCamera, media)] = media;
				break;
				case "TouchCamera":
					TouchCamera[this.properties.getIndex(TouchCamera, media)] = media;
				break;
				case "FollowCamera":
					FollowCamera[this.properties.getIndex(FollowCamera, media)] = media;
				break;
				case "procedural":
					materialsProcedural[this.properties.getIndex(materialsProcedural, media)] = media;
				break;
				case "particle":
					particleSystem[this.properties.getIndex(particleSystem, media)] = media;
				break;
				case "textures":

				break;
				case "video":

				break;
				case "postprocess":

				break;
				case "reseau":

				break;
			}
			this._TransformGizmo(_mesh, _name, _pickPoint);
		}
		this.loadProperty(type);
	};

	CASTORENGINE.scene.prototype.clickRight = function(that, e)
	{
		e.preventDefault();
		var rightclick;
		if (!e) e = window.event;
		if (e.which) rightclick = (e.which == 3);
		else if (e.button) rightclick = (e.button == 2);
		if(rightclick === true) {
			that.resetGizmo();
		}
	};

	/*!
	* Afficher des panneau pour aider au debugade du scene ou pour verifier ce qui consomme...
	*/
	CASTORENGINE.scene.prototype.switchDebugLayer = function()
	{
		if(this.scene) {
        	if (this.scene.debugLayer.isVisible()) {
        		this.scene.debugLayer.hide();
            } else {
                this.scene.debugLayer.show(true, this.scene.activeCamera, this.rootDebugLayer);
				$("#DebugLayer").css({"left":"0px", "top":"0px"});
				this.show_stats_scene = false;
				$('#show_stats_scene').hide();
            }
        }
	};

	CASTORENGINE.scene.prototype.createAxis = function(size)
	{
		var that = this;
		var makeTextPlane = function(text, color, size) {
			var dynamicTexture = new BABYLON.DynamicTexture("DynamicTextureForAxisChar", 64, that.scene, true);
			dynamicTexture.drawText(text, 5, 40, "bold 30px Arial", color , "transparent", true);
			dynamicTexture.hasAlpha = true;
			var planeAxis = BABYLON.Mesh.CreatePlane("TextPlaneAxis", size, that.scene, true);
			planeAxis.isPickable = false;
			planeAxis.material = new BABYLON.StandardMaterial("TextPlaneAxisMaterial", that.scene);
			planeAxis.material.backFaceCulling = false;
			planeAxis.material.specularColor = BABYLON.Color3.Black();
			planeAxis.material.diffuseTexture = dynamicTexture;
			return planeAxis;
		};

		this._axisX = BABYLON.Mesh.CreateLines("_axisX", [BABYLON.Vector3.Zero(), new BABYLON.Vector3(size, 0, 0), new BABYLON.Vector3(size * 0.95, 0.05 * size, 0), new BABYLON.Vector3(size, 0, 0), new BABYLON.Vector3(size * 0.95, -0.05 * size, 0)], this.scene);
		this._axisX.isPickable = false;
		this._axisX.color = BABYLON.Color3.Green();
		this.xChar = makeTextPlane("X", "green", size / 10);
		this.xChar.position = new BABYLON.Vector3(1 * size, 0.03 * size, 0);
		this.xChar.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;

		this._axisY = BABYLON.Mesh.CreateLines("_axisY", [BABYLON.Vector3.Zero(), new BABYLON.Vector3(0, size, 0), new BABYLON.Vector3( -0.05 * size, size * 0.95, 0), new BABYLON.Vector3(0, size, 0), new BABYLON.Vector3( 0.05 * size, size * 0.95, 0)], this.scene);
		this._axisY.isPickable = false;
		this._axisY.color = BABYLON.Color3.Red();
		this.yChar = makeTextPlane("Y", "red", size / 10);
		this.yChar.position = new BABYLON.Vector3(0, 1 * size, -0.03 * size);
		this.yChar.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;

		this._axisZ = BABYLON.Mesh.CreateLines("_axisZ", [BABYLON.Vector3.Zero(), new BABYLON.Vector3(0, 0, size), new BABYLON.Vector3( 0 , -0.05 * size, size * 0.95),new BABYLON.Vector3(0, 0, size), new BABYLON.Vector3( 0, 0.05 * size, size * 0.95)], this.scene);
		this._axisZ.isPickable = false;
		this._axisZ.color = BABYLON.Color3.Blue();
		this.zChar = makeTextPlane("Z", "blue", size / 10);
		this.zChar.position = new BABYLON.Vector3(0, 0.03 * size, 1 * size);
		this.zChar.billboardMode = BABYLON.Mesh.BILLBOARDMODE_ALL;

		this.editeMode = true;

		//this.octree = this.scene.createOrUpdateSelectionOctree();
	};

	CASTORENGINE.scene.prototype.disposeAxis = function()
	{
		this._axisX.dispose();
		this._axisY.dispose();
		this._axisZ.dispose();
		this.xChar.dispose();
		this.yChar.dispose();
		this.zChar.dispose();
		this.editeMode = false;
		this.resetGizmo();
	};

	CASTORENGINE.scene.prototype.resetGizmo = function() {
		if(nameObjetSelected) {
			try {
				switchObjects[nameObjetSelected] = switchObjects[nameObjetSelected].reset();
			} catch(e) {
				switchObjects[nameObjetSelected+"Gizmo"] = switchObjects[nameObjetSelected+"Gizmo"].reset();
			}
			nameObjetSelected = null;
		}
	};

	/*!
	* Permet de mettre le navigateur en plein ecran ou de le quitter
	*/
	CASTORENGINE.scene.prototype.fullscreen = function()
	{
        // Mode plein ecran
		var element = null;
		if(this.full === false) {
			element = document.documentElement;
			if(element.requestFullscreen) {
				element.requestFullscreen();
			} else if(element.mozRequestFullScreen) {
				element.mozRequestFullScreen();
			} else if(element.webkitRequestFullscreen) {
				element.webkitRequestFullscreen();
			} else if(element.msRequestFullscreen) {
				element.msRequestFullscreen();
			}
			this.full = true;
			$("#fullscreen").text("Quitter plein écran");
		} else {
			element = document;
			if (element.exitFullscreen) {
				element.exitFullscreen();
			} else if (element.mozCancelFullScreen) {
				element.mozCancelFullScreen();
			} else if (element.webkitCancelFullScreen) {
				element.webkitCancelFullScreen();
			} else if (element.msExitFullscreen) {
				element.msExitFullscreen();
			}
			this.full = false;
			$("#fullscreen").text("Plein écran");
		}
	};

	CASTORENGINE.scene.prototype.showStats = function()
	{
		if(this.show_stats_scene === false) {
			this.show_stats_scene = true;
			$('#show_stats_scene').show();
			this.scene.debugLayer.hide();
		} else {
			this.show_stats_scene = false;
			$('#show_stats_scene').hide();
		}
	};

	CASTORENGINE.scene.prototype.statistiques = function(engine, scene)
	{
		$('#show_stats_scene').html("FPS: " + BABYLON.Tools.Format(engine.getFps(), 0) + " FPS<br />"+
			"Potential FPS: " + BABYLON.Tools.Format(1000.0 / scene.getLastFrameDuration(), 0) + "<br /><br />"+
			"<b>Count</b><br />"+
			"Total meshes: " + scene.meshes.length + "<br />"+
			"Total vertices: " + scene.getTotalVertices() + "<br />"+
			"Total materials: " + scene.materials.length + "<br />"+
			"Total textures: " + scene.textures.length + "<br />"+
			"Active meshes: " + scene.getActiveMeshes().length + "<br />"+
			"Active indices: " + scene.getActiveIndices() + "<br />"+
			"Active bones: " + scene.getActiveBones() + "<br />"+
			"Active particles: " + scene.getActiveParticles() + "<br />"+
			"Draw calls: " + engine.drawCalls + "<br /><br />"+
			"<b>Duration</b><br />"+
			"Meshes selection:</i> " + BABYLON.Tools.Format(scene.getEvaluateActiveMeshesDuration()) + " ms<br />"+
			"Render Targets: " + BABYLON.Tools.Format(scene.getRenderTargetsDuration()) + " ms<br />"+
			"Particles: " + BABYLON.Tools.Format(scene.getParticlesDuration()) + " ms<br />"+
			"Sprites: " + BABYLON.Tools.Format(scene.getSpritesDuration()) + " ms<br />"+
			"Render: " + BABYLON.Tools.Format(scene.getRenderDuration()) + " ms<br />"+
			"Frame: " + BABYLON.Tools.Format(scene.getLastFrameDuration()) + " ms<br /><br />"+
			"<b>Extensions</b><br />"+
			"Std derivatives: " + (engine.getCaps().standardDerivatives ? "Yes" : "No") + "<br />"+
			"Compressed textures: " + (engine.getCaps().s3tc ? "Yes" : "No") + "<br />"+
			"Hardware instances: " + (engine.getCaps().instancedArrays ? "Yes" : "No") + "<br />"+
			"Texture float: " + (engine.getCaps().textureFloat ? "Yes" : "No") + "<br />"+
			"32bits indices: " + (engine.getCaps().uintIndices ? "Yes" : "No") + "<br /><br />"+
			"<b>Caps.</b><br />"+
			"Max textures units: " + engine.getCaps().maxTexturesImageUnits + "<br />"+
			"Max textures size: " + engine.getCaps().maxTextureSize + "<br />"+
			"Max anisotropy: " + engine.getCaps().maxAnisotropy + "<br /><br />"+
			"<b>Info</b><br />"+
			engine.getGlInfo().renderer + "<br />"
		);
	};

	CASTORENGINE.scene.prototype.loadProperty = function(prop)
	{
		switch(prop) {
			case "empty": case "box": case "sphere": case "cylindre": case "torus": case "tube": case "plan": case "ligne": case "ground": case "importScene": case "importMesh": case "meshes":
				this.properties.showMesh();
			break;
			case "sprite":
				this.properties.showSprite();
			break;
			case "material":
				this.properties.showMaterial();
			break;
			case "procedural":
				this.properties.showProcedural();
			break;
			case "particules":
				this.properties.showParticulesSystem();
			break;
			case "sound":
				this.properties.showSound();
			break;
			case "script":
				this.properties.showScript();
			break;
			case "scene": case "scenes":
				this.properties.showScene();
			break;
			case "engine": case "engines":
				this.properties.showEngine();
			break;
			case "fog":
				this.properties.showFog();
			break;
			case "lensflare":
				this.properties.showLensFlare();
			break;
			case "rigidbody":
				this.properties.showRigidBody();
			break;
			case "collider":
				this.properties.showCollider();
			break;
			case "environnement":
				this.properties.showEnvironnement();
			break;
			case "FreeCamera": case "ArcRotateCamera": case "GamePadCamera": case "TouchCamera": case "FollowCamera":
				this.properties.showCamera();
			break;
			case "spot": case "point": case "directionnnelle": case "hemispherique": case "Light":
				this.properties.showLight();
			break;
			case "sphereshape": case "boxshape":
				this.properties.showPhysic();
			break;
			case "sceneOptimisation":
				this.properties.showOptimizer();
			break;
		}
	};

})();