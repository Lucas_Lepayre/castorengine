var CASTORENGINE = CASTORENGINE || {};

(function () {

	CASTORENGINE.menu = function()
	{
		this.modeGame = false;
	};

	CASTORENGINE.menu.prototype.outils = function()
	{
		var that = this;
		$("#new-projet").button({
			text: false,
			icons: {
				primary: "ui-icon-document"
			}
		}).click(function(){ that.newProjet(); });

		$("#open-projet").button({
			text: false,
			icons: {
				primary: "ui-icon-folder-open"
			}
		}).click(function(){ that.openProjet(); });

		$("#save-projet").button({
			text: false,
			icons: {
				primary: "ui-icon-disk"
			}
		}).click(function(){ that.saveProjet(); });

		$("#screeshot").button({
			text: false,
			icons: {
				primary: "ui-icon-image"
			}
		}).click(function(){
			if(editor.scene.isReady()) {
				var size = { precision: 0.6 };
				BABYLON.Tools.CreateScreenshot(editor.engine, editor.scene.activeCamera, size);
			}
		});

		$("#play").button({
			text: false,
			icons: { primary: "ui-icon-play" }
		}).click(function(){
			if(that.modeGame == false) {
				that.playGame();
				$("#play").button( "option", "icons", { primary: "ui-icon-stop" });
				that.modeGame = true;
			} else {
				that.stopGame();
				$("#play").button( "option", "icons", { primary: "ui-icon-play" });
				that.modeGame = false;
			}
		});

		$("#cloner").button({
			text: false,
			icons: { primary: "ui-icon-copy"}
		}).click(function(){ layout.addObjet.addCloneMesh(editor.objets[nameObjetSelected]); });

		$("#instancier").button({
			text: false,
			icons: { primary: "ui-icon-person"}
		}).click(function(){ layout.addObjet.addInstancyMesh(editor.objets[nameObjetSelected]); });

		$("#supprimer").button({
			text: false,
			icons: { primary: "ui-icon-closethick"}
		}).click(function(){
			if($( "#tabsEditor" ).tabs( "option", "active" ) == 0) { // on est sur l'onglet design, on suprime les objets
				var getAttribut = $("a#"+editor.objets[nameObjetSelected].name).parent().attr("id");
				var explorer = getAttribut.replace("ex_", "");
				$(".explorer").find("#count_"+explorer).text(parseInt($(".explorer").find("#count_"+explorer).text()) - 1);
				$(".explorer").find("a#"+editor.objets[nameObjetSelected].name).remove();
				$(".explorer_media").find("#count_"+explorer).text(parseInt($(".explorer_media").find("#count_"+explorer).text()) - 1);
				$(".explorer_media").find("a#"+editor.objets[nameObjetSelected].name).remove();
				editor.objets[nameObjetSelected].dispose();
				editor.resetGizmo();
			} else if($( "#tabsEditor" ).tabs( "option", "active" ) == 1) { // on est sur l'onglet code, on suprime les scripts ou references selectionner
				if (confirm(lang.load["menu-comfirm-delete-script"])) {
					$.ajax({ type: "POST",url: 'fonctions/script.php', data: "deleteScript="+ window.iframe3.ScriptActif,
						success: function (msg) {
							window.iframe3.script.editeurcode.setValue("");
							var fichierName = basename(window.iframe3.ScriptActif);
							var name = fichierName.replace(".js", "");
							var getAttribut = $("a.scripjs_"+name).parent().attr("id");
							var explorer = getAttribut.replace("ex_", "");
							$(".explorer_code").find("#count_"+explorer).text(parseInt($(".explorer_code").find("#count_"+explorer).text()) - 1);
							$(".explorer_code").find("a.scripjs_"+name).remove();
							$(".explorer_media").find("#count_"+explorer).text(parseInt($(".explorer_media").find("#count_"+explorer).text()) - 1);
							$(".explorer_media").find("a.scripjs_"+name).remove();
							window.iframe3.ScriptActif = null;
						}
					});
				}
			} else if($( "#tabsEditor" ).tabs( "option", "active" ) == 2) { // on est sur l'onglet media, on suprime les medias
				var getAttribut = $("a#"+media.objets[nameObjetSelected].name).parent().attr("id");
				var explorer = getAttribut.replace("ex_", "");
				$(".explorer_media").find("#count_"+explorer).text(parseInt($(".explorer_media").find("#count_"+explorer).text()) - 1);
				$(".explorer_media").find("a#"+media.objets[nameObjetSelected].name).remove();
				$(".explorer").find("#count_"+explorer).text(parseInt($(".explorer").find("#count_"+explorer).text()) - 1);
				$(".explorer").find("a#"+media.objets[nameObjetSelected].name).remove();
				media.objets[nameObjetSelected].dispose();
			}
		});

		$("#move").button({text: false, icons: { primary: "ui-icon-arrow-4"}}).click(function(){
			if(nameObjetSelected) {
				MODETRANSFORM = 1;
				editor.resetGizmo();
				nameObjetSelected = _name;
				editor._TransformGizmo(_mesh, _name, _pickPoint);
			}
		});

		$("#rotate").button({text: false, icons: { primary: "ui-icon-arrowrefresh-1-n"}}).click(function(){
			if(nameObjetSelected) {
				MODETRANSFORM = 2;
				editor.resetGizmo();
				nameObjetSelected = _name;
				editor._TransformGizmo(_mesh, _name, _pickPoint);
			}
		});

		$("#scale").button({text: false, icons: { primary: "ui-icon-arrow-4-diag"}}).click(function(){
			if(nameObjetSelected) {
				MODETRANSFORM = 3;
				editor.resetGizmo();
				nameObjetSelected = _name;
				editor._TransformGizmo(_mesh, _name, _pickPoint);
			}
		});

		$( "#repeat" ).buttonset();
	};

	CASTORENGINE.menu.prototype.menuFile = function(type)
	{
		switch(type) {
			case "new": this.newProjet(); break;
			case "open": this.openProjet(); break;
			case "openServeur": this.openProjetServeur(); break;
			case "save": this.saveProjet(); break
			case "saveServeur": this.saveProjetServeur(); break;
			case "publish": this.publishGame(); break;
			case "exportZip": this.exportZip(); break;
			case "about":
				var hauteur = 380, largeur = 660,
				top=(screen.height - hauteur)/2,
				left=(screen.width - largeur)/2;
				window.open("about.php",""+lang.load["menu-title-about"]+"","top="+top+",left="+left+",width="+largeur+",height="+hauteur+",toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no");
			break;
		}
	};

	CASTORENGINE.menu.prototype.menuEdite = function(type)
	{
		switch(type) {
			case "undo": editor.canvas.loadFromJSON($(".redo").val(), editor.canvas.renderAll()); break;
			case "redo": editor.canvas.loadFromJSON($(".undo").val(), editor.canvas.renderAll()); break;
			case "French": this.changeLangue("French"); break;
			case "English": this.changeLangue("English"); break;
			case "add_lang": this.add_lang(); break;
		}
	};

	CASTORENGINE.menu.prototype.menuCompte = function(type)
	{
		switch(type) {
			case "login": this.login(); break;
			case "profil": this.profil(); break;
			case "games": this.games(); break;
			case "actualiter": this.actualiter(); break;
			case "commentaire": this.commentaire(); break;
		}
	};

	CASTORENGINE.menu.prototype.menuDisplay = function(type)
	{
		switch(type) {
			case "stats": editor.showStats(); break;
			case "debug": editor.switchDebugLayer(); break;
			case "properties": layout.openProperties(); break;
			case "store": layout.openStore(); break;
		}
	};

	CASTORENGINE.menu.prototype.login = function()
	{
		var form =  '<div id="dialog-login" title="Connexion">'+
						'<form>'+ 
							'<label for="name">'+lang.load["menu-form-login-pseudo"]+' :</label><br /><input type="text" name="pseudo-or-mail" id="pseudo-or-mail" value="" class="text ui-widget-content ui-corner-all"><br />'+
							'<label for="password">'+lang.load["menu-form-login-password"]+' :</label><br /><input type="password" name="password" id="password" value="" class="text ui-widget-content ui-corner-all">'+
							'<input type="submit" style="position:absolute;top:-1000px">'+
						'</form>'+
					'</div>';
		layout.layoutW2uiDesing.content("main", {
			render: function() {
				$("#containeur").append(form);
				$("#dialog-login").dialog({
					autoOpen: true, height: 200, width: 220, modal: true,
					buttons: {
						"Connexion": function() {
							var that = this;
							$.ajax({ type: "POST", url: 'http://www.castorengine.com/setup.php', crossDomain: true, data: "pseudo-or-mail="+ $("#pseudo-or-mail").val()+"&password="+$("#password").val(),
								success: function (msg) {
									if(msg.trim() == "ok") {
										editor.isLoguer = true;
										$("#li_connexion").remove();
										$("#li_profil").show();
										$(that).dialog("close");
									} else {
										alert(lang.load["menu-form-login-error"]);
									}
								}
							});
						}, Cancel: function() { $(this).dialog( "close" ); }
					}, close: function() { $(this).dialog( "close" ); }
				});
			}
		});
	};

	CASTORENGINE.menu.prototype.loginFTP = function()
	{
		var form =  '<div id="dialog-login" title="Connexion FTP">'+
						'<form>'+
							'<label for="name">'+lang.load["menu-form-login-host"]+' :</label><br /><input type="text" name="host" id="host" value="" class="text ui-widget-content ui-corner-all"><br />'+
							'<label for="name">'+lang.load["menu-form-login-user"]+' :</label><br /><input type="text" name="user" id="user" value="" class="text ui-widget-content ui-corner-all"><br />'+
							'<label for="password">'+lang.load["menu-form-login-password"]+' :</label><br /><input type="password" name="password" id="password" value="" class="text ui-widget-content ui-corner-all">'+
							'<input type="submit" style="position:absolute;top:-1000px">'+
						'</form>'+
					'</div>';
		layout.layoutW2uiDesing.content("main", {
			render: function() {
				$("#containeur").append(form);
				$("#dialog-login").dialog({
					autoOpen: true, height: 230, width: 250, modal: true,
					buttons: {
						"Connexion": function() {
							var that = this;
							$.ajax({ type: "POST", url: 'fonctions/ftp.php', data: "host="+$("#host").val()+"user="+$("#user").val()+"&password="+$("#password").val(),
								success: function (msg) {
									if(msg.trim() == "ok") {
										editor.isFTPConnected = true;
										$(that).dialog("close");
									} else {
										alert(lang.load["menu-form-login-error"]);
									}
								}
							});
						}, Cancel: function() { $(this).dialog( "close" ); }
					}, close: function() { $(this).dialog( "close" ); }
				});
			}
		});
	};

	CASTORENGINE.menu.prototype.changeLangue = function(langage)
	{
		lang.setCookie("langCastor", langage, 365);
		var conf = confirm(lang.load["menu-sub-menu-changelang"]);
		if(conf == true) {
			window.location.href = "index.php?lang="+langage;
			lang.lang = langage;
			lang.setCookie("langCastor", langage, 365);
			window.location.refresh();
		} else {
			this.saveProjet();
		}
	};

	CASTORENGINE.menu.prototype.profil = function()
	{
		var form =  '<div id="dialog-profil" title="'+lang.load["menu-form-title-profil"]+'"><iframe src="http://www.castorengine.com/compte.php?editeur=1" align="center" width="100%" height="100%" frameborder="0" scrolling="yes" id="iframe1" name="iframe1"></iframe></div>';
		layout.layoutW2uiDesing.content("main", {
			render: function() {
				$("#containeur").append(form);
				$("#dialog-profil").dialog({
					autoOpen: true, height: 600, width: 1024,
					buttons: {
						"Close": function() { $(this).dialog("close"); }
					}, close: function() { $(this).dialog( "close" ); }
				});
			}
		});
	};

	CASTORENGINE.menu.prototype.add_lang = function()
	{
		var hauteur = 600, largeur = 960,
		top=(screen.height - hauteur)/2,
		left=(screen.width - largeur)/2;
		window.open("add_lang.php",""+lang.load["menu-form-title-addlang"]+"","top="+top+",left="+left+",width="+largeur+",height="+hauteur+",toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no");
	};

	CASTORENGINE.menu.prototype.actualiter = function()
	{
		window.open("http://www.castorengine.com/add_news.php");
	};

	CASTORENGINE.menu.prototype.commentaire = function()
	{
		window.open("http://www.castorengine.com/avis.php.php?auteur="+user+"projet="+editor.nameScene);
	};

	CASTORENGINE.menu.prototype.games = function()
	{
		window.open("http://www.castorengine.com/games.php");// aller vers la page des jeux publier
	};

	CASTORENGINE.menu.prototype.newProjet = function()
	{
		editor.engine.dispose();
		editor.scene.dispose();
		delete editor;
		editor = new CASTORENGINE.scene();
		editor.init();
		editor.properties.showScene();
		$('.explorer').find("a").each(function() {
			var getAttribut = $(this).parent().attr("id");
			var explorer = getAttribut.replace("ex_", "");
			$("#count_"+explorer).text("0");
			(this).remove();
		});
		layout.addObjet.addExplorer({name: "new scene"}, "scenes");
	};

	CASTORENGINE.menu.prototype.openProjet = function()
	{
		var that = this;
		if($( "#tabsEditor" ).tabs( "option", "active" ) == 0) {
			layout.layoutW2uiDesing.content("main", {
				render: function() {
					$("#containeur").append('<input type="file" id="file-input" style="display:none;" />');
					$("#file-input").trigger("click");
					$("#file-input").on('change', function(e) {
						var file = e.target.files[0];
						if (!file) { return; }
						var reader = new FileReader();
						reader.onload = function(e) {
							// charge la scene
							var contents = $.parseJSON(e.target.result);
							var sceneToLoad = JSON.stringify(contents.world);
							layout.addObjet.loadScene("", sceneToLoad, editor.engine);
							// charge les scripts
							if(contents.scripts) {
								$.each(contents.scripts, function(name) {
									window.iframe3.script.addFile("creatscript", name, contents.scripts[name]);
								});
							}
							// charge les reference externe
							if(contents.references) {
								$.each(contents.references, function(url) {
									window.iframe3.script.addFile("creatreference", null, contents.references[url]);
								});
							}
						};
						reader.readAsText(file);
					});
				}
			});
		} else {
			alert(lang.load["menu-info-open-projet"]);
		}
	};

	CASTORENGINE.menu.prototype.openProjetServeur = function()
	{
		// Ouverture d'un projet du serveur du client et on ajoute le projet dans l'explorer.
		if(editor.isFTPConnected == true) {
			$.ajax({ type: "GET", url: 'fonctions/ftp.php', data: "listingFTP=true",
				success: function (msg) {
					layout.layoutW2uiDesing.content("main", {
						render: function() {
							var form =  '<div id="dialog-liste-ftp" title="Mon FTP">'+ msg +'</div>';
							$("#containeur").append(form);
							$("#dialog-liste-ftp").dialog({
								autoOpen: true, height: 500, width: 900,
								buttons: {
									"Close": function() { $(this).dialog("close"); }
								}, close: function() { $(this).dialog( "close" ); }
							});
						}
					});
				}
			});
		} else { this.loginFTP(); }
	};

	CASTORENGINE.menu.prototype.saveProjet = function(isZip)
	{
		if(editor.isLoguer == true) {
			if(switchObjects[nameObjetSelected]) { editor.resetGizmo(); }
			var serializedScene = BABYLON.SceneSerializer.Serialize(editor.scene);
			var world = {};
			world.references = {};
			world.world = serializedScene;
			var fichierName = window.iframe3.fichierSaisi;
			$.ajaxSetup({ async: false});
			$("#ex_references").find("a.reference").each(function(url) {
				world.references[basename($(this).html())] = $(this).html();
			});
			$.ajax({ type: "POST",url: 'fonctions/script.php', data: "setScript=1&user="+user,
				success: function (msg) {
					if(msg) {
						world.scripts = $.parseJSON(msg);
					}
					$.ajax({ type: "POST", url: 'fonctions/sauvegarder.php', data: "type=final&user="+user+"&data="+encodeURIComponent(JSON.stringify(world)),
						success: function (readfile) {
							if(readfile == "ok") {
								if(isZip == undefined) {
									document.location.href='readfile.php?type=castor3d', target='_blank';
								}
							}
						}
					});
				}
			});
			$.ajaxSetup({ async: true});
		} else { this.login(); }
	};

	CASTORENGINE.menu.prototype.saveProjetServeur = function()
	{
		// On enregistre le projet complet sous le format Json et sur le serveur de l'utilisateur
		if(editor.isFTPConnected == true) {
			if(switchObjects[nameObjetSelected]) { editor.resetGizmo(); }
			var serializedScene = BABYLON.SceneSerializer.Serialize(editor.scene);			
			var world = {};
			world.references = {};
			world.world = serializedScene;	
			$.ajaxSetup({ async: false});
			$("#ex_references").find("a.reference").each(function(url) {
				world.references[basename($(this).html())] = $(this).html();
			});
			$.ajax({ type: "POST",url: 'fonctions/script.php', data: "setScript=1&user="+user,
				success: function (msg) {
					if(msg) {
						world.scripts = $.parseJSON(msg);
					}
					$.ajax({ type: "GET", url: 'fonctions/sauvegarder.php', data: "saveProjet=true&projetName=&data="+encodeURIComponent(JSON.stringify(world)),
						success: function (msg) {
							layout.layoutW2uiDesing.content("main", {
								render: function() {
									var form =  '<div id="dialog-liste-ftp" title="Mon FTP">'+ msg +'</div>';
									$("#containeur").append(form);
									$("#dialog-liste-ftp").dialog({
										autoOpen: true, height: 500, width: 900,
										buttons: {
											"Enregistrer dans": function() {
												var that = this;
												var saisie = prompt("Nom du dossier", "");
												if (saisie !== null) {
													$.ajax({ type: "POST", url: 'fonctions/ftp.php', data: "makeDir=true&dossierName="+saisie,
														success: function (msg) {
															alert(lang.load["menu-info-save-projet"]);
															$(that).dialog("close");
														}
													});
												}
											}, "Enregistrer": function() {
												var that = this;
												$.ajax({ type: "POST", url: 'fonctions/ftp.php', data: "makeDir=true&dossierName=0",
													success: function (msg) {
														alert(lang.load["menu-info-save-projet"]);
														$(that).dialog("close");
													}
												});

											}, "Close": function() { $(this).dialog("close"); }
										}, close: function() { $(this).dialog( "close" ); }
									});
								}
							});
						}
					});
				}
			});
			$.ajaxSetup({ async: true});
		} else { this.loginFTP(); }
	};

	CASTORENGINE.menu.prototype.publishGame = function()
	{
		$.ajaxSetup({ async: false});
		this.saveProjet(true);
		var ref = [];
		$.ajaxSetup({ async: false});
		$("#ex_references").find("a.reference").each(function(url) {
			ref.push($(this).html());
		});
		var serializedScene = BABYLON.SceneSerializer.Serialize(editor.scene);
		// verification de base avant envoie
		var verif = 0;
		if(editor.scene.meshes.length > 0) verif += 1; // si on a au moin un objet sur la scene
		if(parseInt($("#count_scripts").val()) > 0) verif += 1; // si on a créer au moin 1 scripts
		if(editor.scene.activeCamera) verif += 1; // si on a une camera d'active sur la scene
		if(parseInt($("#count_lights").val()) > 0) verif += 1; // si on a ajouter une lumière
		if(verif == 4) {
			$.ajax({ type: "POST", url: 'fonctions/publier.php', data: "scene="+JSON.stringify(serializedScene)+"&creatReferences="+ref.join(";")+"&user="+user,
				success: function (msg) {
					var hauteur = 390, largeur = 730,
					top=(screen.height - hauteur)/2,
					left=(screen.width - largeur)/2;
					window.open("http://www.castorengine.com/publish.php?projetName="+editor.nameScene,"Publier","top="+top+",left="+left+",width="+largeur+",height="+hauteur+",toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no");
				}
			});
		} else {
			alert(lang.load["menu-info-save-scene-impomplete"]);
		}
		$.ajaxSetup({ async: true});
	};

	CASTORENGINE.menu.prototype.exportZip = function()
	{
		$.ajaxSetup({ async: false});
		this.saveProjet(true);
		var ref = [];
		$.ajaxSetup({ async: false});
		$("#ex_references").find("a.reference").each(function(url) {
			ref.push($(this).html());
		});
		var serializedScene = BABYLON.SceneSerializer.Serialize(editor.scene);
		$.ajax({ type: "POST", url: 'fonctions/ziper.php', data: "zipfile=1&scene="+JSON.stringify(serializedScene)+"&creatReferences="+ref.join(";")+"&user="+user,
			success: function (msg) {
				document.location.href='readfile.php?type=zip', target='_blank';
			}
		});
		$.ajaxSetup({ async: true});
	};

	CASTORENGINE.menu.prototype.playGame = function()
	{
		// On teste le jeu directement sur le canvas et on stop le mode editeur
		layout.layoutW2uiDesing.hide('left', true);
		layout.layoutW2uiDesing.hide('right', true);
		layout.layoutW2uiDesing.hide('bottom', true);

		// On change de camera pour utiliser celle de l'utilisateur qui est celle active.


	};

	CASTORENGINE.menu.prototype.stopGame = function()
	{
		// On stop le jeu et on revient au mode editeur
		layout.layoutW2uiDesing.show('left', true);
		layout.layoutW2uiDesing.show('right', true);
		layout.layoutW2uiDesing.hide('bottom', true);

		// On change la camera pour revenir a celle de l'editeur.

	};

	CASTORENGINE.menu.prototype.saveState = function(currentAction) {
		currentAction = currentAction || '';
		$(".redo").val($(".undo").val());
		$(".undo").val(JSON.stringify(editor.canvas));
		console.log("Saving After " + currentAction);
		lastAction = currentAction;
		var objects = editor.canvas.getObjects();
		for (i in objects) {
			if (objects.hasOwnProperty(i)) {
				objects[i].setCoords();
			}
		}
	};

})();


