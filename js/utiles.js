var writeViewPort = function() {
    var ua = navigator.userAgent;
    if (ua.indexOf("Android") >= 0 && ua.indexOf("AppleWebKit") >= 0) {
        if (parseFloat(ua.slice(ua.indexOf("AppleWebKit") + 12)) < 535) {
			document.write('<meta name="viewport" content="initial-scale=1" />');
		}
    }
}; writeViewPort();
var objectCreate = function(proto) { //Alternative aux anciens navigateurs qui ne dispose pas de la méthode Object.create()
    function construct() { }
    construct.prototype = proto;
    return new construct();
};
var Extends = function(ChildClass, ParentClass) { // ClassB (child) herite de classA (parent)
    ChildClass.prototype = Object.create(ParentClass.prototype) || objectCreate(ParentClass.prototype);
	ChildClass.prototype.constructor = ChildClass;
};
var Super = function(ChildClass) { //appel du constructeur de la classe mere
	ChildClass.call(this);
};
var remove_item = function(arr, value){
	for(var b in arr ){ if(arr[b] == value) { arr.splice(b,1); break;} } return arr;
};
var basename = function(path, suffix) { // Recupere le nom d'un fichier
	if(path !== '' || path !== null) var b = path.replace(/^.*[\/\\]/g, '');
	if (typeof suffix === 'string' && b.substr(b.length - suffix.length) == suffix) {
		b = b.substr(0, b.length - suffix.length);
	}
	return b;
};
var getFile = function(fullPath) { // recuper le fichier avec l'extention d'un chemin complet
	var filename = fullPath.replace(/^.*[\\\/]/, '');
	return filename;
};
var no_extention = function(path) { // Recupere le fichier sans l'extention
	var ext = path.split('.').pop();
	var pathNoExtention = path.replace("."+ext, "");
	return pathNoExtention;
};
var extention = function(path) { // Recupere l'extention d'un fichier
	return path.split('.').pop();
};
var stringToBoolean = function(myString) {
	switch(myString.toLowerCase()) {
		case "true": case "yes": case "1": return true;
		case "false": case "no": case "0": case null: return false;
		default: return Boolean(myString);
	}
};
var file_exists = function(file) { // Verifie l'existance d'un fichier
	$.ajaxSetup({ async: false});
	var retour;
	$.ajax({ url: file, success: function(data){ retour = true;}, error: function(data){ retour = false; }});
	$.ajaxSetup({ async: true});
	return retour;
};
var rafraichir = function(id, url) {
	var e = document.getElementById(id);
	if(e) {
		var xhr_object = null;
		if(window.XMLHttpRequest) xhr_object = new XMLHttpRequest();
		else if (window.ActiveXObject) xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
		xhr_object.open("GET", url, true);
		xhr_object.onreadystatechange = function() {
			if(xhr_object.readyState < 4) {
				e.innerHTML = "<br /><br /><center><img src='images/loading.gif' style='z-index:1010;position:absolute;margin-left:-48px;margin-top:-20px' /></center>";
			}
			if(xhr_object.readyState == 4 && xhr_object.status == 200) {
				var sourceHTML = xhr_object.responseText;
				sourceHTML = sourceHTML.replace(/&(l|g|quo)t;/g, function(a,b){ return {l:'<', g:'>', quo:'"' }[b];});
				e.innerHTML = sourceHTML;
				var scripts = e.getElementsByTagName('script');
				for(var i=0; i < scripts.length;i++) {
					if (window.execScript) {
						window.execScript(scripts[i].text.replace('<!--',''));
					} else {
						window.eval(scripts[i].text);
					}
				}
			}
		};
		try {
			$(window).bind('beforeunload', function () {
				delete(xhr_object);
			});
		} catch(e){
			$(window).unload(function () {
				delete(xhr_object);
			});
		}
		xhr_object.send(null);
	}
};
var hexToR = function(h) {return parseInt((cutHex(h)).substring(0,2),16)};
var hexToG = function(h) {return parseInt((cutHex(h)).substring(2,4),16)};
var hexToB = function(h) {return parseInt((cutHex(h)).substring(4,6),16)};
var cutHex = function(h) {return (h.charAt(0)=="#") ? h.substring(1,7):h};
var toHex = function(n) { n = parseInt(n,10); if (isNaN(n)) return "00"; n = Math.max(0,Math.min(n,255)); return "0123456789abcdef".charAt((n-n%16)/16) + "0123456789abcdef".charAt(n%16); };
// Gestion des couleurs (convertion hexa et rgb)
var hexToRgb = function(hex) {
	var c_r = hexToR(hex) / 255,
		c_g = hexToG(hex) / 255,
		c_b = hexToB(hex) / 255;
	return {r: c_r, g: c_g, b: c_b};
};
var rgbToHex = function(R,G,B) {
	return "#" + toHex(R*255)+toHex(G*255)+toHex(B*255);
};
$.fn.textWidth = function(){
	var self = $(this),
		children = self.contents(),
		calculator = $('<span style="white-space:nowrap;" />'),
		width;
	children.wrap(calculator);
	width = children.parent().width();
	children.unwrap();
	return width;
};