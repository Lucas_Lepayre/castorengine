<?php
header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<html oncontextmenu="return false">
	<head>
		<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
		<link rel="stylesheet" type="text/css" href="css/codemirror.css">
		<link rel="stylesheet" type="text/css" href="css/monokai.css">
		<link rel="stylesheet" type="text/css" href="css/show-hint.css">
		<link rel="stylesheet" type="text/css" href="css/lint.css">
		<link rel="stylesheet" type="text/css" href="css/editor.css">
		<link rel="stylesheet" type="text/css" href="css/jquery.contextMenu.css" />

		<script src="libs/jquery.min.js" type="text/javascript"></script>
		<script src="libs/jquery-ui.min.js" type="text/javascript"></script>
		<script src="js/lang.js" type="text/javascript"></script>
		<script>
			jQuery.support.cors = true;
			var script = null,
			user = "<?php echo @$_COOKIE['pseudo'];?>",
			ScriptActif,
			changement,
			fichierSaisi = null,
			widgets = [],
			waiting,
			monMode = "javascript",
			lang = new LANG.loading(),
			parentDocument = window.parent.document;
		</script>
	</head>
	<body id="body">
		<?php		
		require_once("classes/jsmin.class.php");
		?>
		<form>
			<textarea id="code" name="code"></textarea>
		</form>
		<script src="js/utiles.js" type="text/javascript"></script>
		<script src="libs/codemirror.js" type="text/javascript"></script>
		<script src="libs/jshint.js" type="text/javascript"></script>
		<script src="libs/jquery.contextMenu.js" type="text/javascript"></script>

		<script src="js/script.js" type="text/javascript"></script>
		<script>
		script = new CASTORENGINE.script();
		script.contextMenu();
		</script>
	</body>
</html>