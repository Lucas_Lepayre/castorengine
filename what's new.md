## Version 5.1: ##

	- **Nouvelle fonctionnalités** 
	
	- **Updates**	

	- **Bug fixes**  

	- **Changements**

--------------------------------
## Version 5.0: ##

- **Nouvelle fonctionnalités**  
	- Sauvegarde dans un fichier .castor3d et export dans un zip ses projets sur son ordinateur ou vers un FTP.
	- Import d'objets, de scènes et autres médias par URL ou/et par la boutique intégrée.
	- Partage possible de média importé via la boutique intégrer à l'éditeur.
	- Connexion et modification de profil a partir de l'éditeur.
	- Publier un projet/jeux/démo sur le site web de CastorEngine à partir de l'éditeur.
	- Gestion d'actualité et de commentaire de ses projets publiés.
    - Système multi-langage avec éditeur de langue pour en ajouter ou les éditer.
    - Système de console de récupération des erreurs dans les scripts mais aussi sur la scène.

- **Updates**	

- **Bug fixes**  

- **Changements**  
	- Réécriture de l'interface de l'éditeur afin de la rendre plus agréable et plus simple.
	- Réécriture du site web pour le lier à l'éditeur et non le contraire dans la version 4.
	- Ajout d'une démo complète 'Survival' réalisée et publiée avec l'éditeur avec toutes les ressources disponibles en boutique.