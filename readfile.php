<?php
session_start();
function clearFolder($folder) {
	$dossier=opendir($folder);
	while ($fichier = readdir($dossier)) {
		if ($fichier != "." && $fichier != "..") {
			$Vidage= $folder.$fichier;
			unlink($Vidage);
		}
	}
	closedir($dossier);
}
function donwloadFile($extention) {
	$file = "game_castor.".$extention;
	if($extention == "zip") {
		$path = "public_scripts/temp_".$_SESSION['user']."/";
	} else{
		$path = "public_scripts/".$_SESSION['user']."/";
	}
	define('CFG_SYSTEM_FILENAME', ''.$file.''); // Nom du fichier pour le système
	define('CFG_SEND_FILENAME', ''.$file.''); // Nom du ficher pour le navigateur				
	define('CFG_DATE_FORMAT', 'D, d M Y H:i:s');
	ini_set('zlib.output_compression', "Off");
	header('Pragma: public');
	header('Last-Modified: '.gmdate(CFG_DATE_FORMAT).' GMT');
	header('Cache-Control: must-revalidate, pre-check=0, post-check=0, max-age=0');
	header('content-type: application/octet-stream');
	header('Content-Disposition: attachment; filename="'.CFG_SEND_FILENAME.'"');
	header('Date: '.gmdate(CFG_DATE_FORMAT, time()).' GMT');
	header('Expires: '.gmdate(CFG_DATE_FORMAT, time()+1).' GMT');
	header('Last-Modified: '.gmdate(CFG_DATE_FORMAT, time()).' GMT');
	ob_clean();
	flush();
	if(readfile($path.$file)) {
		@unlink($path.$file);
		@rmdir("public_scripts/temp_".$_SESSION['user']."/");
		clearFolder("public_scripts/".$_SESSION['user']."/");
	}
}
if($_GET['type'] == "castor3d") {	
	donwloadFile("castor3d");
} 
elseif($_GET['type'] == "zip") {
	donwloadFile("zip");
}
?>