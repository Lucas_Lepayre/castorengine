# **CastorEngine Version 5** #

### C’est quoi ? ###

[CastorEngine](http://www.castorengine.com/) est un éditeur de jeu 3D **gratuit** intégré au navigateur utilisant la technologie WebGL grâce au moteur 3D [BabylonJS](http://www.babylonjs.com/). 
Cet éditeur a été pensé pour les créateurs amateurs, particuliers qui souhaitent créer des jeux vidéo 3D sans mettre les mains sur des logiciels difficiles à comprendre ou à prendre en main. Les utilisateurs avancés et professionnels peuvent aussi l'utiliser et en tirer toute sa puissance pour créer de grands jeux. Vous pouvez avec CastorEngine créer tout type de jeux (plateforme, FPS, RPG, multijoueur, MMO, click and points et autres...)
 
Voici à quoi ressemble l'interface :

![castorengine_new.jpg](http://www.castorengine.com/images/CastorEngine.jpg)

* Vous voulez m'aider et participer, partager vos contributions? Clonez [le dépôt](https://bitbucket.org/Dad72/castorengine/src) dans: [sourceTree](http://www.sourcetreeapp.com/?utm_source=internal&utm_medium=link&utm_campaign=clone_repo_win)

**Pour ce qui est à venir et en cours, voir notre:** [**Feuille de route**](https://bitbucket.org/Dad72/castorengine/wiki/Feuille%20de%20route)

# Caractéristiques #

CastorEngine est un moteur de jeu 3D complet.

* Graphique
    * Base WebGL: moteur de rendu 3D Babylon.js
    * Lumière Directionnelle, spot, point et hémisphérique (Directionnelle, point et spot peuvent émettre des ombres)
    * Importation d'objets 3d par URL distante.
    * Support des animations avec images clés
    * Support des matériaux standards par défaut de 3ds Max et Blender
    * Exports de modèles et scènes complètes avec 3Ds Max, Blender, Unity3D, Cheetah3d.
    * Support des texture tga et dds	
* Physique
    * Moteur physique CannonJS
* Collision
    * Moteur d'intersection d'objets sans recours à la physique
    * Scène picking pour sélectionner/cliquer des objets de la scène
    * Collisions webworkers
* Performance
    * Instances
	* Clonage
    * Octree statique et dynamique
    * Optimiseur de scène
    * Niveaux de détail (LOD)
	* Fusion d'objets
* Musique et son
    * Moteur Audio via l'API Web Audio	
	* Streming
	* Audio 3D 
* Système de script
    * Ecrire le comportement des jeux en attachant des scripts JavaScript sur les objets. (Écriture de scripts basés sur BabylonJS)
* Contrôles
    * Souris, clavier
    * Touch (Hand.js)
    * Gamepad
	* Joystick
* Autres
	* Exportation par zip de vos projets (contient: un .babylon et toutes les dépendances) sur votre ordinateur.
	* Publication de vos jeux en un clique via notre site dédier avec gestion des commentaires et actualités.	
	* Connexion à votre compte et édition de votre profil via l’éditeur.
	* Enregistrement de vos projets en cours sur votre ordinateur ou/et sur un serveur FTP.
	* Partage de media importer vers la boutique comunautaire.

Et bien d'autres caractéristiques. Pour la liste complètes des fonctionnalités du moteur, [voir ici](https://github.com/BabylonJS/Babylon.js#features).

# License #

CastorEngine est publié sous la licence MIT. [Voir fichier de licence.](http://opensource.org/licenses/MIT)