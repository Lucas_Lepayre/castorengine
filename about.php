<!DOCTYPE html>
<html>
	<head>
		<title>About</title>
		<meta name="Publisher" content="ActifGames" />
		<link rel="shortcut icon" type="image/x-icon" href="images/CastorEngine.ico" />
		<style> body { background-image: url(css/images/ui-bg_loop_25_000000_21x21.png); color:white; }	a, a:link { color:#a1d3ff; } a:hover { color:#b3fffe; } </style>
	</head>
		<body>
			<?php
			require_once("classes/lang.class.php");
			$lang = new Lang();
			?>
			<table width="100%">
				<tr>
					<td align="center" valign="center" width="300px"><img src="images/logo_castor_white.png" style="width:230px;height:280px;" /></td>
					<td valign="top" align="center">
						<br /><font size="5"><b>Castor3D studio</b></font><br /><br /><br /><br /><br />
						<b><?php echo $lang->load["about-proprio"];?> :</b> David Pellier (Dad72)<br />
						<b><?php echo $lang->load["about-contribute"];?> :</b> <a href="https://bitbucket.org/Dad72/castorengine/src/aa7ce392a654ff0c1b70e3dff5f9a139487efdfe/what's%20new.md?at=master&fileviewer=file-view-default" target="_blank"><font size='4px'>Credits - Change log</font></a><br />
						<?php
						$version = json_decode(file_get_contents('version.json'), true); echo "<br /><br /><font size='5px'>Version : ".$version['version']." - Update: ".$version['update']."</font>";
						?>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center"><br /><br /><small>All rights reserved © 2013-<?php echo date('Y', time());?> @castorengine.com</small></td>
				</tr>
			</table>
		</body>
</html>