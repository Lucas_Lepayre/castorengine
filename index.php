<?php
header('Content-Type: text/html; charset=utf-8');
$time_load = 4000;
?>
<!DOCTYPE html>
<html oncontextmenu="return false">
	<head>
		<title>Castor3D Studio 5</title>
		<link rel="stylesheet" type="text/css" href="css/w2ui.css">
		<link rel="stylesheet" type="text/css" href="css/menu-bar.css">
		<link rel="stylesheet" type="text/css" href="css/menu-tabs.css">
		<link rel="stylesheet" type="text/css" href="css/toolbar.css">
		<link rel="stylesheet" type="text/css" href="css/editor.css">
		<link rel="stylesheet" type="text/css" href="css/spectrum.css">
		<link rel="stylesheet" type="text/css" href="css/property-grid.css">
		<link rel="stylesheet" type="text/css" href="css/jquery.contextMenu.css" />
		<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">

		<script src="libs/jquery.min.js" type="text/javascript"></script>
		<script src="libs/jquery-ui.min.js" type="text/javascript"></script>
		<script src="libs/w2ui.min.js" type="text/javascript"></script>
		<script src="libs/spectrum.js" type="text/javascript"></script>
		<script src="libs/propertyGrid.js" type="text/javascript"></script>
		<script src="libs/cannon.js" type="text/javascript"></script>
		<script src="libs/babylon.js" type="text/javascript"></script>
		<script src="libs/hand.js" type="text/javascript"></script>
		<script src="libs/jquery.contextMenu.js" type="text/javascript"></script>
		<script src="libs/jquery.knob.js" type="text/javascript"></script>
		<link rel="shortcut icon" href="images/CastorEngine.ico">
		<script>
			jQuery.support.cors = true;
			$(function() {
				$({value: 0}).animate({value: 100},{ duration: <?php echo $time_load;?>, easing:"swing", step: function(){ $(".knob").val(this.value).trigger("change");} });
				$(".knob").knob();
				$("#chargementCE").show().css("height", ""+(document.body.offsetHeight)+"px");
				setTimeout(LoadFin, <?php echo $time_load;?>);
			});
			function LoadFin() { $("#chargementCE").hide();}
			var audio = new Audio(), ogg = false, mp3 = false, editor = null, media = null, nameObjetSelected = null, switchObjects = [], MODETRANSFORM = 1, layout = null, menu = null, _mesh = null, _name = null, _pickPoint = null;
			var user = "<?php echo @$_COOKIE['pseudo'];?>", lang = null;
			//variable array
			var myObjet = [], lensFlareSystem = [], parentLightDir = [], LightDirectional = [], LightPoint = [], parentLightPoint = [], LightHemispherique = [], parentLightHemispherique = [], LightSpot = [], parentLightSpot = [],
			ColliderMesh = [], Scripts = [], spriteManager = [], particleSystem = [], materials = [], materialsProcedural = [], sounds = [], FreeCamera = [], ArcRotateCamera = [], TouchCamera = [], FollowCamera = [], GameadCamera = [];
		</script>
	</head>
	<body>
		<?php
		require_once("classes/lang.class.php");
		$lang = new Lang();
		?>
		<div id="chargementCE" class="SplashScreen" style="display:none;">
			<div class="Frame_SplashScreen">
				<div class="splash"></div>
				<br /><input class="knob" data-fgColor="#383838" data-width="150" data-displayInput="false">
				<br /><?php echo $lang->load['chargement_projet'];?>
			</div>
		</div>
		<header>
			<?php
			require_once("includes/menu_barre.php");
			require_once("includes/menu_outils.php");
			// libs
			require_once("classes/zip.class.php");
			require_once("classes/ftp.class.php");
			?>
		</header>
		<section>
			<div id="tabsEditor">
				<ul>
					<li><a href="#tabs-desing"><?php echo $lang->load["design-tabs"];?></a></li>
					<li><a href="#tabs-code"><?php echo $lang->load["code-tabs"];?></a></li>
					<li><a href="#tabs-media"><?php echo $lang->load["media-tabs"];?></a></li>
				</ul>
				<div id="tabs-desing" class="layout-desing"></div>
				<div id="tabs-code" class="layout-code"></div>
				<div id="tabs-media" class="layout-store"></div>
			</div>
			<input type="text" class="undo" style="display:none;" />
			<input type="text" class="redo" style="display:none;" />
			<input type="hidden" id="proprieterCurrent" value="" />

			<script src="js/lang.js" type="text/javascript"></script>
			<script src="js/utiles.js" type="text/javascript"></script>
			<script src="js/menus.js" type="text/javascript"></script>
			<script src="js/collider.js" type="text/javascript"></script>
			<script src="js/manipulator.js" type="text/javascript"></script>
			<script src="js/layout.js" type="text/javascript"></script>
			<script src="js/property.js" type="text/javascript"></script>
			<script src="js/scene.js" type="text/javascript"></script>
			<script src="js/media.js" type="text/javascript"></script>
			<script src="js/addObjet.js" type="text/javascript"></script>
			<script src="js/editeObjet.js" type="text/javascript"></script>
		</section>
		<script id="script">
		lang = new LANG.loading();
		if(audio.canPlayType("audio/ogg") != "") { // ogg
			ogg = true;
		} else if(audio.canPlayType("audio/mpeg") != "") { // mp3
			mp3 = true;
		}
		$(document).ready(function(){
			setTimeout(function() {
				layout = new CASTORENGINE.layout();
				menu = new CASTORENGINE.menu();
				menu.outils();
				editor = new CASTORENGINE.scene();
				media = new CASTORENGINE.media();
				if (BABYLON.Engine.isSupported()) {
					editor.init();
					media.init();
					if(user != "") {
						editor.isLoguer = true;
						$("#li_connexion").remove();
						$("#li_profil").show();
					}
				} else {
					alert("<?php echo $lang->load["compatible-WebGL"];?>");
				}
			}, 500);
		});
		</script>
	</body>
</html>